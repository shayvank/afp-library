package org.augment.afp.request.textoverlay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.util.Coordinate;
import org.augment.afp.util.Dpi;
import org.augment.afp.util.UnitBase;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class TextOverlayHelperTest {

	String overlayTextWithCommentHex = "0008D3A89B0000000013D3EEEE000000E385A7A3D6A585999381A80013D3EEEE000000D7998995C3C540E385A7A30028D3EE9B0000002BD306F700002D0003F10004C701E004D302D00BDBE385A2A340A385A7A302F80008D3A99B000000";
	String overlayTextWithoutCommentHex = "0008D3A89B0000000013D3EEEE000000E385A7A3D6A585999381A80028D3EE9B0000002BD306F700002D0003F10004C701E004D302D00BDBE385A2A340A385A7A302F80008D3A99B000000";
	
	Dpi dpi = new Dpi(UnitBase.TEN_INCHES, 2400);
	
	@Test
	public void test_parse() throws IOException {
		byte[] textOverlayBytes = ByteHelpers.fromHexString(overlayTextWithCommentHex);
		
		TextOverlay textOverlay = TextOverlayHelper.parse(textOverlayBytes, new SizeDescriptor(dpi, dpi, 240, 250));
		
		assertEquals("PrinCE Text", textOverlay.getDescription());
		assertEquals(0, textOverlay.getDirection());
		//assertEquals(new TrueTypeFont("Times New Roman", 12), textOverlay.getFont());
		assertEquals((new Coordinate(2.0, 3.0)).getX(), textOverlay.getPosition().getX(), 0.0);
		assertEquals((new Coordinate(2.0, 3.0)).getY(), textOverlay.getPosition().getY(), 0.0);
		assertEquals("Test text", textOverlay.getText());
	}
	
	@Test
	public void test_format_with_description() throws IOException {
		TextOverlay.Builder builder = new TextOverlay.Builder();
		builder.withDescription("PrinCE Text");
		builder.withDirection(0);
		builder.withFont(new TrueTypeFont("Times New Roman", 12));
		builder.withPosition(new Coordinate(2.0, 3.0));
		builder.withText("Test text");
		
		byte [] actualBytes = TextOverlayHelper.format(builder.build(), new SizeDescriptor(dpi, dpi, 240, 250));
		byte[] expectedBytes = ByteHelpers.fromHexString(overlayTextWithCommentHex);
		
		assertEquals(expectedBytes.length, actualBytes.length);
		assertEquals(ByteHelpers.toHexString(expectedBytes), ByteHelpers.toHexString(actualBytes));
		assertTrue(Arrays.equals(expectedBytes, actualBytes));
	}
	
	@Test
	public void test_format_without_description() throws IOException {
		TextOverlay.Builder builder = new TextOverlay.Builder();
		builder.withDirection(0);
		builder.withFont(new TrueTypeFont("Times New Roman", 12));
		builder.withPosition(new Coordinate(2.0, 3.0));
		builder.withText("Test text");
		
		Dpi dpi = new Dpi(UnitBase.TEN_INCHES, 2400);
		
		byte [] actualBytes = TextOverlayHelper.format(builder.build(), new SizeDescriptor(dpi, dpi, 240, 250));
		byte[] expectedBytes = ByteHelpers.fromHexString(overlayTextWithoutCommentHex);
		
		assertEquals(expectedBytes.length, actualBytes.length);
		assertEquals(ByteHelpers.toHexString(expectedBytes), ByteHelpers.toHexString(actualBytes));
		assertTrue(Arrays.equals(expectedBytes, actualBytes));
	}
}
