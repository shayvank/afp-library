package org.augment.afp.request.textoverlay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import java.io.IOException;
import java.util.List;

import org.augment.afp.util.StructuredField;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class MapCodedFontTest {

	@Test
	public void test_parse_namepair() {
		// CP = T1001069, CS = C0H20790
		String value = "005ED3AB8A000000002B0C028500E3F1F0F0F1F0F6F90C028600C3F0C8F2F0F7F9F00325000424050104260000061D00002D00002B0C028500E3F1F0F0F1F0F6F90C028600C3F0C8F2F0F7F9F00325000424050204260000061D87000000";
		byte[] testBytes = ByteHelpers.fromHexString(value);
		
		MapCodedFont font = MapCodedFont.parse(new StructuredField(testBytes));
		List<AfpFont> fonts = font.getAfpFonts();
		
		assertEquals(2, fonts.size());
		AfpFont font1 = fonts.get(0);
		assertEquals("T1001069", font1.getCodePageName());
		assertEquals("C0H20790", font1.getCharacterSetName());
		assertEquals(1, font1.getLid());
		
		AfpFont font2 = fonts.get(1);
		assertEquals("T1001069", font2.getCodePageName());
		assertEquals("C0H20790", font2.getCharacterSetName());
		assertEquals(2, font2.getLid());
		
		assertEquals(2, font.getMaxLid());
		assertSame(font1, font.findAfpFont(font1));
		assertNotSame(font2, font.findAfpFont(font2)); // this is due to both fonts being the same with different lids.
	}
	
	@Test
	public void test_format_namepair() throws IOException {
		String expected = "0044D3AB8A000000001E0C028500E3F1F0F0F1F0F6F90C028600C3F0C8F2F0F7F9F004240501001E0C028500E3F1F0F0F1F0F6F90C028600C3F0C8F2F0F7F9F004240502";
		MapCodedFont codedFont = new MapCodedFont();
		codedFont.addAfpFont(new AfpFont("C0H20790", "T1001069", 1));
		codedFont.addAfpFont(new AfpFont("C0H20790", "T1001069", 2));
		byte[] data = MapCodedFont.format(codedFont);
		assertEquals(expected, ByteHelpers.toHexString(data));
	}
	
	@Test
	public void test_parse_coded() throws IOException {
		// 1 = X0H270, 2 = X0H276
		String value = "0028D3AB8A00000000100A028E00E7F0C8F2F7F00424050100100A028E00E7F0C8F2F7F604240502";
		byte[] testBytes = ByteHelpers.fromHexString(value);
		
		MapCodedFont font = MapCodedFont.parse(new StructuredField(testBytes));
		List<AfpFont> fonts = font.getAfpFonts();
		
		assertEquals(2, fonts.size());
		AfpFont font1 = fonts.get(0);
		assertEquals("X0H270", font1.getCodedFontName());
		assertEquals(1, font1.getLid());
		
		AfpFont font2 = fonts.get(1);
		assertEquals("X0H276", font2.getCodedFontName());
		assertEquals(2, font2.getLid());
		
		assertEquals(2, font.getMaxLid());
		
		assertSame(font1, font.findAfpFont(font1));
	}
	
	@Test
	public void test_format_coded() throws IOException {
		String expected = "0028D3AB8A00000000100A028E00E7F0C8F2F7F00424050100100A028E00E7F0C8F2F7F604240502";
		MapCodedFont codedFont = new MapCodedFont();
		codedFont.addAfpFont(new AfpFont("X0H270", 1));
		codedFont.addAfpFont(new AfpFont("X0H276", 2));
		byte[] data = MapCodedFont.format(codedFont);
		assertEquals(expected, ByteHelpers.toHexString(data));
	}
}
