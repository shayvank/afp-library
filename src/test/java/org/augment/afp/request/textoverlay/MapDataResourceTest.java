package org.augment.afp.request.textoverlay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.List;

import org.augment.afp.util.StructuredField;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class MapDataResourceTest {

	@Test
	public void test_parse() {
		// tnr - 12
		// tnrb - 16
		String value = "00BDD3ABC30000000058181000410000A80006072B12000401013300000000000000045061000C028500E3F1E5F1F0F5F0F0108B002000F0000000000003000100000601000001F41302DE00E3899485A240D585A640D9969481950502BE0001005D181000410000A80006072B12000401013300000000000000045061000C028500E3F1E5F1F0F5F0F0108B00200140000000000003000100000601000001F41802DE00E3899485A240D585A640D99694819540C29693840502BE0002";
		byte[] testBytes = ByteHelpers.fromHexString(value);
		
		MapDataResource font = MapDataResource.parse(new StructuredField(testBytes));
		List<TrueTypeFont> fonts = font.getTrueTypeFonts();
		
		assertEquals(2, fonts.size());
		TrueTypeFont font1 = fonts.get(0);
		assertEquals("Times New Roman", font1.getName());
		assertEquals(12, font1.getPt());
		assertEquals(1, font1.getLid());
		
		TrueTypeFont font2 = fonts.get(1);
		assertEquals("Times New Roman Bold", font2.getName());
		assertEquals(16, font2.getPt());
		assertEquals(2, font2.getLid());		
		
		assertEquals(2, font.getMaxLid());
		assertEquals(font1, font.findTrueTypeFont("Times New Roman", 12));
		assertNull(font.findTrueTypeFont("Arial", 10));
	}
	
	@Test
	public void test_format() throws IOException {
		String expected = "00BDD3ABC30000000058181000410000A80006072B12000401013300000000000000045061000C028500E3F1E5F1F0F5F0F0108B002000F0000000000003000100000601000001F41302DE00E3899485A240D585A640D9969481950502BE0001005D181000410000A80006072B12000401013300000000000000045061000C028500E3F1E5F1F0F5F0F0108B00200140000000000003000100000601000001F41802DE00E3899485A240D585A640D99694819540C29693840502BE0002";
		
		MapDataResource mapDataResource = new MapDataResource();
		mapDataResource.addTrueTypeFont(new TrueTypeFont("Times New Roman", 12, 1));
		mapDataResource.addTrueTypeFont(new TrueTypeFont("Times New Roman Bold", 16, 2));
		byte[] data = MapDataResource.format(mapDataResource);
		assertEquals(expected, ByteHelpers.toHexString(data));
	}
}
