package org.augment.afp.request.textoverlay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.augment.afp.request.RequestHandlerLoader;
import org.augment.afp.util.Coordinate;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class TextOverlayRequestHandlerTest {

	String pgGrp = "0018D3A8AD000000C7D9D6E4D7F1404008028D00E3C5E2E30010D3ABCC000000E3C5E2E340404040001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF4040404040400010D3A9AD000000FFFF404040404040";
	
	String pg = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF404040404040";
	
	String pgWithInsertText = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000060D3ABC30000000058181000410000A80006072B12000401013300000000000000045061000C028500E3F1E5F1F0F5F0F0108B002000F0000000000003000100000601000001F41302DE00E3899485A240D585A640D9969481950502BE00010008D3A9C90000000008D3A89B0000000013D3EEEE000000E385A7A3D6A585999381A80028D3EE9B0000002BD306F700002D0003F10104C701E004D302D00BDBE385A2A340A385A7A302F80008D3A99B0000000010D3A9AF000000FFFF404040404040";
	
	String pgAfterRemoveText = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000060D3ABC30000000058181000410000A80006072B12000401013300000000000000045061000C028500E3F1E5F1F0F5F0F0108B002000F0000000000003000100000601000001F41302DE00E3899485A240D585A640D9969481950502BE00010008D3A9C90000000010D3A9AF000000FFFF404040404040";
	
	@Test
	public void test_handleInsertTextOverlayRequest_Page() throws IOException {
		TextOverlay.Builder builder = new TextOverlay.Builder();
		builder.withDirection(0);
		builder.withFont(new TrueTypeFont("Times New Roman", 12));
		builder.withPosition(new Coordinate(2.0, 3.0));
		builder.withText("Test text");
		InsertTextOverlayRequest request = new InsertTextOverlayRequest(builder.build());
		
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pg));
		assertEquals(pgWithInsertText, ByteHelpers.toHexString(actual));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_handleInsertTextOverlayRequest_PageGroup() throws IOException {
		TextOverlay.Builder builder = new TextOverlay.Builder();
		builder.withDirection(0);
		builder.withFont(new TrueTypeFont("Times New Roman", 12));
		builder.withPosition(new Coordinate(2.0, 3.0));
		builder.withText("Test text");
		InsertTextOverlayRequest request = new InsertTextOverlayRequest(builder.build());
		
		RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgGrp));
	}	
	
	@Test
	public void test_handleRemoveTextOverlayRequest() throws IOException {
		RemoveTextOverlayRequest request = RemoveTextOverlayRequest.removeByText(".*Test.*");
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithInsertText));
		assertTrue(request.isSuccess());
		assertEquals(pgAfterRemoveText, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_handleRemoveTextOverlayRequest_NotFound() throws IOException {
		RemoveTextOverlayRequest request = RemoveTextOverlayRequest.removeByText(".*Tst.*");
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithInsertText));
		assertFalse(request.isSuccess());
		assertEquals(pgWithInsertText, ByteHelpers.toHexString(actual));
	}	
	
	@Test
	public void test_handleFindTextOverlayRequest() throws IOException {
		FindTextOverlayRequest request = FindTextOverlayRequest.findByText(".*Test.*");
		RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithInsertText));
		assertEquals(1, request.getResult().size());
		TextOverlay text = request.getResult().get(0);
		assertEquals(0, text.getDirection());
		//assertEquals(new TrueTypeFont("Times New Roman", 12), text.getFont());
		assertEquals((new Coordinate(2.0, 3.0)).getX(), text.getPosition().getX(), 0.0);
		assertEquals((new Coordinate(2.0, 3.0)).getY(), text.getPosition().getY(), 0.0);
		assertEquals("Test text", text.getText());
	}	
}
