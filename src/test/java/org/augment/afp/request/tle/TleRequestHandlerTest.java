package org.augment.afp.request.tle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.augment.afp.request.RequestHandlerLoader;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class TleRequestHandlerTest {

	String pgGrp = "0018D3A8AD000000C7D9D6E4D7F1404008028D00E3C5E2E30010D3ABCC000000E3C5E2E340404040001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF4040404040400010D3A9AD000000FFFF404040404040";
	
	String pgGrpWithTle = "0018D3A8AD000000C7D9D6E4D7F1404008028D00E3C5E2E30019D3A09000000008020B009581948509360000A58193A4850010D3ABCC000000E3C5E2E340404040001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF4040404040400010D3A9AD000000FFFF404040404040";
	
	String pg = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF404040404040";
	
	String pgWithTle = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000019D3A09000000008020B009581948509360000A58193A4850010D3A9AF000000FFFF404040404040";
	
	@Test
	public void test_handleInsertTleRequest_Page() throws IOException {
		TaggedLogicalElement tle = new TaggedLogicalElement("name", "value");
		InsertTleRequest request = new InsertTleRequest(tle);
		
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pg));
		assertEquals(pgWithTle, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_handleInsertTleRequest_PageGroup() throws IOException {
		TaggedLogicalElement tle = new TaggedLogicalElement("name", "value");
		InsertTleRequest request = new InsertTleRequest(tle);
		
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgGrp));
		assertEquals(pgGrpWithTle, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_handleRemoveTleRequest_Page() throws IOException {
		RemoveTleRequest request = RemoveTleRequest.removeByName(".*nam.*");
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithTle));
		assertTrue(request.isSuccess());
		assertEquals(pg, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_handleRemoveTleRequest_PageGroup() throws IOException {
		RemoveTleRequest request = RemoveTleRequest.removeByName(".*nam.*");
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgGrpWithTle));
		assertTrue(request.isSuccess());
		assertEquals(pgGrp, ByteHelpers.toHexString(actual));
	}	
	
	@Test
	public void test_handleRemoveTleRequest_NotFound() throws IOException {
		RemoveTleRequest request = RemoveTleRequest.removeByName(".*test.*");
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithTle));
		assertFalse(request.isSuccess());
		assertEquals(pgWithTle, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_handleFindTleRequest_byName() throws IOException {
		FindTleRequest request = FindTleRequest.findByName("name");
		RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithTle));
		assertEquals(1, request.getResult().size());
		assertEquals("name", request.getResult().get(0).getQualifiedName());
		assertEquals("value", request.getResult().get(0).getAttrValue());
	}
	
	@Test
	public void test_handleFindTleRequest_byValue() throws IOException {
		FindTleRequest request = FindTleRequest.findByValue("value");
		RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithTle));
		assertEquals(1, request.getResult().size());
		assertEquals("name", request.getResult().get(0).getQualifiedName());
		assertEquals("value", request.getResult().get(0).getAttrValue());
	}	
}
