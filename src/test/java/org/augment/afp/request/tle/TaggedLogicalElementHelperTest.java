package org.augment.afp.request.tle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import org.augment.afp.util.StructuredField;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class TaggedLogicalElementHelperTest {

	@Test
	public void test_parse() throws IOException {
		byte[] tinTleBytes = ByteHelpers.fromHexString("001CD3A09000000007020B00E3C9D50D360000F9F9F9F9F9F9F9F9F9");
		StructuredField sf = new StructuredField(tinTleBytes);
		
		TaggedLogicalElement tle = TaggedLogicalElementHelper.parse(sf);
		assertEquals("TIN", tle.getQualifiedName());
		assertEquals("999999999", tle.getAttrValue());
	}
	
	@Test
	public void test_parse_null_name() throws IOException {
		byte[] tinTleBytes = ByteHelpers.fromHexString("0015D3A0900000000D360000F9F9F9F9F9F9F9F9F9");
		StructuredField sf = new StructuredField(tinTleBytes);
		
		TaggedLogicalElement tle = TaggedLogicalElementHelper.parse(sf);
		assertNull(tle);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_parse_invalid_FQNType() throws IOException {
		byte[] tinTleBytes = ByteHelpers.fromHexString("001CD3A09000000007020A00E3C9D50D360000F9F9F9F9F9F9F9F9F9");
		StructuredField sf = new StructuredField(tinTleBytes);
		
		TaggedLogicalElementHelper.parse(sf);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_parse_invalid_FQNFmt() throws IOException {
		byte[] tinTleBytes = ByteHelpers.fromHexString("001CD3A09000000007020B20E3C9D50D360000F9F9F9F9F9F9F9F9F9");
		StructuredField sf = new StructuredField(tinTleBytes);
		
		TaggedLogicalElementHelper.parse(sf);
	}
	
	@Test
	public void test_format() throws IOException {
		byte[] expectedBytes = ByteHelpers.fromHexString("001CD3A09000000007020B00E3C9D50D360000F9F9F9F9F9F9F9F9F9");
				
		TaggedLogicalElement tle = new TaggedLogicalElement("TIN", "999999999");
		
		StructuredField field = TaggedLogicalElementHelper.format(tle);
		
		assertEquals(expectedBytes.length, field.bytes().length);
		assertTrue(Arrays.equals(expectedBytes, field.bytes()));
	}
	
	@Test
	public void test_format_null_value() throws IOException {
		byte[] expectedBytes = ByteHelpers.fromHexString("0013D3A09000000007020B00D7C3C104360000");
				
		TaggedLogicalElement tle = new TaggedLogicalElement("PCA", null);
		
		StructuredField field = TaggedLogicalElementHelper.format(tle);
		
		assertEquals(expectedBytes.length, field.bytes().length);
		assertTrue(Arrays.equals(expectedBytes, field.bytes()));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_format_null_tle_exception() throws IOException {	
		TaggedLogicalElementHelper.format(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_format_null_name() throws IOException {
		TaggedLogicalElement tle = new TaggedLogicalElement(null, null);
		
		TaggedLogicalElementHelper.format(tle);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_format_empty_name() throws IOException {
		TaggedLogicalElement tle = new TaggedLogicalElement("", null);
		
		TaggedLogicalElementHelper.format(tle);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_format_value_toolong() throws IOException {
		TaggedLogicalElement tle = new TaggedLogicalElement("PCA", String.format("%1$251s", ""));
		
		TaggedLogicalElementHelper.format(tle);
	}	
}
