package org.augment.afp.request.blockout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.augment.afp.request.RequestHandlerLoader;
import org.augment.afp.util.Coordinate;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class BlockoutRequestHandlerTest {
	
	String pgGrp = "0018D3A8AD000000C7D9D6E4D7F1404008028D00E3C5E2E30010D3ABCC000000E3C5E2E340404040001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF4040404040400010D3A9AD000000FFFF404040404040";
	
	String pg = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF404040404040";
	
	String pgWithBlockout = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000008D3A8FB0000000010D3EEEE000000C29396839296A4A3000CD3EEEE000000A385A2A30008D3A8C7000000002DD3A66B000000034301084B000038403840094C02000870000B400370010E4E0040000000001000000000080020D3AC6B00000001170002D00003C000002D000000000000000000002D00010017D3A6FB000000000BB80BB801C20258F604000000080008D3A9C70000000022D3EEFB00000070009101FF9409000BB80BB801C2025895028201960101970100005AD3EEFB000000FE92004EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF001001000CD3EEFB000000930071000008D3A9FB0000000010D3A9AF000000FFFF404040404040";
	
	@Test
	public void test_handleInsertBlockoutRequest_Page() throws IOException {
		Blockout.Builder builder = new Blockout.Builder();
		builder.withColor(Blockout.Color.BLACK.getCode());
		builder.withDescription("test");
		builder.withPosition(new Coordinate(3,4));
		builder.withSize(2, 1.5);
		InsertBlockoutRequest request = new InsertBlockoutRequest(builder.build());
	
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pg));
		assertEquals(pgWithBlockout, ByteHelpers.toHexString(actual));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_handleInsertBlockoutRequest_PageGroup() throws IOException {
		Blockout.Builder builder = new Blockout.Builder();
		builder.withColor(Blockout.Color.BLACK.getCode());
		builder.withDescription("test");
		builder.withPosition(new Coordinate(3,4));
		builder.withSize(2, 1.5);
		InsertBlockoutRequest request = new InsertBlockoutRequest(builder.build());
	
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgGrp));
		assertEquals(pgWithBlockout, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_removeBlockoutRequest() throws IOException {
		RemoveBlockoutRequest request = RemoveBlockoutRequest.removeByDescription(".*test.*");
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithBlockout));
		assertTrue(request.isSuccess());
		assertEquals(pg, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_removeBlockoutRequest_NotFound() throws IOException {
		RemoveBlockoutRequest request = RemoveBlockoutRequest.removeByDescription("testtest");
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithBlockout));
		assertFalse(request.isSuccess());
		assertEquals(pgWithBlockout, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_handleFindBlockoutRequest() throws IOException {
		FindBlockoutRequest request = FindBlockoutRequest.findByDescription(".*test.*");
		RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithBlockout));
		assertEquals(1, request.getResult().size());
		Blockout blockout = request.getResult().get(0);
		assertEquals(Blockout.Color.BLACK.getCode(), blockout.getColor());
		assertEquals("test", blockout.getDescription());
		assertEquals(3, blockout.getPosition().getX(), 0.0);
		assertEquals(4, blockout.getPosition().getY(), 0.0);
		assertEquals(2, blockout.getHeight(), 0.0);
		assertEquals(1.5, blockout.getWidth(), 0.0);
	}
}
