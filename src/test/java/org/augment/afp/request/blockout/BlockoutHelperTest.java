package org.augment.afp.request.blockout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import org.augment.afp.request.blockout.Blockout.Color;
import org.augment.afp.util.Coordinate;
import org.junit.BeforeClass;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class BlockoutHelperTest {
	
	static String blockoutWithCommentHex;
	static String blockoutWithoutCommentHex;
	
	static String blockoutCompressedWithCommentHex = "0008D3A8FB0000000010D3EEEE000000C29396839296A4A30017D3EEEE000000D7998995C3C540829396839296A4A30008D3A8C7000000002DD3A66B000000034301084B000038403840094C02001DD00013B00370010E4E0040000000001000000000080020D3AC6B000000011700073800099000002D000000000000000000002D00010017D3A6FB000000000BB80BB80636041AF604000000080008D3A9C70000000022D3EEFB00000070009101FF9409000BB80BB80636041A950282019601019701000093D3EEFB000000FE920087FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0001001000CD3EEFB000000930071000008D3A9FB000000";
	static String blockoutCompressedWithoutCommentHex = "0008D3A8FB0000000010D3EEEE000000C29396839296A4A30008D3A8C7000000002DD3A66B000000034301084B000038403840094C02001DD00013B00370010E4E0040000000001000000000080020D3AC6B000000011700073800099000002D000000000000000000002D00010017D3A6FB000000000BB80BB80636041AF604000000080008D3A9C70000000022D3EEFB00000070009101FF9409000BB80BB80636041A950282019601019701000093D3EEFB000000FE920087FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0001001000CD3EEFB000000930071000008D3A9FB000000";
	
	
	@BeforeClass
	public static void setupOnce() throws IOException {
		ClassLoader classLoader = BlockoutHelperTest.class.getClassLoader();
		
		String fileName = "blockoutWithCommentHex.afp";
		File origFile = new File(classLoader.getResource(fileName).getFile());
		blockoutWithCommentHex = ByteHelpers.toHexString(Files.readAllBytes(origFile.toPath()));
		
		fileName = "blockoutWithoutCommentHex.afp";
		origFile = new File(classLoader.getResource(fileName).getFile());
		
		blockoutWithoutCommentHex = ByteHelpers.toHexString(Files.readAllBytes(origFile.toPath()));
	}
	
	@Test
	public void test_parse() throws IOException {
		int pageDpi = 240;		
		
		byte[] barcodeBytes = ByteHelpers.fromHexString(blockoutWithCommentHex);
		
		Blockout blockout = BlockoutHelper.parse(barcodeBytes, pageDpi);
		
		assertEquals("PrinCE blockout", blockout.getDescription());
		
		assertEquals(Color.BLACK.getCode(), blockout.getColor());
		assertEquals(7.7, blockout.getPosition().getX(), 0.0);
		assertEquals(10.2, blockout.getPosition().getY(), 0.0);
		assertEquals(3.5, blockout.getHeight(), 0.0);
		assertEquals(5.3, blockout.getWidth(), 0.0);
	}
	
	@Test
	public void test_format_with_desription() throws IOException {
		int pageDpi = 240;
		double sheetHeight = 11.0;
		double sheetWidth = 8.5;
		
		Blockout.Builder builder = new Blockout.Builder();
		builder.withColor(Blockout.Color.BLACK.getCode());
		builder.withPosition(new Coordinate(7.7, 10.2));
		builder.withSize(3.5, 5.3);
		builder.withDescription("PrinCE blockout");
		builder.withCompression(false);
		
		byte[] actualBytes = BlockoutHelper.format(builder.build(), sheetWidth, sheetHeight, pageDpi);
		byte[] expectedBytes = ByteHelpers.fromHexString(blockoutWithCommentHex);

		assertEquals(expectedBytes.length, actualBytes.length);
		assertEquals(ByteHelpers.toHexString(expectedBytes), ByteHelpers.toHexString(actualBytes));
		assertTrue(Arrays.equals(expectedBytes, actualBytes));
	}
	
	@Test
	public void test_format_without_description() throws IOException {
		int pageDpi = 240;
		double sheetHeight = 11.0;
		double sheetWidth = 8.5;
		
		Blockout.Builder builder = new Blockout.Builder();
		builder.withColor(Blockout.Color.BLACK.getCode());
		builder.withPosition(new Coordinate(7.7, 10.2));
		builder.withSize(3.5, 5.3);
		builder.withCompression(false);
		
		byte[] actualBytes = BlockoutHelper.format(builder.build(), sheetWidth, sheetHeight, pageDpi);
		byte[] expectedBytes = ByteHelpers.fromHexString(blockoutWithoutCommentHex);
		
		//assertEquals(expectedBytes.length, actualBytes.length);
		assertEquals(ByteHelpers.toHexString(expectedBytes), ByteHelpers.toHexString(actualBytes));
		assertTrue(Arrays.equals(expectedBytes, actualBytes));				
	}
	
	@Test
	public void test_format_compressed_with_desription() throws IOException {
		int pageDpi = 240;
		double sheetHeight = 11.0;
		double sheetWidth = 8.5;
		
		Blockout.Builder builder = new Blockout.Builder();
		builder.withColor(Blockout.Color.BLACK.getCode());
		builder.withPosition(new Coordinate(7.7, 10.2));
		builder.withSize(3.5, 5.3);
		builder.withDescription("PrinCE blockout");
		
		byte[] actualBytes = BlockoutHelper.format(builder.build(), sheetWidth, sheetHeight, pageDpi);
		byte[] expectedBytes = ByteHelpers.fromHexString(blockoutCompressedWithCommentHex);

		//assertEquals(expectedBytes.length, actualBytes.length);
		assertEquals(ByteHelpers.toHexString(expectedBytes), ByteHelpers.toHexString(actualBytes));
		assertTrue(Arrays.equals(expectedBytes, actualBytes));
	}
	
	@Test
	public void test_format_compressed_without_description() throws IOException {
		int pageDpi = 240;
		double sheetHeight = 11.0;
		double sheetWidth = 8.5;
		
		Blockout.Builder builder = new Blockout.Builder();
		builder.withColor(Blockout.Color.BLACK.getCode());
		builder.withPosition(new Coordinate(7.7, 10.2));
		builder.withSize(3.5, 5.3);
		
		byte[] actualBytes = BlockoutHelper.format(builder.build(), sheetWidth, sheetHeight, pageDpi);
		byte[] expectedBytes = ByteHelpers.fromHexString(blockoutCompressedWithoutCommentHex);
		
		//assertEquals(expectedBytes.length, actualBytes.length);
		assertEquals(ByteHelpers.toHexString(expectedBytes), ByteHelpers.toHexString(actualBytes));
		assertTrue(Arrays.equals(expectedBytes, actualBytes));				
	}	
}
