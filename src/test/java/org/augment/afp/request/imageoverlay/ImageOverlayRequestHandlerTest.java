package org.augment.afp.request.imageoverlay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.augment.afp.request.RequestHandlerLoader;
import org.augment.afp.util.Coordinate;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class ImageOverlayRequestHandlerTest {

	String pgGrp = "0018D3A8AD000000C7D9D6E4D7F1404008028D00E3C5E2E30010D3ABCC000000E3C5E2E340404040001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF4040404040400010D3A9AD000000FFFF404040404040";
	
	String pg = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF404040404040";
	
	String pgWithInsertOverlay = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C9000000001AD3ABD800000000120C028400D6F1F0F0F0F0F3F0042402010017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000018D3AFD8000000D6F1F0F0F0F0F3F000000000000000000010D3A9AF000000FFFF404040404040";
	
	@Test
	public void test_handleInsertImageOverlayRequest_Page() throws IOException {
		ImageOverlay.Builder builder = new ImageOverlay.Builder();
		builder.withDirection(0);
		builder.withName("O1000030");
		builder.withPosition(new Coordinate(0,0));
		InsertImageOverlayRequest request = new InsertImageOverlayRequest(builder.build());
		
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pg));
		assertEquals(pgWithInsertOverlay, ByteHelpers.toHexString(actual));
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_handleInsertImageOverlayRequest_PageGroup() throws IOException {
		ImageOverlay.Builder builder = new ImageOverlay.Builder();
		builder.withDirection(0);
		builder.withName("O1000030");
		builder.withPosition(new Coordinate(0,0));
		InsertImageOverlayRequest request = new InsertImageOverlayRequest(builder.build());
		
		RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgGrp));
	}
	
	@Test
	public void test_handleRemoveImageOverlayRequest() throws IOException {
		RemoveImageOverlayRequest request = RemoveImageOverlayRequest.removeByName(".*O1000030.*");
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithInsertOverlay));
		assertTrue(request.isSuccess());
		assertEquals(pg, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_handleRemoveImageOverlayRequest_NotFound() throws IOException {
		RemoveImageOverlayRequest request = RemoveImageOverlayRequest.removeByName(".*O10030.*");
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithInsertOverlay));
		assertFalse(request.isSuccess());
		assertEquals(pgWithInsertOverlay, ByteHelpers.toHexString(actual));		
	}
	
	@Test
	public void test_handleFindImageOverlayRequest() throws IOException {
		FindImageOverlayRequest request = FindImageOverlayRequest.findByName(".*O1000030.*");
		RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithInsertOverlay));
		assertEquals(1, request.getResult().size());
		ImageOverlay overlay = request.getResult().get(0);
		assertEquals(0, overlay.getDirection());
		assertEquals(new Coordinate(0,0).getX(), overlay.getPosition().getX(), 0.0);
		assertEquals(new Coordinate(0,0).getY(), overlay.getPosition().getY(), 0.0);
		assertEquals("O1000030", overlay.getQualifiedName());
	}
}
