package org.augment.afp.request.imageoverlay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.util.Coordinate;
import org.augment.afp.util.Dpi;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.UnitBase;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class ImageOverlayHelperTest {

	String imageIPOHex = "0018D3AFD8000000D6F1F0F0F0F0F3F00000000000000000";
	String imageMPOHex = "001AD3ABD800000000120C028400D6F1F0F0F0F0F3F004240201";
	
	Dpi dpi = new Dpi(UnitBase.TEN_INCHES, 2400);
	
	@Test
	public void test_formatIPO() throws IOException {
		ImageOverlay.Builder builder = new ImageOverlay.Builder();
		builder.withName("O1000030");
		builder.withDirection(0);
		builder.withPosition(new Coordinate(0,0));
		
		byte[] expectedBytes = ByteHelpers.fromHexString(imageIPOHex);
		StructuredField field = ImageOverlayHelper.formatIPO(builder.build(), 300);
		
		assertEquals(expectedBytes.length, field.bytes().length);
		assertTrue(Arrays.equals(expectedBytes,  field.bytes()));
	}
	
	@Test
	public void test_parseIPO() throws IOException {
		StructuredField sf = new StructuredField(ByteHelpers.fromHexString(imageIPOHex));
		ImageOverlay overlay = ImageOverlayHelper.parseIPO(sf, new SizeDescriptor(dpi, dpi, 240, 250));
		assertEquals("O1000030", overlay.getQualifiedName());
		assertEquals(0, overlay.getDirection());
		assertEquals(0.0, overlay.getPosition().getX(), 0.0);
		assertEquals(0.0, overlay.getPosition().getY(), 0.0);
	}
	
	@Test
	public void test_formatMPO() throws IOException {
		Mpo mpo = new Mpo("O1000030", 1);
		
		byte[] expectedBytes = ByteHelpers.fromHexString(imageMPOHex);
		StructuredField field = ImageOverlayHelper.formatMPO(mpo);
		
		assertEquals(expectedBytes.length, field.bytes().length);
		assertTrue(Arrays.equals(expectedBytes,  field.bytes()));
	}
	
	@Test
	public void test_parseMPO() throws IOException {
		StructuredField sf = new StructuredField(ByteHelpers.fromHexString(imageMPOHex));
		Mpo mpo = ImageOverlayHelper.parseMPO(sf);
		assertEquals("O1000030", mpo.getName());
		assertEquals(1, mpo.getLid());
	}
}
