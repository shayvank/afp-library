package org.augment.afp.request.datamatrix;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.augment.afp.request.RequestHandlerLoader;
import org.augment.afp.util.Coordinate;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class DataMatrixBarcodeRequestHandlerTest {

	String pgGrp = "0018D3A8AD000000C7D9D6E4D7F1404008028D00E3C5E2E30010D3ABCC000000E3C5E2E340404040001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF4040404040400010D3A9AD000000FFFF404040404040";
	
	String pg = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF404040404040";
	
	String pgWithBarcode = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000008D3A8EB0000000008D3A8C7000000002DD3A66B000000034301084B000009600960094C020000DC0000DC0370010E4E00400000000010000000FF080020D3AC6B000000011700070300095B00002D000000000000000000002D0001000DD3ABEB0000000005030400001FD3A6EB00000000000960096000DC00DC00001C00FF000014FFFF01FFFF0008D3A9C70000000022D3EEEB000000000034003480001800180000FEFE009781A893968184888599850008D3A9EB0000000010D3A9AF000000FFFF404040404040";
	
	@Test
	public void test_handleInsertDataMatrixBarcodeRequest_Page() throws IOException {
		DataMatrixBarcode.Builder builder = new DataMatrixBarcode.Builder();
		builder.withLocation(new Coordinate(7.48, 9.98));
		builder.withEbcdic2Ascii(true);
		builder.withResolution(240);
		builder.withGridSize(24, 24);
		builder.withElementSizeInMils(20);
		builder.withQuietZoneInElements(11);
		builder.withPayload("payloadhere");
		InsertDataMatrixBarcodeRequest barRequest = new InsertDataMatrixBarcodeRequest(builder.build());
		
		byte[] actual = RequestHandlerLoader.findHandler(barRequest).handle(barRequest, ByteHelpers.fromHexString(pg));
		assertEquals(pgWithBarcode, ByteHelpers.toHexString(actual));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_handleInsertDataMatrixBarcodeRequest_PageGroup() throws IOException {
		DataMatrixBarcode.Builder builder = new DataMatrixBarcode.Builder();
		builder.withLocation(new Coordinate(7.48, 9.98));
		builder.withEbcdic2Ascii(true);
		builder.withResolution(240);
		builder.withGridSize(24, 24);
		builder.withElementSizeInMils(20);
		builder.withQuietZoneInElements(11);
		builder.withPayload("payloadhere");
		InsertDataMatrixBarcodeRequest barRequest = new InsertDataMatrixBarcodeRequest(builder.build());
		
		RequestHandlerLoader.findHandler(barRequest).handle(barRequest, ByteHelpers.fromHexString(pgGrp));
	}
	
	@Test
	public void test_removeDataMatrixBarcodeRequest() throws IOException {
		RemoveDataMatrixBarcodeRequest removeBarcodeRequest = RemoveDataMatrixBarcodeRequest.removeByPayload(".*payloadhere.*");
		byte[] actual = RequestHandlerLoader.findHandler(removeBarcodeRequest).handle(removeBarcodeRequest, ByteHelpers.fromHexString(pgWithBarcode));
		assertTrue(removeBarcodeRequest.isSuccess());
		assertEquals(pg, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_removeDataMatrixBarcodeRequest_NotFound() throws IOException {
		RemoveDataMatrixBarcodeRequest removeBarcodeRequest = RemoveDataMatrixBarcodeRequest.removeByPayload(".*notfound.*");
		byte[] actual = RequestHandlerLoader.findHandler(removeBarcodeRequest).handle(removeBarcodeRequest, ByteHelpers.fromHexString(pgWithBarcode));
		assertFalse(removeBarcodeRequest.isSuccess());
		assertEquals(pgWithBarcode, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_handleFindDataMatrixBarcodeRequest() throws IOException {
		FindDataMatrixBarcodeRequest findBarcodeRequest = FindDataMatrixBarcodeRequest.findByPayload(".*payload.*");
		RequestHandlerLoader.findHandler(findBarcodeRequest).handle(findBarcodeRequest, ByteHelpers.fromHexString(pgWithBarcode));
		assertEquals(1, findBarcodeRequest.getResult().size());
		DataMatrixBarcode barcode = findBarcodeRequest.getResult().get(0);
		assertEquals(7.48, barcode.getLocation().getX(), 0.01);
		assertEquals(9.98, barcode.getLocation().getY(), 0.01);
		assertTrue(barcode.isEbcdic2Ascii());
		assertEquals(240, barcode.getResolution());
		assertEquals(24, barcode.getNumberOfColumns());
		assertEquals(24, barcode.getNumberOfRows());
		assertEquals(20, barcode.getElementSizeInMils());
		//assertEquals(11, barcode.getQuietZoneInElements()); we haven't yet figured out how to calculate this
		assertEquals("payloadhere", barcode.getPayload());
	}
	
}
