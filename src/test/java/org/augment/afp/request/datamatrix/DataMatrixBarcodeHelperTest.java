package org.augment.afp.request.datamatrix;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import org.augment.afp.util.Coordinate;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class DataMatrixBarcodeHelperTest {
	
	String barcodeWithCommentHex    = "0008D3A8EB0000000016D3EEEE000000D7998995C3C540C28199839684850008D3A8C7000000002DD3A66B000000034301084B000009600960094C020000DC0000DC0370010E4E00400000000010000000FF080020D3AC6B000000011700073800099000002D000000000000000000002D0001000DD3ABEB0000000005030400001FD3A6EB00000000000960096000DC00DC00001C00FF000014FFFF01FFFF0008D3A9C7000000003ED3EEEB000000000034003480001800180000FEFE00F0F1F0F1F0F0F0F0F0F1F0F0F0F0F0F0F0F0C4D3F0F1D2F1F1F7F3F5F1F1F0F0F0F0F0F0F0F0F10008D3A9EB000000";
	String barcodeWithoutCommentHex = "0008D3A8EB0000000008D3A8C7000000002DD3A66B000000034301084B000009600960094C020000DC0000DC0370010E4E00400000000010000000FF080020D3AC6B000000011700073800099000002D000000000000000000002D0001000DD3ABEB0000000005030400001FD3A6EB00000000000960096000DC00DC00001C00FF000014FFFF01FFFF0008D3A9C7000000003ED3EEEB000000000034003480001800180000FEFE00F0F1F0F1F0F0F0F0F0F1F0F0F0F0F0F0F0F0C4D3F0F1D2F1F1F7F3F5F1F1F0F0F0F0F0F0F0F0F10008D3A9EB000000";

	@Test
	public void test_parse() throws IOException {
		
		byte[] barcodeBytes = ByteHelpers.fromHexString(barcodeWithCommentHex);
		
		DataMatrixBarcode barcode = DataMatrixBarcodeHelper.parse(barcodeBytes, 240);
		
		assertEquals("PrinCE Barcode", barcode.getDescription());
		
		assertEquals("010100000100000000DL01K1173511000000001", barcode.getPayload());
		assertTrue(barcode.isEbcdic2Ascii());
		assertEquals(24, barcode.getNumberOfColumns());
		assertEquals(24, barcode.getNumberOfRows());
		assertEquals(20, barcode.getElementSizeInMils());
		assertEquals(7.7, barcode.getLocation().getX(), 0.0);
		assertEquals(10.2, barcode.getLocation().getY(), 0.0);
	}
	
	@Test
	public void test_format_with_description() throws IOException {
		DataMatrixBarcode.Builder builder = new DataMatrixBarcode.Builder();
		builder.withPayload("010100000100000000DL01K1173511000000001");
		builder.withEbcdic2Ascii(true);
		builder.withGridSize(24, 24);
		builder.withElementSizeInMils(20);
		builder.withQuietZoneInElements(11);
		builder.withLocation(new Coordinate(7.7, 10.2));
		builder.withDescription("PrinCE Barcode");
		
		byte[] actualBytes = DataMatrixBarcodeHelper.format(builder.build(), 240);
		byte[] expectedBytes = ByteHelpers.fromHexString(barcodeWithCommentHex);
		
		assertEquals(expectedBytes.length, actualBytes.length);
		assertEquals(ByteHelpers.toHexString(expectedBytes), ByteHelpers.toHexString(actualBytes));
		assertTrue(Arrays.equals(expectedBytes, actualBytes));				
	}
	
	@Test
	public void test_format_without_description() throws IOException {
		DataMatrixBarcode.Builder builder = new DataMatrixBarcode.Builder();
		builder.withPayload("010100000100000000DL01K1173511000000001");
		builder.withEbcdic2Ascii(true);
		builder.withGridSize(24, 24);
		builder.withElementSizeInMils(20);
		builder.withQuietZoneInElements(11);
		builder.withLocation(new Coordinate(7.7, 10.2));
		
		byte[] actualBytes = DataMatrixBarcodeHelper.format(builder.build(), 240);
		byte[] expectedBytes = ByteHelpers.fromHexString(barcodeWithoutCommentHex);
		
		assertEquals(expectedBytes.length, actualBytes.length);
		assertEquals(ByteHelpers.toHexString(expectedBytes), ByteHelpers.toHexString(actualBytes));
		assertTrue(Arrays.equals(expectedBytes, actualBytes));				
	}	
	
}
