package org.augment.afp.request.comment;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class InsertCommentRequestTest {

	@Test
	public void test_values() {
		Comment comment = new Comment("test");
		InsertCommentRequest req = new InsertCommentRequest(comment);
		
		assertSame(comment, req.getValue());
		req.setSuccess(true);
		assertTrue(req.isSuccess());
		assertTrue(req.isUpdate());
	}
}
