package org.augment.afp.request.comment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class FindCommentRequestTest {

	@Test
	public void test_values() {
		FindCommentRequest req = FindCommentRequest.findByValue("test");
		assertEquals("test", req.getValueSearch());
		
		List<Comment> comments = new ArrayList<>();
		comments.add(new Comment("test"));
		req.setResult(comments);
		assertSame(comments, req.getResult());
		assertFalse(req.isUpdate());
	}
}
