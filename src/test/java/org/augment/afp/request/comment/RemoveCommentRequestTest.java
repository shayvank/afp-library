package org.augment.afp.request.comment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class RemoveCommentRequestTest {

	@Test
	public void test_values() {
		RemoveCommentRequest req = new RemoveCommentRequest("test");
		
		assertEquals("test", req.getValueSearch());
		req.setSuccess(true);
		assertTrue(req.isSuccess());
		assertTrue(req.isUpdate());
	}
}
