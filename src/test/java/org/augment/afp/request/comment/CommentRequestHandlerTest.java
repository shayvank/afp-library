package org.augment.afp.request.comment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.augment.afp.request.RequestHandlerLoader;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class CommentRequestHandlerTest {

	String pg = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF404040404040";
	
	String pgWithComment = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E3038101000CD3EEEE000000A385A2A30008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF404040404040";
	
	@Test
	public void test_handleInsertCommentRequest() throws IOException {
		Comment comment = new Comment("test");
		InsertCommentRequest request = new InsertCommentRequest(comment);
		byte[] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pg));
		assertEquals(pgWithComment, ByteHelpers.toHexString(actual));
	}
	
	@Test
	public void test_handleRemoveCommentRequest() throws IOException {
		RemoveCommentRequest request = new RemoveCommentRequest("test");
		byte [] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithComment));
		assertTrue(request.isSuccess());
		assertEquals(pg, ByteHelpers.toHexString(actual));		
	}
	
	@Test
	public void test_handleRemoveCommentRequest_NotFound() throws IOException {
		RemoveCommentRequest request = new RemoveCommentRequest("test2");
		byte [] actual = RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithComment));
		assertFalse(request.isSuccess());
		assertEquals(pgWithComment, ByteHelpers.toHexString(actual));		
	}	
	
	@Test
	public void test_handleFindCommentRequest() throws IOException {
		FindCommentRequest request = FindCommentRequest.findByValue(".*test.*");
		RequestHandlerLoader.findHandler(request).handle(request, ByteHelpers.fromHexString(pgWithComment));
		assertEquals(1, request.getResult().size());
		assertEquals("test", request.getResult().get(0).getValue());
	}

}
