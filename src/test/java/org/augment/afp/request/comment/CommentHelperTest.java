package org.augment.afp.request.comment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import org.augment.afp.util.StructuredField;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class CommentHelperTest {

	@Test
	public void test_parse() throws IOException {
		byte[] commentBytes = ByteHelpers.fromHexString("0010D3EEEE000000D7D7C6C161C1C9E7");
		StructuredField sf = new StructuredField(commentBytes);
				
		Comment comment = CommentHelper.parse(sf);
		assertEquals("PPFA/AIX", comment.getValue());
	}
	
	@Test
	public void test_format() throws IOException {
		byte[] expectedBytes = ByteHelpers.fromHexString("0010D3EEEE000000D7D7C6C161C1C9E7");
		
		Comment comment = new Comment("PPFA/AIX");
		StructuredField field = CommentHelper.format(comment);
		
		assertEquals(expectedBytes.length, field.bytes().length);
		assertTrue(Arrays.equals(expectedBytes, field.bytes()));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_format_null_value() throws IOException {		
		Comment comment = new Comment(null);
		CommentHelper.format(comment);
	}
	
	@Test
	public void test_format_empty_value() throws IOException {
		byte[] expectedBytes = ByteHelpers.fromHexString("0008D3EEEE000000");
		
		Comment comment = new Comment("");
		StructuredField field = CommentHelper.format(comment);
		
		assertEquals(expectedBytes.length, field.bytes().length);
		assertTrue(Arrays.equals(expectedBytes, field.bytes()));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_format_value_toolong() throws IOException {
		Comment comment = new Comment(String.format("%1$32760s", ""));
		CommentHelper.format(comment);
	}

}
