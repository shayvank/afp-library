package org.augment.afp.request.comment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.augment.afp.util.StructuredField;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class CommentTest {

	@Test
	public void test_values() {
		Comment comment = new Comment("test");
		assertEquals("test", comment.getValue());
	}
		
	@Test
	public void test_is() {
		StructuredField sf = new StructuredField(ByteHelpers.fromHexString("0010D3EEEE000000D7D7C6C161C1C9E7"));
		assertTrue(Comment.is(sf));
		
		sf = new StructuredField(ByteHelpers.fromHexString("0010D3EEBB000000D7D7C6C161C1C9E7"));
		assertFalse(Comment.is(sf));
		
		sf = new StructuredField(ByteHelpers.fromHexString("0010D3A8CC000000D7D7C6C161C1C9E7"));
		assertFalse(Comment.is(sf));
	}

}
