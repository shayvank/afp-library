package org.augment.afp.request;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.augment.afp.request.comment.CommentRequestHandler;
import org.augment.afp.request.comment.FindCommentRequest;
import org.junit.Test;


public class RequestHandlerLoaderTest {

	@Test
	public void test_handle_happy() {
		FindCommentRequest request = FindCommentRequest.findByValue("test");
		assertTrue(RequestHandlerLoader.findHandler(request) instanceof CommentRequestHandler);
	}
	
	@Test
	public void test_handle_unhappy() {
		ActionRequest actionRequest = mock(ActionRequest.class);
		assertNull(RequestHandlerLoader.findHandler(actionRequest));
	}
}
