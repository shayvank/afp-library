package org.augment.afp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class CategoryCodeTest {

	@Test
	public void test_values() {
		assertEquals(32, CategoryCode.values().length);
		assertEquals(0x5F, CategoryCode.PAGE_SEGMENT.toByte());
	}
	
	@Test
	public void test_valueOf_Happy() {
		assertEquals(CategoryCode.PAGE_SEGMENT, CategoryCode.valueOf(0x5F));
	}
	
	@Test
	public void test_valueOf_Null() {
		assertNull(CategoryCode.valueOf(0x00));
	}
	
	@Test
	public void test_valueOf_enum() {
		assertEquals(CategoryCode.PAGE_SEGMENT, CategoryCode.valueOf("PAGE_SEGMENT"));
	}
}
