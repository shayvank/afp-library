package org.augment.afp.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import test.helpers.ByteHelpers;

public class CharacterEncodingEnumTest {
	
	@Test
	public void test_values() {
		CharacterEncoding ebcdic = CharacterEncodingEnum.EBCDIC;
		assertEquals("Cp500", ebcdic.getCharset());
		assertEquals(24832, ebcdic.getScheme());
		assertEquals("T1V10500", ebcdic.getCodePageName());
		assertEquals(500, ebcdic.getCodePage());		
	}
	
	@Test
	public void test_convert_values() {
		String hexBytes = "E3C5E2E3";
		assertEquals("TEST", CharacterEncodingEnum.EBCDIC.stringValue(ByteHelpers.fromHexString(hexBytes)));
		assertEquals(hexBytes, ByteHelpers.toHexString(CharacterEncodingEnum.EBCDIC.byteValue("TEST")));
	}

}
