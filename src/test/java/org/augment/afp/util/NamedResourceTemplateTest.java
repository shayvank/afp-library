package org.augment.afp.util;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import test.helpers.ByteHelpers;

public class NamedResourceTemplateTest {
	
	@Test
	public void test_createNamedResource() throws IOException {
		String expected = "001CD3A8CE000000A385A2A34040404000000A21FC00000000000000010203040010D3A9CE000000A385A2A340404040";
		
		byte[] data = new byte[] { 0x01, 0x02, 0x03, 0x04 };
		byte[] actual = NamedResourceTemplate.createNamedResource("test", ResourceObjectType.OVERLAY, data);
		
		assertEquals(expected, ByteHelpers.toHexString(actual));
	}

}
