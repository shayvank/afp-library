package org.augment.afp.util;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Test;

import test.helpers.ByteHelpers;

public class StructuredFieldOutputStreamTest {

	String testField = "0010D3A8CC000000E3C5E2E340404040";
	String expectedWith5A = "5A0010D3A8CC000000E3C5E2E3404040400010D3A8CC000000E3C5E2E34040404000";
	String expectedWithout5A = "0010D3A8CC000000E3C5E2E3404040400010D3A8CC000000E3C5E2E34040404000";
	
	@Test
	public void test_operations_with5A() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try (StructuredFieldOutputStream sos = new StructuredFieldOutputStream(out)) {
			StructuredField sf = new StructuredField(ByteHelpers.fromHexString(testField));
			sos.writeStructuredField(sf);
			sos.write(sf.bytes(), 0, sf.bytes().length);
			sos.write(0x00);
		}
		
		assertEquals(expectedWith5A, ByteHelpers.toHexString(out.toByteArray()));
	}
	
	@Test
	public void test_operations_without5A() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try (StructuredFieldOutputStream sos = new StructuredFieldOutputStream(out, false)) {
			StructuredField sf = new StructuredField(ByteHelpers.fromHexString(testField));
			sos.writeStructuredField(sf);
			sos.write(sf.bytes());
			sos.write(0x00);
		}
		
		assertEquals(expectedWithout5A, ByteHelpers.toHexString(out.toByteArray()));
	}
}
