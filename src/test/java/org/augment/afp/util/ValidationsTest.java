package org.augment.afp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class ValidationsTest {
	
	@Test
	public void test_notNull_Happy() {
		Validations.notNull(new String(), "not thrown");
	}
	
	@Test
	public void test_notNull_Fail() {
		try {
			Validations.notNull(null, "exception thrown");
			fail("should have throw error");
		} catch (IllegalArgumentException e) {
			assertEquals("exception thrown", e.getMessage());
		}
	}
	
	@Test
	public void test_notEmpty_Happy() {
		Validations.notEmpty(new String("test"), "not thrown");
	}
	
	@Test
	public void test_notEmpty_Fail() {
		try {
			Validations.notEmpty(new String(), "exception thrown");
			fail("should have throw error");
		} catch (IllegalArgumentException e) {
			assertEquals("exception thrown", e.getMessage());
		}
	}
	
	@Test
	public void test_equals_Happy() {
		String test = "test";
		Validations.equals(test, test, "not thrown");
	}
	
	@Test
	public void test_equals_Fail() {
		try {
			Validations.equals("a","b", "exception thrown");
			fail("should have throw error");
		} catch (IllegalArgumentException e) {
			assertEquals("exception thrown", e.getMessage());
		}
	}

}
