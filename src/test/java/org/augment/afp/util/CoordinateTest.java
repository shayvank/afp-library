package org.augment.afp.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CoordinateTest {
	
	@Test
	public void test_values() {
		Coordinate position = new Coordinate(1,2);
		assertEquals(1.0, position.getX(), 0);
		assertEquals(2, position.getY(), 0);
	}

}
