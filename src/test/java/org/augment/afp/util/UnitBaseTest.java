package org.augment.afp.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UnitBaseTest {

	@Test
	public void test_values() {
		assertEquals(0x00, UnitBase.TEN_INCHES.getByteValue());
		assertEquals(0x01, UnitBase.TEN_CENTIMETERS.getByteValue());
		
		assertEquals(2, UnitBase.values().length);
	}
	
	@Test
	public void test_valueOf_Happy() {
		assertEquals(UnitBase.TEN_INCHES, UnitBase.valueOf(0x00));
		assertEquals(UnitBase.TEN_CENTIMETERS, UnitBase.valueOf(0x01));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_valueOf_Fail() {
		UnitBase.valueOf(0x02);
	}
	
	@Test
	public void test_valueOf_enum() {
		assertEquals(UnitBase.TEN_INCHES, UnitBase.valueOf("TEN_INCHES"));
	}
}
