package org.augment.afp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import test.helpers.ByteHelpers;

public class StructuredFieldFileParserTest {

	String testField = "0010D3A8CC000000E3C5E2E340404040";
	
	@Rule
	public TemporaryFolder folder= new TemporaryFolder();
	
	@Test
	public void test_read_with5A() throws IOException {
		File file = folder.newFile();		
		StructuredField field = new StructuredField(ByteHelpers.fromHexString(testField));
		
		try (StructuredFieldOutputStream out = new StructuredFieldOutputStream(new FileOutputStream(file))) {
			out.writeStructuredField(field);
		}
		
		try (StructuredFieldFileParser parser = new StructuredFieldFileParser(file)) {
			assertEquals(file.getName(), parser.getFile().getName());
			StructuredField check = parser.readNextSfBytes();
			assertEquals(testField, ByteHelpers.toHexString(check.bytes()));
			assertNull(parser.readNextSfBytes());
		}
	}
	
	@Test
	public void test_read_without5A() throws IOException {
		File file = folder.newFile();		
		StructuredField field = new StructuredField(ByteHelpers.fromHexString(testField));
		
		try (StructuredFieldOutputStream out = new StructuredFieldOutputStream(new FileOutputStream(file), false)) {
			out.writeStructuredField(field);
		}
		
		try (StructuredFieldFileParser parser = new StructuredFieldFileParser(file)) {
			assertEquals(file.getName(), parser.getFile().getName());
			StructuredField check = parser.readNextSfBytes();
			assertEquals(testField, ByteHelpers.toHexString(check.bytes()));
			assertNull(parser.readNextSfBytes());
		}
	}
}
