package org.augment.afp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class TypeCodeTest {

	@Test
	public void test_values() {
		assertEquals(17, TypeCode.values().length);
		assertEquals(0x8C, TypeCode.FONT_INDEX.toByte());
	}
	
	@Test
	public void test_valueOf_Happy() {
		assertEquals(TypeCode.FONT_INDEX, TypeCode.valueOf(0x8C));
	}
	
	@Test
	public void test_valueOf_Null() {
		assertNull(TypeCode.valueOf(0x00));
	}
	
	@Test
	public void test_valueOf_enum() {
		assertEquals(TypeCode.FONT_INDEX, TypeCode.valueOf("FONT_INDEX"));
	}
}
