package org.augment.afp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.junit.Test;

import test.helpers.ByteHelpers;

public class StructuredFieldTest {

	String testField = "0010D3A8CC000000E3C5E2E340404040";
	String testFieldNoData = "0008D3A8CC000000";
	
	@Test
	public void test_object() {
		byte[] data = ByteHelpers.fromHexString(testField);
		StructuredField sf = new StructuredField(data);
		assertEquals(TypeCode.BEGIN, sf.getTypeCode());
		assertEquals(CategoryCode.MEDIUM_MAP, sf.getCategoryCode());
		assertEquals(16, sf.getLength());
		assertEquals(testField, ByteHelpers.toHexString(sf.bytes()));
		assertEquals("TEST    ", CharacterEncodingEnum.EBCDIC.stringValue(sf.getData()));
	}
	
	@Test
	public void test_object_nodata() {
		byte[] data = ByteHelpers.fromHexString(testFieldNoData);
		StructuredField sf = new StructuredField(data);
		assertEquals(TypeCode.BEGIN, sf.getTypeCode());
		assertEquals(CategoryCode.MEDIUM_MAP, sf.getCategoryCode());
		assertEquals(8, sf.getLength());
		assertEquals(testFieldNoData, ByteHelpers.toHexString(sf.bytes()));
		assertEquals(0, sf.getData().length);
	}
	
	@Test
	public void test_getNext_Happy() throws IOException {
		ByteArrayInputStream input = new ByteArrayInputStream(ByteHelpers.fromHexString(testField));
		StructuredField sf = StructuredField.getNext(new DataInputStream(input));
		assertEquals(TypeCode.BEGIN, sf.getTypeCode());
		assertEquals(CategoryCode.MEDIUM_MAP, sf.getCategoryCode());
		assertEquals(16, sf.getLength());
		assertEquals(testField, ByteHelpers.toHexString(sf.bytes()));
		assertEquals("TEST    ", CharacterEncodingEnum.EBCDIC.stringValue(sf.getData()));
	}
	
	@Test
	public void test_getNext_D3_Error() throws IOException {
		ByteArrayInputStream input = new ByteArrayInputStream(ByteHelpers.fromHexString("0008D2A8CC000000"));
		try {
			StructuredField.getNext(new DataInputStream(input));
			fail("should have errored");
		} catch (IllegalArgumentException e) {
			assertEquals("Structured Field is not set to MODCA class code D3.", e.getMessage());
		}
		
	}
}
