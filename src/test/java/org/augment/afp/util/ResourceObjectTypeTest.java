package org.augment.afp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class ResourceObjectTypeTest {

	
	@Test
	public void test_values() {
		for (ResourceObjectType type: ResourceObjectType.values()) {
			ResourceObjectType check = ResourceObjectType.findByValue(type.getValue());
			assertEquals(type.getValue(), check.getValue());
			assertEquals(type.getFileNamePrefix(), type.getFileNamePrefix());
		}
		
		assertNull(ResourceObjectType.findByValue(0xFF));
	}
}
