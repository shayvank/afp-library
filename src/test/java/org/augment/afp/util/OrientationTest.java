package org.augment.afp.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import test.helpers.ByteHelpers;

public class OrientationTest {

	@Test
	public void test_values() {
		assertEquals(0, Orientation.DEGREES_0.getShortValue());
		assertEquals("0000", ByteHelpers.toHexString(Orientation.DEGREES_0.getByteValue()));
		assertEquals(11520, Orientation.DEGREES_90.getShortValue());
		assertEquals("2D00", ByteHelpers.toHexString(Orientation.DEGREES_90.getByteValue()));
		assertEquals(23040, Orientation.DEGREES_180.getShortValue());
		assertEquals("5A00", ByteHelpers.toHexString(Orientation.DEGREES_180.getByteValue()));
		assertEquals(34560, Orientation.DEGREES_270.getShortValue());
		assertEquals("8700", ByteHelpers.toHexString(Orientation.DEGREES_270.getByteValue()));
		assertEquals(4, Orientation.values().length);
	}
	
	@Test
	public void test_findByInt() {
		assertEquals(Orientation.DEGREES_0, Orientation.findByInt(0));
		assertEquals(Orientation.DEGREES_90, Orientation.findByInt(90));
		assertEquals(Orientation.DEGREES_180, Orientation.findByInt(180));
		assertEquals(Orientation.DEGREES_270, Orientation.findByInt(270));
	}
	
	@Test
	public void test_findByValue() {
		assertEquals(Orientation.DEGREES_0, Orientation.findByValue(0));
		assertEquals(Orientation.DEGREES_90, Orientation.findByValue(11520));
		assertEquals(Orientation.DEGREES_180, Orientation.findByValue(23040));
		assertEquals(Orientation.DEGREES_270, Orientation.findByValue(34560));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_findByValue_exception() {
		Orientation.findByValue(34561);
	}
	
	@Test
	public void test_valueOf() {
		assertEquals(Orientation.DEGREES_270, Orientation.valueOf("DEGREES_270"));
	}
}
