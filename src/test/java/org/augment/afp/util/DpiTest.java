package org.augment.afp.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DpiTest {

	@Test
	public void test_values() {
		Dpi dpi = new Dpi(UnitBase.TEN_INCHES, 2400);
		assertEquals(UnitBase.TEN_INCHES, dpi.getUnitBase());
		assertEquals(2400, dpi.getUnitsPerUnitBase());
		assertEquals(240, dpi.getValue());
	}
}
