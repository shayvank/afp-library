package org.augment.afp.util;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class TripletTest {

	@Test
	public void test_createTriplet_verify_output() {
		byte[] data = {0x03, 0x01, (byte)0x02};
		Triplet triplet = new Triplet(data);
		
		assertEquals(3, triplet.getLength());
		assertEquals(0x01, triplet.getCode());
		assertEquals(1, triplet.getContents().length);
		assertEquals(0x02, triplet.getContents()[0]);
	}
	
	@Test
	public void test_parse() {
		byte[] data = {0x03, 0x01, 0x02, 0x04, 0x03, 0x01, 0x02};
		List<Triplet> triplets = Triplet.parse(data, 0, data.length);
		assertEquals(2, triplets.size());
		assertEquals(0x01, triplets.get(0).getCode());
		assertEquals(0x03, triplets.get(1).getCode());
	}
}
