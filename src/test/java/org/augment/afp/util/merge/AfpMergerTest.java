package org.augment.afp.util.merge;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldFileParser;
import org.augment.afp.util.StructuredFieldOutputStream;
import org.augment.afp.util.TypeCode;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import test.helpers.ByteHelpers;

public class AfpMergerTest {

	@Rule
	public TemporaryFolder folder= new TemporaryFolder();
	
	@Test
	public void test_merge_one_file() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		String fileName = "testfile.afp";
		 
		ClassLoader classLoader = getClass().getClassLoader();
		File origFile = new File(classLoader.getResource(fileName).getFile());
		
		File file = getFileWithoutNOP(origFile);
		
		try (AfpMerger merger = AfpMerger.createFromFiles(Arrays.asList(file), out)) {
			merger.mergeAll();			
		}
		
		assertEquals(ByteHelpers.toHexString(Files.readAllBytes(Paths.get(file.toURI()))), ByteHelpers.toHexString(out.toByteArray()));
	}
	
	@Test
	public void test_merge_same_resources() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		String fileName = "testfile.afp";
		String resultFileName = "test-merge-same.afp";
		 
		ClassLoader classLoader = getClass().getClassLoader();
		File origFile = new File(classLoader.getResource(fileName).getFile());
		File resultFile = new File(classLoader.getResource(resultFileName).getFile());
		
		File file = getFileWithoutNOP(origFile);
		
		try (AfpMerger merger = AfpMerger.createFromFiles(Arrays.asList(file, file), out)) {
			merger.mergeAll();			
		}
		
		assertEquals(ByteHelpers.toHexString(Files.readAllBytes(Paths.get(resultFile.toURI()))), ByteHelpers.toHexString(out.toByteArray()));
	}
	
	@Test
	public void test_merge_diff_resources() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		String fileName = "mergetestfile.afp";
		String fileName2 = "mergetestfile.diff.afp";
		String resultFileName = "mergetestfile-result.afp";
		 
		ClassLoader classLoader = getClass().getClassLoader();
		File origFile = getFileWithoutNOP(new File(classLoader.getResource(fileName).getFile()));
		File origFile2 = getFileWithoutNOP(new File(classLoader.getResource(fileName2).getFile()));
		File resultFile = getFileWithoutNOP(new File(classLoader.getResource(resultFileName).getFile()));
		
		try (AfpMerger merger = AfpMerger.createFromFiles(Arrays.asList(origFile, origFile2), out)) {
			merger.mergeAll();
		}
		
		byte[] expectedOutcome = Files.readAllBytes(Paths.get(resultFile.toURI()));
		byte[] actualOutcome = out.toByteArray();
		
		// the renaming function uses time so we have to cut out the renamed resource and compare later.
		int start = 0;
		int end = 3088;		
		assertEquals(ByteHelpers.toHexString(Arrays.copyOfRange(expectedOutcome, start, end)), ByteHelpers.toHexString(Arrays.copyOfRange(actualOutcome, start, end)));
		start = 3094;
		end = 5777;		
		assertEquals(ByteHelpers.toHexString(Arrays.copyOfRange(expectedOutcome, start, end)), ByteHelpers.toHexString(Arrays.copyOfRange(actualOutcome, start, end)));
		start = 5783;
		end = 6410;		
		assertEquals(ByteHelpers.toHexString(Arrays.copyOfRange(expectedOutcome, start, end)), ByteHelpers.toHexString(Arrays.copyOfRange(actualOutcome, start, end)));
		start = 6416;
		end = actualOutcome.length;		
		assertEquals(ByteHelpers.toHexString(Arrays.copyOfRange(expectedOutcome, start, end)), ByteHelpers.toHexString(Arrays.copyOfRange(actualOutcome, start, end)));
		
		
		int start1 = 3088;
		int end1 = 3094;
		int start2 = 5777;
		int end2 = 5783;
		assertEquals(ByteHelpers.toHexString(Arrays.copyOfRange(actualOutcome, start1, end1)), ByteHelpers.toHexString(Arrays.copyOfRange(actualOutcome, start2, end2)));
		start2 = 6410;
		end2 = 6416;
		assertEquals(ByteHelpers.toHexString(Arrays.copyOfRange(actualOutcome, start1, end1)), ByteHelpers.toHexString(Arrays.copyOfRange(actualOutcome, start2, end2)));		
	}
	
	
	private File getFileWithoutNOP(final File file) throws IOException {
		
		File outputFile = folder.newFile(file.getName());
				
		try (StructuredFieldFileParser reader = new StructuredFieldFileParser(file);
				StructuredFieldOutputStream out = new StructuredFieldOutputStream(new FileOutputStream(outputFile))) {
			StructuredField sf = null;
			
			while ((sf = reader.readNextSfBytes()) != null) {
				if (sf.getCategoryCode() == CategoryCode.NO_OPERATION && sf.getTypeCode() == TypeCode.DATA) {
					// nop, skip
				} else {
					out.writeStructuredField(sf);
				}
			}
		}
		
		return outputFile;
	}
}
