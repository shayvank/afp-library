package org.augment.afp.util.merge;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;

import org.augment.afp.util.AugmentAfpException;
import org.augment.afp.util.ResourceObjectType;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.merge.AfpMerger.*;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class DocumentRenamerTest {

	@Test
	public void renameResourceEnvelopeSF_NamedResource_NoNewName() throws IOException {
		String nameName = null;
		String namedResourceBegin       = "001CD3A8CE000000D6F1F0F0F0F0F0F100000A21FC00000000000000";
		
		StructuredField sfnamedResourceBegin = new StructuredField(ByteHelpers.fromHexString(namedResourceBegin));
		assertEquals(namedResourceBegin, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBegin, nameName).bytes()));		
	}
	
	@Test
	public void renameResourceEnvelopeSF_NamedResource() throws IOException {
		String nameName = "TESTTEST";
		String namedResourceBegin       = "001CD3A8CE000000D6F1F0F0F0F0F0F100000A21FC00000000000000";
		String namedResourceBeginResult = "001CD3A8CE000000E3C5E2E3E3C5E2E300000A21FC00000000000000";
		String namedResourceEnd       = "0010D3A9CE000000D6F1F0F0F0F0F0F1";
		String namedResourceEndResult = "0010D3A9CE000000E3C5E2E3E3C5E2E3";
		String namedResourceBeginWithFQN01       = "0028D3A8CE000000D6F1F0F0F0F0F0F100000A21FC000000000000000C020100D6F1F0F0F0F0F0F1";
		String namedResourceBeginWithFQN01Result = "0028D3A8CE000000E3C5E2E3E3C5E2E300000A21FC000000000000000C020100E3C5E2E3E3C5E2E3";
		
		StructuredField sfnamedResourceBegin = new StructuredField(ByteHelpers.fromHexString(namedResourceBegin));
		assertEquals(namedResourceBeginResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBegin, nameName).bytes()));
		StructuredField sfnamedResourceEnd = new StructuredField(ByteHelpers.fromHexString(namedResourceEnd));
		assertEquals(namedResourceEndResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceEnd, nameName).bytes()));
		StructuredField sfnamedResourceBeginWithFQN01 = new StructuredField(ByteHelpers.fromHexString(namedResourceBeginWithFQN01));
		assertEquals(namedResourceBeginWithFQN01Result, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBeginWithFQN01, nameName).bytes()));		
	}
	
	@Test
	public void renameResourceEnvelopeSF_Overlay() throws IOException {
		String nameName = "TESTTEST";
		String namedResourceBegin       = "0010D3A8DF000000D6F1F0F0F0F0F0F1";
		String namedResourceBeginResult = "0010D3A8DF000000E3C5E2E3E3C5E2E3";
		String namedResourceEnd       = "0010D3A9DF000000D6F1F0F0F0F0F0F1";
		String namedResourceEndResult = "0010D3A9DF000000E3C5E2E3E3C5E2E3";
		String namedResourceBeginWithFQN01       = "001CD3A8DF000000D6F1F0F0F0F0F0F10C020100D6F1F0F0F0F0F0F1";
		String namedResourceBeginWithFQN01Result = "001CD3A8DF000000E3C5E2E3E3C5E2E30C020100E3C5E2E3E3C5E2E3";
		String namedResourceEndNoName       = "0008D3A9DF000000";
		String namedResourceEndNoNameResult = "0008D3A9DF000000";		
		
		StructuredField sfnamedResourceBegin = new StructuredField(ByteHelpers.fromHexString(namedResourceBegin));
		assertEquals(namedResourceBeginResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBegin, nameName).bytes()));
		StructuredField sfnamedResourceEnd = new StructuredField(ByteHelpers.fromHexString(namedResourceEnd));
		assertEquals(namedResourceEndResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceEnd, nameName).bytes()));
		StructuredField sfnamedResourceBeginWithFQN01 = new StructuredField(ByteHelpers.fromHexString(namedResourceBeginWithFQN01));
		assertEquals(namedResourceBeginWithFQN01Result, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBeginWithFQN01, nameName).bytes()));
		StructuredField sfnamedResourceEndNoName = new StructuredField(ByteHelpers.fromHexString(namedResourceEndNoName));
		assertEquals(namedResourceEndNoNameResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceEndNoName, nameName).bytes()));		
	}	
	
	@Test
	public void renameResourceEnvelopeSF_MediumMap() throws IOException {
		String nameName = "TESTTEST";
		String namedResourceBegin       = "0010D3A8CC000000D6F1F0F0F0F0F0F1";
		String namedResourceBeginResult = "0010D3A8CC000000E3C5E2E3E3C5E2E3";
		String namedResourceEnd       = "0010D3A9CC000000D6F1F0F0F0F0F0F1";
		String namedResourceEndResult = "0010D3A9CC000000E3C5E2E3E3C5E2E3";
		String namedResourceBeginWithFQN01       = "001CD3A8CC000000D6F1F0F0F0F0F0F10C020100D6F1F0F0F0F0F0F1";
		String namedResourceBeginWithFQN01Result = "001CD3A8CC000000E3C5E2E3E3C5E2E30C020100E3C5E2E3E3C5E2E3";		
		
		StructuredField sfnamedResourceBegin = new StructuredField(ByteHelpers.fromHexString(namedResourceBegin));
		assertEquals(namedResourceBeginResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBegin, nameName).bytes()));
		StructuredField sfnamedResourceEnd = new StructuredField(ByteHelpers.fromHexString(namedResourceEnd));
		assertEquals(namedResourceEndResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceEnd, nameName).bytes()));
		StructuredField sfnamedResourceBeginWithFQN01 = new StructuredField(ByteHelpers.fromHexString(namedResourceBeginWithFQN01));
		assertEquals(namedResourceBeginWithFQN01Result, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBeginWithFQN01, nameName).bytes()));		
	}		
	
	@Test
	public void renameResourceEnvelopeSF_ObjectContainer() throws IOException {
		String nameName = "TESTTEST";
		String namedResourceBegin       = "0010D3A892000000D6F1F0F0F0F0F0F1";
		String namedResourceBeginResult = "0010D3A892000000E3C5E2E3E3C5E2E3";
		String namedResourceEnd       = "0010D3A992000000D6F1F0F0F0F0F0F1";
		String namedResourceEndResult = "0010D3A992000000E3C5E2E3E3C5E2E3";
		String namedResourceBeginWithFQN01       = "001CD3A892000000D6F1F0F0F0F0F0F10C020100D6F1F0F0F0F0F0F1";
		String namedResourceBeginWithFQN01Result = "001CD3A892000000E3C5E2E3E3C5E2E30C020100E3C5E2E3E3C5E2E3";		
		
		StructuredField sfnamedResourceBegin = new StructuredField(ByteHelpers.fromHexString(namedResourceBegin));
		assertEquals(namedResourceBeginResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBegin, nameName).bytes()));
		StructuredField sfnamedResourceEnd = new StructuredField(ByteHelpers.fromHexString(namedResourceEnd));
		assertEquals(namedResourceEndResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceEnd, nameName).bytes()));
		StructuredField sfnamedResourceBeginWithFQN01 = new StructuredField(ByteHelpers.fromHexString(namedResourceBeginWithFQN01));
		assertEquals(namedResourceBeginWithFQN01Result, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBeginWithFQN01, nameName).bytes()));		
	}		
	
	@Test
	public void renameResourceEnvelopeSF_PageSegment() throws IOException {
		String nameName = "TESTTEST";
		String namedResourceBegin       = "0010D3A85F000000D6F1F0F0F0F0F0F1";
		String namedResourceBeginResult = "0010D3A85F000000E3C5E2E3E3C5E2E3";
		String namedResourceEnd       = "0010D3A95F000000D6F1F0F0F0F0F0F1";
		String namedResourceEndResult = "0010D3A95F000000E3C5E2E3E3C5E2E3";
		String namedResourceBeginWithFQN01       = "001CD3A85F000000D6F1F0F0F0F0F0F10C020100D6F1F0F0F0F0F0F1";
		String namedResourceBeginWithFQN01Result = "001CD3A85F000000E3C5E2E3E3C5E2E30C020100E3C5E2E3E3C5E2E3";		
		
		StructuredField sfnamedResourceBegin = new StructuredField(ByteHelpers.fromHexString(namedResourceBegin));
		assertEquals(namedResourceBeginResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBegin, nameName).bytes()));
		StructuredField sfnamedResourceEnd = new StructuredField(ByteHelpers.fromHexString(namedResourceEnd));
		assertEquals(namedResourceEndResult, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceEnd, nameName).bytes()));
		StructuredField sfnamedResourceBeginWithFQN01 = new StructuredField(ByteHelpers.fromHexString(namedResourceBeginWithFQN01));
		assertEquals(namedResourceBeginWithFQN01Result, ByteHelpers.toHexString(DocumentRenamer.renameResourceEnvelopeSf(sfnamedResourceBeginWithFQN01, nameName).bytes()));		
	}		
	
	@Test
	public void renameIMM() throws IOException {
		String originalName = "MM000001";
		String newName      = "TESTTEST";
		
		AfpInput input = new AfpInput();
		input.mediumMaps = new HashMap<>();
		MediumMapObj obj = new MediumMapObj();
		obj.name = originalName;
		obj.newName = null;
		input.mediumMaps.put(originalName, obj);
		
		String iim    = "0010D3ABCC000000D4D4F0F0F0F0F0F1";		
		assertEquals(iim, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(iim), input)));
		
		
		obj.newName = newName;
		input.mediumMaps.put(originalName, obj);
		String result = "0010D3ABCC000000E3C5E2E3E3C5E2E3";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(iim), input)));		
	}
	
	@Test
	public void renameIOB() throws IOException {
		String originalName = "MM000001";
		String newName      = "TESTTEST";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf    = "0010D3AFC3000000D4D4F0F0F0F0F0F1";		
		assertEquals(sf, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		
		obj.newName = newName;
		input.fileResources.put(originalName, obj);
		String result = "0010D3AFC3000000E3C5E2E3E3C5E2E3";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));		
	}
	
	@Test(expected = AugmentAfpException.class)
	public void renameIPG() throws IOException {
		String originalName = "blah";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf     = "0008D3AFAF000000";		
		DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input);
	}
	
	@Test
	public void renameIPO() throws IOException {
		String originalName = "O1000001";
		String newName      = "TESTTEST";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf     = "0018D3AFD8000000D6F1F0F0F0F0F0F10000D30007860000";		
		assertEquals(sf, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		
		obj.newName = newName;
		obj.type = ResourceObjectType.OVERLAY.getValue();
		input.fileResources.put(originalName, obj);
		String result = "0018D3AFD8000000E3C5E2E3E3C5E2E30000D30007860000";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));		
	}
	
	@Test
	public void renameIPS() throws IOException {
		String originalName = "O1000001";
		String newName      = "TESTTEST";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf     = "0016D3AF5F000000D6F1F0F0F0F0F0F10000D3000786";		
		assertEquals(sf, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		
		obj.newName = newName;
		obj.type = ResourceObjectType.PAGESEGMENT.getValue();
		input.fileResources.put(originalName, obj);
		String result = "0016D3AF5F000000E3C5E2E3E3C5E2E30000D3000786";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));		
	}
	
	@Test
	public void renameMCF() throws IOException {
		String originalName = "T1GMCUSR";
		String newName      = "TESTTEST";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf     = "003ED3AB8A00000000360C028500E3F1C7D4C3E4E2D90C028600C3F0F0F2C1E3D94004240501141F000000DC0000000000000000000000000000045D00DC";		
		assertEquals(sf, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		// codepage
		obj.newName = newName;
		input.fileResources.put(originalName, obj);
		String result = "003ED3AB8A00000000360C028500E3C5E2E3E3C5E2E30C028600C3F0F0F2C1E3D94004240501141F000000DC0000000000000000000000000000045D00DC";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		// charset
		input.fileResources.clear();		
		originalName = "C002ATR ";
		obj.name = originalName;
		obj.newName = newName;
		input.fileResources.put(originalName, obj);
		result        = "003ED3AB8A00000000360C028500E3F1C7D4C3E4E2D90C028600E3C5E2E3E3C5E2E304240501141F000000DC0000000000000000000000000000045D00DC";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		// codedfont
		input.fileResources.clear();		
		originalName = "X0H2100C";
		obj.name = originalName;
		obj.newName = newName;
		input.fileResources.put(originalName, obj);
		sf            = "002CD3AB8A00000000120C028E00E7F0C8F2F1F0F0C30424050100120C028E00E7F0C8F2F5F7F8F904240502";
		result        = "002CD3AB8A00000000120C028E00E3C5E2E3E3C5E2E30424050100120C028E00E7F0C8F2F5F7F8F904240502";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));		
	}	
	
	@Test(expected = AugmentAfpException.class)
	public void renameMCF1() throws IOException {
		String originalName = "C0100000";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf     = "0008D3B18A000000";		
		DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input);
	}
	
	@Test
	public void renameMDR() throws IOException {
		String originalName = "ABCDEFGH";
		String newName      = "TESTTEST";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf     = "002ED3ABC300000000260C028400C1C2C3C4C5C6C7C8181000410000000000000000000000000000000000000000";		
		assertEquals(sf, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		// Begin Resource Object Reference
		obj.newName = newName;
		input.fileResources.put(originalName, obj);
		String result = "002ED3ABC300000000260C028400E3C5E2E3E3C5E2E3181000410000000000000000000000000000000000000000";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		// Other Object Data Reference
		input.fileResources.clear();		
		obj.newName = newName;
		input.fileResources.put(originalName, obj);
		sf     = "002ED3ABC300000000260C02CE00C1C2C3C4C5C6C7C8181000410000000000000000000000000000000000000000";
		result = "002ED3ABC300000000260C02CE00E3C5E2E3E3C5E2E3181000410000000000000000000000000000000000000000";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		// Data Object External Resource Reference,
		input.fileResources.clear();		
		obj.newName = newName;
		input.fileResources.put(originalName, obj);
		sf     = "002ED3ABC300000000260C02DE00C1C2C3C4C5C6C7C8181000410000000000000000000000000000000000000000";
		result = "002ED3ABC300000000260C02DE00E3C5E2E3E3C5E2E3181000410000000000000000000000000000000000000000";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));		
	}
	
	@Test
	public void renameMMO() throws IOException {
		String originalName = "O1000001";
		String newName      = "TESTTEST";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf     = "0018D3B1DF0000000C00000001000000D6F1F0F0F0F0F0F1";		
		assertEquals(sf, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		
		obj.newName = newName;
		obj.type = ResourceObjectType.OVERLAY.getValue();
		input.fileResources.put(originalName, obj);
		String result = "0018D3B1DF0000000C00000001000000E3C5E2E3E3C5E2E3";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));		
	}
	
	@Test(expected = AugmentAfpException.class)
	public void renameMPG() throws IOException {
		AfpInput input = new AfpInput();
		String sf     = "001AD3ABAF00000000120C028400D6F1E2D9D3E3F0F104240201";
		assertEquals(sf, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
	}
	
	@Test
	public void renameMPO() throws IOException {
		String originalName = "O1SRLT01";
		String newName      = "TESTTEST";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf     = "001AD3ABD800000000120C028400D6F1E2D9D3E3F0F104240201";		
		assertEquals(sf, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		
		obj.newName = newName;
		obj.type = ResourceObjectType.OVERLAY.getValue();
		input.fileResources.put(originalName, obj);
		String result = "001AD3ABD800000000120C028400E3C5E2E3E3C5E2E304240201";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));		
	}
	
	@Test
	public void renameMPS() throws IOException {
		String originalName = "S1000001";
		String newName      = "TESTTEST";
		
		AfpInput input = new AfpInput();
		input.fileResources = new HashMap<>();
		ResourceObj obj = new ResourceObj();
		obj.name = originalName;
		obj.newName = null;
		input.fileResources.put(originalName, obj);
		
		String sf     = "0018D3B15F0000000C00000000000000E2F1F0F0F0F0F0F1";		
		assertEquals(sf, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));
		
		
		obj.newName = newName;
		obj.type = ResourceObjectType.PAGESEGMENT.getValue();
		input.fileResources.put(originalName, obj);
		String result = "0018D3B15F0000000C00000000000000E3C5E2E3E3C5E2E3";
		assertEquals(result, ByteHelpers.toHexString(DocumentRenamer.renameResourceBuffer(ByteHelpers.fromHexString(sf), input)));		
	}
}
