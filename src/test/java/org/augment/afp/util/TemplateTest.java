package org.augment.afp.util;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.augment.afp.data.InMemResourceDataStore;
import org.augment.afp.data.Page;
import org.augment.afp.data.PageGroup;
import org.augment.afp.data.PrintFileResourceGroup;
import org.augment.afp.data.formmap.FormMap;
import org.augment.afp.data.formmap.FormMapResource;
import org.augment.afp.data.formmap.MediumMap;
import org.augment.afp.data.formmap.PageDescriptor.SheetGroup;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class TemplateTest {

	@Test(expected = IllegalArgumentException.class)
	public void test_createMediumMap_NameError_Empty() throws IOException {
		Templates.createMediumMap(null, false, null, 0, 0);
	}
	
	@Test
	public void test_createMediumMap_Simplex() throws IOException {
		String expected = "0010D3A8CC000000E3C5E2E3404040400013D3B1AF000000010A0000000000000000000018D3A6880000000000096009600007F8000A5080036800000ED3A2880000000001000100010010D3A78800000001FF90009101F4010010D3A9CC000000E3C5E2E340404040";
		byte[] mm = Templates.createMediumMap("TEST", false, new Dpi(UnitBase.TEN_INCHES, 2400), 8.5, 11.0);
		assertEquals(expected, ByteHelpers.toHexString(mm));
	}
	
	@Test
	public void test_createMediumMap_Duplex() throws IOException {
		String expected = "0010D3A8CC000000E3C5E2E340404040001DD3B1AF000000010A0000000000000000000A0000000000000000010018D3A6880000000000096009600007F8000A50800368000014D3A2880000000001000100010002000200020010D3A78800000001FF90009101F4020010D3A78800000002FF90009101F4020010D3A9CC000000E3C5E2E340404040";
		byte[] mm = Templates.createMediumMap("TEST", true, new Dpi(UnitBase.TEN_INCHES, 2400), 8.5, 11.0);		
		assertEquals(expected, ByteHelpers.toHexString(mm));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_createInvokeMediumMap_NameError_Empty() throws IOException {
		Templates.createInvokeMediumMap(null);
	}
	
	@Test
	public void test_createInvokeMediumMap() throws IOException {
		String expected = "0010D3ABCC000000E3C5E2E340404040";
		byte[] imm = Templates.createInvokeMediumMap("TEST");
		assertEquals(expected, ByteHelpers.toHexString(imm));
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_createPage_PageName_Null() throws IOException {
		Templates.createPage(null, null, null, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_createPage_MediumMap_Null() throws IOException {
		Templates.createPage("PAGE1", null, null, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_createPage_SheetGroup_Null() throws IOException {
		FormMap formMap = FormMap.parse("dummy", new DataInputStream(new ByteArrayInputStream(Templates.createMediumMap("TEST", false, new Dpi(UnitBase.TEN_INCHES, 2400), 8.5, 11.0))));
		Templates.createPage("PAGE1", formMap.getMediumMap("TEST    "), null, null);
	}
	
	@Test
	public void test_createPage() throws IOException {
		FormMap formMap = FormMap.parse("dummy", new DataInputStream(new ByteArrayInputStream(Templates.createMediumMap("TEST", false, new Dpi(UnitBase.TEN_INCHES, 2400), 8.5, 11.0))));
		String pageName = "PAGE1";
		MediumMap map = formMap.getMediumMap("TEST    ");
		SheetGroup frontSheet = map.getPgp().getFrontSide();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		StructuredFieldOutputStream sos = new StructuredFieldOutputStream(baos, false);
		
		String expected = "001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF404040404040";
		Page page = Templates.createPage(pageName, map, frontSheet, map.getMdd());
		page.writeTo(sos);
		assertEquals(expected, ByteHelpers.toHexString(baos.toByteArray()));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_createPageGroup_PageGroupName_Null() throws IOException {
		Templates.createPageGroup(null, null, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_createPageGroup_Page_Null() throws IOException {
		Templates.createPageGroup("Group1", null, null);
	}	
	
	@Test(expected = IllegalArgumentException.class)
	public void test_createPageGroup_PrintFileResourceGroup_Null() throws IOException {
		FormMap formMap = FormMap.parse("dummy", new DataInputStream(new ByteArrayInputStream(Templates.createMediumMap("TEST", false, new Dpi(UnitBase.TEN_INCHES, 2400), 8.5, 11.0))));
		String pageName = "PAGE1";
		MediumMap map = formMap.getMediumMap("TEST    ");
		SheetGroup frontSheet = map.getPgp().getFrontSide();
		
		Templates.createPageGroup("Group1", Templates.createPage(pageName, map, frontSheet, map.getMdd()), null);
	}	
	
	@Test
	public void test_createPageGroup() throws IOException {
		byte[] mediumMapBytes = Templates.createMediumMap("TEST    ", false, new Dpi(UnitBase.TEN_INCHES, 2400), 8.5, 11.0);
		FormMap formMap = FormMap.parse("dummy", new DataInputStream(new ByteArrayInputStream(mediumMapBytes)));
		String pageName = "PAGE1";
		MediumMap map = formMap.getMediumMap("TEST    ");
		SheetGroup frontSheet = map.getPgp().getFrontSide();
		
		Page page = Templates.createPage(pageName, map, frontSheet, map.getMdd());
				
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		StructuredFieldOutputStream sos = new StructuredFieldOutputStream(baos, false);
		
		PrintFileResourceGroup resGroup = new PrintFileResourceGroup();
		InMemResourceDataStore resDataStore = new InMemResourceDataStore();
		resDataStore.store(map.getName(), mediumMapBytes);
		FormMapResource formMapRes = new FormMapResource("F1TEST", new InMemResourceDataStore(), formMap);
		resGroup.addResource(formMapRes);		
		
		String expected = "0018D3A8AD000000C7D9D6E4D7F1404008028D00E3C5E2E30010D3ABCC000000E3C5E2E340404040001BD3A8AF000000D7C1C7C5F140404008028D00E3C5E2E30381010008D3A8C90000000017D3A6AF0000000000096009600007F8000A500000000016D3B19B0000000000096009600007F8000A5000000008D3A9C90000000010D3A9AF000000FFFF4040404040400010D3A9AD000000FFFF404040404040";
		PageGroup group = Templates.createPageGroup("GROUP1", page, resGroup);
		group.load();
		group.writeTo(sos);
		assertEquals(expected, ByteHelpers.toHexString(baos.toByteArray()));
	}
}
