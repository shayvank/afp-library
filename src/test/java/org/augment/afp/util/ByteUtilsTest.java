package org.augment.afp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.Test;

import test.helpers.ByteHelpers;

public class ByteUtilsTest {

	@Test
	public void test_character_encoding() {
		ByteUtils.setDataEncoding(CharacterEncodingEnum.EBCDIC);
		assertSame(CharacterEncodingEnum.EBCDIC, ByteUtils.getDataEncoding());
	}
	
	@Test
	public void test_toModcaDocumentHex() {
		assertEquals("X'03'", ByteUtils.toModcaDocumentHex((byte)0x03));
		assertEquals("X'2D'", ByteUtils.toModcaDocumentHex((byte)0x2D));
	}
	
	@Test
	public void test_toUnsignedByte() {
		assertEquals(0x56, ByteUtils.toUnsignedByte((byte) 0x56));
		assertEquals(183, ByteUtils.toUnsignedByte((byte) -73));
	}
	
	@Test
	public void test_toShort() {
		assertEquals(11520, ByteUtils.toShort(new byte[] {0x2D, 0x00}));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_toShort_Null() {
		ByteUtils.toShort(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_toShort_NotEnoughBytes() {
		byte[] b = new byte[1];
		ByteUtils.toShort(b);
	}
	
	@Test
	public void test_fromShort() {
		assertEquals("2D00", ByteHelpers.toHexString(ByteUtils.fromShort((short) 11520)));
	}
	
	@Test
	public void test_toUnsignedShort() {
		assertEquals(16, ByteUtils.toUnsignedShort(new byte[] {0x00, 0x10}));
	}
	
	@Test
	public void test_toInt() {
		byte[] b = new byte[] {0x65, 0x10, (byte)0xF3, 0x29};
		assertEquals(1695609641, ByteUtils.toInt(b));
	}
	
	@Test
	public void test_toInt_LessThan4Bytes() {
		byte[] b = new byte[] {0x2D, 0x00};
		assertEquals(11520, ByteUtils.toInt(b));
	}	
	
	@Test(expected = IllegalArgumentException.class)
	public void test_toInt_MoreThan4Bytes_Error() {
		byte[] b = new byte[5];
		ByteUtils.toInt(b);
	}
	
	@Test
	public void test_from3ByteNumber() {
		assertEquals("000960", ByteHelpers.toHexString(ByteUtils.from3ByteNumber(2400)));
	}
	
	@Test
	public void test_arraycopy() {
		assertEquals(0, ByteUtils.arraycopy(null).length);
		assertEquals("0001", ByteHelpers.toHexString(ByteUtils.arraycopy(new byte[] {0x00, 0x01})));
		
		assertEquals(0, ByteUtils.arraycopy(null, 1, 2).length);
		assertEquals("0001", ByteHelpers.toHexString(ByteUtils.arraycopy(new byte[] {0x00, 0x00, 0x01}, 1, 2)));
	}
}
