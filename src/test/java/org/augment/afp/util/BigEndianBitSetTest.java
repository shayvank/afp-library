package org.augment.afp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import test.helpers.ByteHelpers;

public class BigEndianBitSetTest {

	@Test
	public void test_values() {
		BigEndianBitSet bs = new BigEndianBitSet();
		bs.set(0);
		assertEquals("80", ByteHelpers.toHexString(new byte[] {bs.toByte()}));
		assertTrue(bs.get(0));
		bs.set(0, false);
		assertFalse(bs.get(0));
		bs = BigEndianBitSet.valueOf(bs.toByte());
		bs.set(0, true);
		assertEquals("80", ByteHelpers.toHexString(new byte[] {bs.toByte()}));
	}
}
