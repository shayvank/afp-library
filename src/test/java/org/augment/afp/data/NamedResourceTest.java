package org.augment.afp.data;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.augment.afp.util.StructuredFieldOutputStream;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class NamedResourceTest {

	@Test
	public void test_operations() throws IOException {
		InMemResourceDataStore dataStore = new InMemResourceDataStore();
		
		NamedResource resource = new NamedResource("test", dataStore);
		assertEquals("test", resource.getName());
		
		String testField = "0010D3A8CC000000E3C5E2E3404040400010D3A8CC000000E3C5E2E340404040";
		
		dataStore.store("test", ByteHelpers.fromHexString(testField));
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StructuredFieldOutputStream sos = new StructuredFieldOutputStream(out, false);
		resource.writeTo(sos);
		assertEquals(testField, ByteHelpers.toHexString(out.toByteArray()));
		assertEquals(testField, ByteHelpers.toHexString(resource.getData()));
	}
	
	@Test
	public void test_writeTo_Null() throws IOException {
		InMemResourceDataStore dataStore = new InMemResourceDataStore();
		
		NamedResource resource = new NamedResource("test", dataStore);
						
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StructuredFieldOutputStream sos = new StructuredFieldOutputStream(out, false);
		resource.writeTo(sos);
		assertEquals(0, out.toByteArray().length);
	}
}
