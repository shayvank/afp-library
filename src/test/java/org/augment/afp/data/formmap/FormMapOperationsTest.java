package org.augment.afp.data.formmap;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.augment.afp.AfpFileReader;
import org.augment.afp.data.InMemPageGroupDataStore;
import org.augment.afp.data.InMemResourceDataStore;
import org.augment.afp.data.PrintFile;
import org.augment.afp.data.PrintFileBuilder;
import org.augment.afp.data.PrintFileResourceGroup;
import org.augment.afp.data.ResourceDataStore;
import org.augment.afp.util.Dpi;
import org.augment.afp.util.StructuredFieldOutputStream;
import org.augment.afp.util.Templates;
import org.augment.afp.util.UnitBase;
import org.junit.Before;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class FormMapOperationsTest {

	PrintFileResourceGroup resGroup;
	
	ByteArrayOutputStream baos;
	StructuredFieldOutputStream sos;
	
	@Before
	public void setup() throws IOException {
		baos = new ByteArrayOutputStream();
		sos = new StructuredFieldOutputStream(baos, false);
		
		String fileName = "testfile.afp";
		 
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());


		ResourceDataStore resourceDataStore = new InMemResourceDataStore();
		
		AfpFileReader afpFileReader = new AfpFileReader(file, new PrintFileBuilder(resourceDataStore, new InMemPageGroupDataStore()));
		
		PrintFile afpFile = afpFileReader.parse();
		
		resGroup = afpFile.getResourceGroup();
	}
	
	
	@Test
	public void test_addMediumMap() throws IOException {
		byte[] mediumMapBytes = Templates.createMediumMap("TEST    ", false, new Dpi(UnitBase.TEN_INCHES, 2400), 8.5, 11.0);
		FormMapResource formRes = (FormMapResource) resGroup.getResourceByName(resGroup.getFormMap().getName());
		formRes.addMediumMap(mediumMapBytes);
		resGroup.removeResource(formRes.getName());
		resGroup.addResource(formRes);
		resGroup.writeTo(sos);
		String expected = "005BD3A8C6000000FFFFFFFFFFFFFFFF116200F0F1F7F0F0F9F1F6F0F0F0F3F0F0060100000352170201002F686F6D652F7376616E6B6530312F6C747272170283002F686F6D652F7376616E6B6530312F6C7472640601000001F4001CD3A8CE000000C6F1C4C3D4D3F0F400000A21FE000000000000000008D3A8CD0000010045D3EEEE000002D7D7C6C161C1C9E72030392F31382F30392031313A323720463144434D4C303420504B33303734322049504D2F41495820342E322E302E3135302020200008D3A8C40000030013D3B1AF000004010A0000180000180000000018D3A688000005000009600960000000000000000368000008D3A9C40000060010D3A8CC000000E3C5E2E3404040400013D3B1AF000000010A0000000000000000000018D3A6880000000000096009600007F8000A5080036800000ED3A2880000000001000100010010D3A78800000001FF90009101F4010010D3A9CC000000E3C5E2E3404040400014D3A8CC000096C2D3C1D5D2D7C740044500020008D3A8C50000970013D3B1AF000099010A0000180000180000000018D3A68800009A00000960096000000000000000036800000ED3A28800009B000100010001001AD3A78800009C01FF0E009102D100E101F100F401F8FFFC020008D3A9C500009D0010D3A9CC00009EC2D3C1D5D2D7C7400008D3A9CD00009F0010D3A9CE000000C6F1C4C3D4D3F0F40010D3A9C6000000FFFFFFFFFFFFFFFF";
		assertEquals(expected, ByteHelpers.toHexString(baos.toByteArray()));
	}
	
	@Test
	public void test_removeMediumMap() throws IOException {
		FormMapResource formRes = (FormMapResource) resGroup.getResourceByName(resGroup.getFormMap().getName());
		formRes.removeMediumMap("BLANKPG ");
		resGroup.removeResource(formRes.getName());
		resGroup.addResource(formRes);
		resGroup.writeTo(sos);
		String expected = "005BD3A8C6000000FFFFFFFFFFFFFFFF116200F0F1F7F0F0F9F1F6F0F0F0F3F0F0060100000352170201002F686F6D652F7376616E6B6530312F6C747272170283002F686F6D652F7376616E6B6530312F6C7472640601000001F4001CD3A8CE000000C6F1C4C3D4D3F0F400000A21FE000000000000000008D3A8CD0000010045D3EEEE000002D7D7C6C161C1C9E72030392F31382F30392031313A323720463144434D4C303420504B33303734322049504D2F41495820342E322E302E3135302020200008D3A8C40000030013D3B1AF000004010A0000180000180000000018D3A688000005000009600960000000000000000368000008D3A9C40000060008D3A9CD00009F0010D3A9CE000000C6F1C4C3D4D3F0F40010D3A9C6000000FFFFFFFFFFFFFFFF";
		assertEquals(expected, ByteHelpers.toHexString(baos.toByteArray()));
	}
	
	
	/*
	 * 			// we need to create a new medium map for the header and trailer pages
			// and then add/replace the form map to the serialized objects
			byte[] newMediumMap = Templates.createMediumMap(prncldrMap, duplex, pageSize.getXDpi(), pageSize.getXSizeInUnits(), pageSize.getYSizeInUnits());
			FormMapResource formRes = ((FormMapResource)serializer.getPrintFileResourceGroup().getResourceByName(serializer.getPrintFileResourceGroup().getFormMap().getName()));
			formRes.addMediumMap(newMediumMap);
			serializer.getPrintFileResourceGroup().removeResource(formRes.getName());
			serializer.getPrintFileResourceGroup().addResource(formRes);
			map = group.getFormMap().getMediumMap(prncldrMap);
	 */
}
