package org.augment.afp.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class InMemPageGroupDataStoreTest {

	@Test
	public void test_operations() {
		InMemPageGroupDataStore store = new InMemPageGroupDataStore();
		byte[] test = new byte[] {0x00};
		String id = "id";
		store.store(id, test);
		assertEquals(test, store.get(id));
	}
}
