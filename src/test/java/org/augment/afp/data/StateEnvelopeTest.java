package org.augment.afp.data;

import static org.junit.Assert.assertEquals;

import org.augment.afp.util.StructuredField;
import org.junit.Test;

import test.helpers.ByteHelpers;

public class StateEnvelopeTest {

	String testField1 = "0010D3A8CC000000E3C5E2E340404040";
	String testField2 = "0010D3A9CC000000E3C5E2E340404040";
	
	@Test
	public void test_operations() {
		StructuredField field1 = new StructuredField(ByteHelpers.fromHexString(testField1));
		StructuredField field2 = new StructuredField(ByteHelpers.fromHexString(testField2));
		
		StateEnvelope envelope = new StateEnvelope(field1, field2);
		assertEquals(ByteHelpers.toHexString(field1.bytes()), ByteHelpers.toHexString(envelope.getBegin().bytes()));
		assertEquals(ByteHelpers.toHexString(field2.bytes()), ByteHelpers.toHexString(envelope.getEnd().bytes()));
		
		envelope.setEnd(field1);
		assertEquals(ByteHelpers.toHexString(field1.bytes()), ByteHelpers.toHexString(envelope.getEnd().bytes()));
	}
}
