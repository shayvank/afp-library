package org.augment.afp;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.augment.afp.data.InMemPageGroupDataStore;
import org.augment.afp.data.InMemResourceDataStore;
import org.augment.afp.data.PrintFile;
import org.augment.afp.data.PrintFileBuilder;
import org.augment.afp.data.ResourceDataStore;
import org.junit.Test;

public class AfpFileReaderTest {

	@Test
	public void test_parse_happy() throws IOException {
		String fileName = "testfile.afp";
		 
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());


		ResourceDataStore resourceDataStore = new InMemResourceDataStore();
		
		AfpFileReader afpFileReader = new AfpFileReader(file, new PrintFileBuilder(resourceDataStore, new InMemPageGroupDataStore()));
		
		PrintFile afpFile = afpFileReader.parse();
		assertEquals(1, afpFile.getResourceGroup().getResources().size());
		assertEquals(1, afpFile.getDocuments().size());
	}
}
