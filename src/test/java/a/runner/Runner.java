package a.runner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.augment.afp.AfpFileReader;
import org.augment.afp.data.InMemPageGroupDataStore;
import org.augment.afp.data.InMemResourceDataStore;
import org.augment.afp.data.Page;
import org.augment.afp.data.PrintFile;
import org.augment.afp.data.PrintFileBuilder;
import org.augment.afp.data.PrintFileDocumentGroup;
import org.augment.afp.request.comment.Comment;
import org.augment.afp.request.comment.FindCommentRequest;
import org.augment.afp.request.comment.InsertCommentRequest;
import org.augment.afp.request.datamatrix.DataMatrixBarcode;
import org.augment.afp.request.datamatrix.FindDataMatrixBarcodeRequest;
import org.augment.afp.request.datamatrix.InsertDataMatrixBarcodeRequest;
import org.augment.afp.request.datamatrix.RemoveDataMatrixBarcodeRequest;
import org.augment.afp.request.textoverlay.FindTextOverlayRequest;
import org.augment.afp.request.textoverlay.InsertTextOverlayRequest;
import org.augment.afp.request.textoverlay.RemoveTextOverlayRequest;
import org.augment.afp.request.textoverlay.TextOverlay;
import org.augment.afp.request.textoverlay.TrueTypeFont;
import org.augment.afp.request.tle.FindTleRequest;
import org.augment.afp.request.tle.InsertTleRequest;
import org.augment.afp.request.tle.RemoveTleRequest;
import org.augment.afp.request.tle.TaggedLogicalElement;
import org.augment.afp.util.Coordinate;
import org.augment.afp.util.StructuredFieldOutputStream;

public class Runner {

	public static void main(String [] args) {
		File file = new File("C:\\Users\\svanke01\\Documents\\PrinceDevelopment\\GitRepos\\afp-library\\examples\\dmsheader.afp");
				
		try (StructuredFieldOutputStream dos = new StructuredFieldOutputStream(new FileOutputStream("c:\\temp\\test.afp"))) {
			// create writer for enhanced afp file
			
			// create reader for input afp file
			AfpFileReader afpFileReader = new AfpFileReader(file, new PrintFileBuilder(new InMemResourceDataStore(), new InMemPageGroupDataStore()));
			
			
			// afp file object we will work with
			PrintFile afpFile = afpFileReader.parse();	
			
			// Write out the BPF
			afpFile.writeBeginEnvelopeTo(dos);
			
			// TODO: we will need to figure out a way of including additional resources in the resource group, this will help with writing out fonts for overlaytext
			// write out the print file resource group
			afpFile.getResourceGroup().writeTo(dos);
						
			for (PrintFileDocumentGroup document:  afpFile.getDocuments()) {
				// process each document group individually
				
				document.writeBeginEnvelopeTo(dos);
				
				// iterate over the page groups
				for (int i = 0; i < document.getPageGroupCount(); i++) {
					System.out.println("Page Group " + (i + 1));
					
					org.augment.afp.data.PageGroup pageGroup = document.getPageGroupData(i);
					pageGroup.load();
					
					
					// TLE Methods (pagegroup level)
					FindTleRequest request = FindTleRequest.findByName("BARCODE");

					pageGroup.execute(request);
					
					for (TaggedLogicalElement tle: request.getResult()) {
						System.out.println(tle.getQualifiedName() + ": " + tle.getAttrValue());
						RemoveTleRequest removeCmd = RemoveTleRequest.removeByName(tle.getQualifiedName());
						pageGroup.execute(removeCmd);
						if (removeCmd.isSuccess()) {
							System.out.println("remove tle successful");
						}
					}
					
					InsertTleRequest insertCmd = new InsertTleRequest(new TaggedLogicalElement("shay","test"));
					pageGroup.execute(insertCmd);
					if (insertCmd.isSuccess()) {
						System.out.println("insert tle successful");
					}
					
					// Comment Methods
					InsertCommentRequest insertComment = new InsertCommentRequest(new Comment("this is a comment!"));
					pageGroup.execute(insertComment);
					if (insertComment.isSuccess()) {
						System.out.println("insert comment successful");
					}

					FindCommentRequest findComment = FindCommentRequest.findByValue(".*");
					pageGroup.execute(findComment);
					for (Comment comment: findComment.getResult()) {
						System.out.println(comment.getValue());
					}
					
					for (int j = 0; j < pageGroup.getPageCount(); j++) {
						
						Page page = pageGroup.getPage(j);
						
						// TLE Methods (page level)
						InsertTleRequest insertPgTle = new InsertTleRequest(new TaggedLogicalElement("princebarcode","1234567890"));
						page.execute(insertPgTle);
						if (insertPgTle.isSuccess()) {
							System.out.println("insert page tle successful");
						}
						
						// DataMatrix Methods
						if (page.isFrontSide()) {
							DataMatrixBarcode.Builder builder = new DataMatrixBarcode.Builder();
							builder.withPayload("payloadhere");
							InsertDataMatrixBarcodeRequest barRequest = new InsertDataMatrixBarcodeRequest(builder.build());
							page.execute(barRequest);
							if (barRequest.isSuccess()) {
								System.out.println("insert datamatrix successful");
							}
						}
						
						FindDataMatrixBarcodeRequest findBarcode = FindDataMatrixBarcodeRequest.findByPayload(".*payload.*");
						page.execute(findBarcode);
						for (DataMatrixBarcode barcode: findBarcode.getResult()) {
							System.out.println(barcode.getPayload());
						}
						
						RemoveDataMatrixBarcodeRequest removeBarcodeRequest = RemoveDataMatrixBarcodeRequest.removeByPayload(".*DL01K117351.*");
						page.execute(removeBarcodeRequest);
						System.out.println("Removed barcode: " + removeBarcodeRequest.isSuccess());
						
						// TextOverlay Methods
						TextOverlay.Builder txtBuilder = new TextOverlay.Builder();
						txtBuilder.withText("000025 Text goes here");
						txtBuilder.withFont(new TrueTypeFont("Times New Roman Bold", 22));
						txtBuilder.withDescription("PrinCE MPSEQ");
						txtBuilder.withDirection(0);
						txtBuilder.withPosition(new Coordinate(3,3));
						InsertTextOverlayRequest txtRequest = new InsertTextOverlayRequest(txtBuilder.build());
						page.execute(txtRequest);
						if (txtRequest.isSuccess()) {
							System.out.println("insert textoverlay successful");
						}
						
						FindTextOverlayRequest findTextOverlay = FindTextOverlayRequest.findByText(".*000025.*");
						page.execute(findTextOverlay);
						for (TextOverlay textOverlay: findTextOverlay.getResult()) {
							System.out.println(textOverlay.getText());
						}
						
						RemoveTextOverlayRequest removeTextOverlayRequest = RemoveTextOverlayRequest.removeByDescription(".*PrinCE MPSEQ.*");
						page.execute(removeTextOverlayRequest);
						System.out.println("Removed textoverlay: " + removeTextOverlayRequest.isSuccess());
					}
						
					pageGroup.writeTo(dos);
					
					pageGroup.unload();
				}
				
				document.writeEndEnvelopeTo(dos);				
			}														
			
			afpFile.writeEndEnvelopeTo(dos);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
