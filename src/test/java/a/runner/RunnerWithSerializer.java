package a.runner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.augment.afp.AfpFileSerializer;
import org.augment.afp.data.Page;
import org.augment.afp.data.PageGroup;
import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.data.formmap.FormMapResource;
import org.augment.afp.data.formmap.MediumMap;
import org.augment.afp.request.blockout.Blockout;
import org.augment.afp.request.blockout.FindBlockoutRequest;
import org.augment.afp.request.blockout.InsertBlockoutRequest;
import org.augment.afp.request.blockout.RemoveBlockoutRequest;
import org.augment.afp.request.comment.Comment;
import org.augment.afp.request.comment.FindCommentRequest;
import org.augment.afp.request.comment.InsertCommentRequest;
import org.augment.afp.request.datamatrix.DataMatrixBarcode;
import org.augment.afp.request.datamatrix.FindDataMatrixBarcodeRequest;
import org.augment.afp.request.datamatrix.InsertDataMatrixBarcodeRequest;
import org.augment.afp.request.datamatrix.RemoveDataMatrixBarcodeRequest;
import org.augment.afp.request.textoverlay.FindTextOverlayRequest;
import org.augment.afp.request.textoverlay.InsertTextOverlayRequest;
import org.augment.afp.request.textoverlay.TextOverlay;
import org.augment.afp.request.textoverlay.TrueTypeFont;
import org.augment.afp.request.tle.FindTleRequest;
import org.augment.afp.request.tle.InsertTleRequest;
import org.augment.afp.request.tle.RemoveTleRequest;
import org.augment.afp.request.tle.TaggedLogicalElement;
import org.augment.afp.util.Coordinate;
import org.augment.afp.util.StructuredFieldOutputStream;
import org.augment.afp.util.Templates;

public class RunnerWithSerializer {

	public static void main(String [] args) {
		File file = new File("C:\\Users\\svanke01\\Documents\\PrinceDevelopment\\GitRepos\\afp-library\\examples\\dmsheader.afp");
				
		try (StructuredFieldOutputStream dos = new StructuredFieldOutputStream(new FileOutputStream("c:\\temp\\test.afp"));
				AfpFileSerializer afpFileSerializer = new AfpFileSerializer(file)) {
			// create writer for enhanced afp file
			
			// Write out the BPF
			afpFileSerializer.writeBeginPrintFileEnvelopeTo(dos);
									
			PageGroup pageGroup = null;

			boolean firstPageWritten = false;
			
			while ((pageGroup = afpFileSerializer.next()) != null) {
				pageGroup.load();
				
				if (!firstPageWritten) {
					Page firstPage = pageGroup.getPage(0);
					SizeDescriptor pgd = firstPage.getPgd();
					byte[] newMediumMap = Templates.createMediumMap("PRNCHDTR", false, pgd.getXDpi(), (double)pgd.getXSize() / pgd.getXDpi().getValue(), (double)pgd.getYSize() / pgd.getYDpi().getValue());
					
					FormMapResource formRes = ((FormMapResource)afpFileSerializer.getPrintFileResourceGroup().getResourceByName(afpFileSerializer.getPrintFileResourceGroup().getFormMap().getName()));
					formRes.addMediumMap(newMediumMap);
					afpFileSerializer.getPrintFileResourceGroup().removeResource(formRes.getName());
					afpFileSerializer.getPrintFileResourceGroup().addResource(formRes);
					afpFileSerializer.getPrintFileResourceGroup().writeTo(dos);
					
					afpFileSerializer.writeBeginDocumentEnvelopeTo(dos);
					
					SizeDescriptor sizeDescriptor = firstPage.getPgd();
					MediumMap blankpg = afpFileSerializer.getPrintFileResourceGroup().getFormMap().getMediumMap("PRNCHDTR");
					
					PageGroup pageGroup1 = Templates.createPageGroup("TESTGRP", Templates.createPage("Page1", blankpg, blankpg.getPgp().getFrontSide(), sizeDescriptor), afpFileSerializer.getPrintFileResourceGroup());
					pageGroup1.load();
					
					pageGroup1.writeTo(dos);
					pageGroup1.unload();
					firstPageWritten = true;
				}
				
				// TLE Methods (pagegroup level)
				FindTleRequest request = FindTleRequest.findByName("BARCODE");

				pageGroup.execute(request);
				
				for (TaggedLogicalElement tle: request.getResult()) {
					System.out.println(tle.getQualifiedName() + ": " + tle.getAttrValue());
					RemoveTleRequest removeCmd = RemoveTleRequest.removeByName(tle.getQualifiedName());
					pageGroup.execute(removeCmd);
					if (removeCmd.isSuccess()) {
						System.out.println("remove tle successful");
					}
				}
				
				InsertTleRequest insertCmd = new InsertTleRequest(new TaggedLogicalElement("shay","test"));
				pageGroup.execute(insertCmd);
				if (insertCmd.isSuccess()) {
					System.out.println("insert tle successful");
				}
				
				// Comment Methods
				InsertCommentRequest insertComment = new InsertCommentRequest(new Comment("this is a comment!"));
				pageGroup.execute(insertComment);
				if (insertComment.isSuccess()) {
					System.out.println("insert comment successful");
				}

				FindCommentRequest findComment = FindCommentRequest.findByValue(".*");
				pageGroup.execute(findComment);
				for (Comment comment: findComment.getResult()) {
					System.out.println(comment.getValue());
				}
				
				for (int j = 0; j < pageGroup.getPageCount(); j++) {
					
					Page page = pageGroup.getPage(j);
					
					// TLE Methods (page level)
					InsertTleRequest insertPgTle = new InsertTleRequest(new TaggedLogicalElement("princebarcode","1234567890"));
					page.execute(insertPgTle);
					if (insertPgTle.isSuccess()) {
						System.out.println("insert page tle successful");
					}
										
					// TextOverlay Methods
					TextOverlay.Builder txtBuilder = new TextOverlay.Builder();
					txtBuilder.withText("000025 Text goes here");
					txtBuilder.withFont(new TrueTypeFont("Times New Roman Bold", 22));
					txtBuilder.withDescription("PrinCE MPSEQ");
					txtBuilder.withDirection(0);
					txtBuilder.withPosition(new Coordinate(0,0.25));
					InsertTextOverlayRequest txtRequest = new InsertTextOverlayRequest(txtBuilder.build());
					page.execute(txtRequest);
					if (txtRequest.isSuccess()) {
						System.out.println("insert textoverlay successful");
					}
					
					FindTextOverlayRequest findTextOverlay = FindTextOverlayRequest.findByText(".*000025.*");
					page.execute(findTextOverlay);
					for (TextOverlay textOverlay: findTextOverlay.getResult()) {
						System.out.println(textOverlay.getText() + " " + textOverlay.getPosition().getX() + " " + textOverlay.getPosition().getY());						
					}
					
					//RemoveTextOverlayRequest removeTextOverlayRequest = RemoveTextOverlayRequest.removeByDescription(".*PrinCE MPSEQ.*");
					//page.execute(removeTextOverlayRequest);
					//System.out.println("Removed textoverlay: " + removeTextOverlayRequest.isSuccess());
					
					// DataMatrix Methods
					if (page.isFrontSide()) {
						DataMatrixBarcode.Builder builder = new DataMatrixBarcode.Builder();
						builder.withLocation(new Coordinate(7.48, 9.98));
						builder.withEbcdic2Ascii(true);
						builder.withResolution(240);
						builder.withGridSize(24, 24);
						builder.withElementSizeInMils(20);
						builder.withQuietZoneInElements(11);
						builder.withPayload("payloadhere");
						InsertDataMatrixBarcodeRequest barRequest = new InsertDataMatrixBarcodeRequest(builder.build());
						page.execute(barRequest);
						if (barRequest.isSuccess()) {
							System.out.println("insert datamatrix successful");
						}
					}
					
					FindDataMatrixBarcodeRequest findBarcode = FindDataMatrixBarcodeRequest.findByPayload(".*payload.*");
					page.execute(findBarcode);
					for (DataMatrixBarcode barcode: findBarcode.getResult()) {
						System.out.println(barcode.getPayload());
					}
					
					RemoveDataMatrixBarcodeRequest removeBarcodeRequest = RemoveDataMatrixBarcodeRequest.removeByPayload(".*DL01K117351.*");
					page.execute(removeBarcodeRequest);
					System.out.println("Removed barcode: " + removeBarcodeRequest.isSuccess());
					
					Blockout.Builder blockoutBuilder = new Blockout.Builder();
					//blockoutBuilder.withColor(Blockout.Color.WHITE.getCode());
					blockoutBuilder.withColor(9);
					blockoutBuilder.withDescription("Test");
					blockoutBuilder.withPosition(new Coordinate(2,3));
					blockoutBuilder.withSize(1.5, 4);
					InsertBlockoutRequest blockRequest = new InsertBlockoutRequest(blockoutBuilder.build());
					page.execute(blockRequest);
					if (blockRequest.isSuccess()) {
						System.out.println("insert blockout successful");
					}
					
					RemoveBlockoutRequest removeBlockoutRequest = RemoveBlockoutRequest.removeByDescription(".*Tes.*");
					page.execute(removeBlockoutRequest);
					System.out.println("Removed blockout: " + removeBlockoutRequest.isSuccess());
					
					FindBlockoutRequest findBlockout = FindBlockoutRequest.findByDescription(".*Tes.*");
					page.execute(findBlockout);
					for (Blockout blockout: findBlockout.getResult()) {
						System.out.println(blockout.getDescription() + " " + blockout.getColor() + " " + blockout.getWidth() + " " + blockout.getHeight() + " " + blockout.getPosition().getX() + " " + blockout.getPosition().getY());
					}
				}
					
				pageGroup.writeTo(dos);
				
				pageGroup.unload();
			}
				
			afpFileSerializer.writeEndDocumentEnvelopeTo(dos);																	
			afpFileSerializer.writeEndPrintFileEnvelopeTo(dos);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
