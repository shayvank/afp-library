package test.helpers;

import java.math.BigInteger;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;

public class ByteHelpers {

	public static byte[] fromHexString(final String src) {
	    byte[] biBytes = new BigInteger("10" + src.replaceAll("\\s", ""), 16).toByteArray();
	    return Arrays.copyOfRange(biBytes, 1, biBytes.length);
	}
	
	public static String toHexString(final byte[] bytes) {
		return DatatypeConverter.printHexBinary(bytes);
	}
}
