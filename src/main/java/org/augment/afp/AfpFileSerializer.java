package org.augment.afp;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;

import org.augment.afp.data.PageGroup;
import org.augment.afp.data.PrintFileResourceGroup;
import org.augment.afp.data.PrintFileSerializeBuilder;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredFieldFileParser;
import org.augment.afp.util.StructuredFieldOutputStream;
import org.augment.afp.util.TypeCode;

public class AfpFileSerializer extends AbstractFileHeader implements Closeable {
	
	private StructuredFieldFileParser reader;
	
	private File file;
	
	public AfpFileSerializer(final File file) throws IOException {
		super(new PrintFileSerializeBuilder());
		this.file = file;
		reader = new StructuredFieldFileParser(file);
		
		while (readNextField(reader) != null) {						
			if (field.getCategoryCode() == CategoryCode.PRINT_FILE && field.getTypeCode() == TypeCode.BEGIN) {
					fileBuilder.storeBPF(field);
			} else if (field.getTypeCode() == TypeCode.BEGIN && field.getCategoryCode() == CategoryCode.RESOURCE_GROUP) {
				fileBuilder.storeBRG(field);
				loadFileLevelResourceGroup(reader);
			} else if (field.getTypeCode() == TypeCode.BEGIN && field.getCategoryCode() == CategoryCode.DOCUMENT) {						
				fileBuilder.storeBDT(field);
				break;
			}
		}
	}
	
	public File getFile() {
		return file;
	}
	
	public void writeBeginPrintFileEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		fileBuilder.writeBeginEnvelopeTo(outputStream);
	}
	
	public void writeEndPrintFileEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		fileBuilder.writeEndEnvelopeTo(outputStream);
	}
	
	public PrintFileResourceGroup getPrintFileResourceGroup() {
		return ((PrintFileSerializeBuilder) fileBuilder).getResourceGroup();
	}
	
	public void writeBeginDocumentEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		((PrintFileSerializeBuilder) fileBuilder).writeBeginDocumentEnvelopeTo(outputStream);
	}

	public void writeEndDocumentEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		((PrintFileSerializeBuilder) fileBuilder).writeEndDocumentEnvelopeTo(outputStream);
	}
	
	public PageGroup next() throws IOException {
		PageGroup pageGroup = null;
		ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();
		try {
			while (readNextField(reader) != null) {
				if (field.getTypeCode() == TypeCode.BEGIN && field.getCategoryCode() == CategoryCode.PAGE_GROUP) {
					outputBuffer = new ByteArrayOutputStream();
					outputBuffer.write(field.bytes());
				} else if (field.getTypeCode() == TypeCode.END && field.getCategoryCode() == CategoryCode.PAGE_GROUP) {
					outputBuffer.write(field.bytes());
					fileBuilder.storePageGroup(outputBuffer.toByteArray());
					outputBuffer.close();
					pageGroup = ((PrintFileSerializeBuilder) fileBuilder).getPageGroup();
					break;
				} else if (field.getTypeCode() == TypeCode.END && field.getCategoryCode() == CategoryCode.DOCUMENT) {
					fileBuilder.storeEDT(field);
				} else if (field.getTypeCode() == TypeCode.END && field.getCategoryCode() == CategoryCode.PRINT_FILE) {
					fileBuilder.storeEPF(field);
				} else {
					outputBuffer.write(field.bytes());
				}
			}
		} catch(EOFException eof) {
			pageGroup = null;
		}
		
		return pageGroup;
	}

	@Override
	public void close() throws IOException {
		reader.close();
	}

}
