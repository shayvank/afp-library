package org.augment.afp.util;

/**
 * Structured field type codes. The type code identifies the function of the
 * structured field, such as begin, end, descriptor, or data.
 */
public enum TypeCode {
	
	/**
	 * Specific characteristics and points to an entry in the Font Patterns Map (FNM) structured field.
	 */
	FONT_INDEX(0x8C),
	/**
	 * An attribute structured field defines an attribute with parameters such
	 * as name and value.
	 */
	ATTRIBUTE(0xA0),
	/**
	 * A copy count structured field specifies groups of sheet copies, called
	 * copy subgroups, that are to be generated, and identifies modification
	 * control structured fields that specify modifications to be applied to
	 * each group.
	 */
	COPY_COUNT(0xA2),
	/**
	 * A descriptor structured field defines the initial characteristics and,
	 * optionally, the formatting directives for all objects, object areas, and
	 * pages. Depending on the specific descriptor structured field type, it may
	 * contain some set of parameters that identify:
	 * <ul>
	 * <li>The size of the page or object</li>
	 * <li>Measurement units</li>
	 * <li>Initial presentation conditions
	 * <li>
	 * </ul>
	 */
	DESCRIPTOR(0xA6),
	/**
	 * A control structured field specifies the type of modifications that are
	 * to be applied to a group of sheet copies, or a copy subgroup.
	 */
	CONTROL(0xA7),
	/**
	 * A begin structured field introduces and identifies a document component.
	 * In general, a begin structured field may contain a parameter that
	 * identifies the name of the component.
	 */
	BEGIN(0xA8),
	/**
	 * An end structured field identifies the end of a document component. In
	 * general, an end structured field may contain a parameter that identifies
	 * the name of the component.
	 */
	END(0xA9),
	/**
	 * A map structured field provides the following functions in the MO:DCA
	 * architecture:
	 * <ul>
	 * <li>All occurrences of a variable embedded in structured field parameter
	 * data can be given a new value by changing only one reference in the
	 * mapping, rather than having to physically change each occurrence. Thus
	 * all references to font X may cause a Times Roman font to be used in one
	 * instance and a Helvetica font in another instance merely by specifying
	 * the proper map coded font structured field.</li>
	 * <li>The presence of the map structured field in a MO:DCA environment
	 * group indicates use of the named resource within the scope of the
	 * environment group.</li>
	 * </ul>
	 */
	MAP(0xAB),
	/**
	 * A position structured field specifies the coordinate offset value and
	 * orientation for presentation spaces.
	 */
	POSITION(0xAC),
	/**
	 * A process structured field specifies processing to be performed on an
	 * object.
	 */
	PROCESS(0xAD),
	/**
	 * Applies to one character rotation of a font character set.
	 */
	FONT_ORIENTATION(0xAE),
	/**
	 * An include structured field selects a named resource which is to be
	 * embedded in the including data stream as if it appeared inline. External
	 * resource object names on the begin structured field may or may not
	 * coincide with the library name of that object, as library name resolution
	 * is outside the scope of the MO:DCA architecture.
	 */
	INCLUDE(0xAF),
	/**
	 * A table structured field contains a list of items of the same or similar
	 * type that are related to one another.
	 */
	TABLE(0xB0),
	/**
	 * A migration structured field is used to distinguish the MO:DCA structured
	 * field from a structured field with the same acronym from an earlier
	 * data-stream architecture. The earlier version is called Format 1. The
	 * MO:DCA version is called Format 2.
	 */
	MIGRATION(0xB1),
	/**
	 * A variable structured field defines or contains variable information.
	 */
	VARIABLE(0xB2),
	/**
	 * A link structured field defines a logical connection, or linkage, between
	 * two document components.
	 */
	LINK(0xB4),
	/**
	 * A data structured field consists of data whose meaning and interpretation
	 * is governed by the object architecture for the particular data object
	 * type.
	 */
	DATA(0xEE);

	private int code;

	private TypeCode(final int code) {
		this.code = code;
	}

	/**
	 * Returns the byte representation of this TypeCode.
	 * 
	 * @return integer byte
	 */
	public int toByte() {
		return this.code;
	}

	/**
	 * Returns the TypeCode value for the given code. If not found, null is
	 * returned.
	 * 
	 * @param inCode
	 *            byte code to convert to TypeCode
	 * @return TypeCode or null if not found
	 */
	public static TypeCode valueOf(final int inCode) {
		TypeCode found = null;
		for (TypeCode type : TypeCode.values()) {
			if (type.code == inCode) {
				found = type;
				break;
			}
		}
		return found;
	}
}
