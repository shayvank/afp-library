package org.augment.afp.util;

public class AugmentAfpException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AugmentAfpException(final String message) {
		super(message);
	}

	public AugmentAfpException(final Throwable exception) {
		super(exception);
	}

	public AugmentAfpException(final String message, final Throwable exception) {
		super(message, exception);
	}

}
