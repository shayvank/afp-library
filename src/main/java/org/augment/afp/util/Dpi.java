package org.augment.afp.util;

/**
 * Dpi is essentially a terribly named class for the print measurement used by MO:DCA.  Use this class
 * to define the Units of Measurement for those structured fields that require it.
 *
 */
public class Dpi {

	private UnitBase unitBase;
	private int unitsPerUnitBase;
	
	/**
	 * Creates a new Dpi from the provided unit base and units per unit base.
	 * 
	 * @param unitBase unit base
	 * @param unitsPerUnitBase units per unit base
	 */
	public Dpi(final UnitBase unitBase, final int unitsPerUnitBase) {
		this.unitBase = unitBase;
		this.unitsPerUnitBase = unitsPerUnitBase;
	}
	
	/**
	 * Returns the unit base, which represents the length of the measurement base.  Ten inches or ten centimeters.
	 * 
	 * @return unit base
	 */
	public UnitBase getUnitBase() {
		return unitBase;
	}
	
	/**
	 * Returns the units per unit base, which represents the number of units in the measurement base.  
	 * @return units per unit base
	 */
	public int getUnitsPerUnitBase() {
		return unitsPerUnitBase;
	}
	
	/**
	 * Returns the units in 1/10 the unit base.  (i.e 2400 units per unit base / unit base of 10 = 240 units)
	 *  
	 * @return value
	 */
	public int getValue() {
		return unitsPerUnitBase / 10;
	}		
}
