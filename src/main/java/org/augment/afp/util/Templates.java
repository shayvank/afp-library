package org.augment.afp.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.augment.afp.data.InMemPageGroupDataStore;
import org.augment.afp.data.Page;
import org.augment.afp.data.PageGroup;
import org.augment.afp.data.PrintFileResourceGroup;
import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.data.formmap.MediumMap;
import org.augment.afp.data.formmap.PageDescriptor.SheetGroup;

/**
 * This class provides a convenience for creating some basic MO:DCA objects.  No warranty that  these methods will work for
 * every situation.
 *
 */
public final class Templates {

	private Templates() {
		// static class
	}
	
	/**
	 * Creates a medium map given the provided values.
	 *  
	 * @param name name for the medium map (cannot be blank and should be unique)
	 * @param duplex duplex or not
	 * @param dpi "dots per inch" but really pels per inch
	 * @param pageWidth page width in the dpi (normally inches)
	 * @param pageHeight page height in the dpi (normally inches)
	 * @return byte array containing the medium map
	 * @throws IOException not likely to be thrown as all operations are ByteArrayOutputStream
	 */
	public static byte[] createMediumMap(final String name, final boolean duplex, final Dpi dpi, final double pageWidth, final double pageHeight) throws IOException {
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();
				
		Validations.notEmpty(name, "Medium Map names cannot be blank.");
		
		String namePad = name + "        ";
		String mediumMapName = namePad.substring(0,8);
		
		// BMM structured field
		int mmLength = 16; 
		sfBytes.write(ByteUtils.fromShort((short) mmLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.BEGIN.toByte());
		sfBytes.write(CategoryCode.MEDIUM_MAP.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(ByteUtils.getDataEncoding().byteValue(mediumMapName));
		
		
		// PGP structured field
		int pgLength = 19;
		if (duplex) {
			pgLength += 10;
		}
		sfBytes.write(ByteUtils.fromShort((short) pgLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.MIGRATION.toByte());
		sfBytes.write(CategoryCode.PAGE.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x01); // reserved constant
		// repeating group
		sfBytes.write(0x0A); // length of repeating group (10)
		sfBytes.write(ByteUtils.from3ByteNumber(0)); // x origin
		sfBytes.write(ByteUtils.from3ByteNumber(0)); // y origin
		sfBytes.write(Orientation.DEGREES_0.getByteValue()); // x orientation
		sfBytes.write(0x00); // front size
		if (duplex) {
			sfBytes.write(0x0A); // length of repeating group (10)
			sfBytes.write(ByteUtils.from3ByteNumber(0)); // x origin
			sfBytes.write(ByteUtils.from3ByteNumber(0)); // y origin
			sfBytes.write(Orientation.DEGREES_0.getByteValue()); // x orientation
			sfBytes.write(0x01); // front size			
		}
		
		// mdd structured field
		int mdLength = 24; 
		sfBytes.write(ByteUtils.fromShort((short) mdLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.DESCRIPTOR.toByte());
		sfBytes.write(CategoryCode.MEDIUM.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(dpi.getUnitBase().getByteValue());
		sfBytes.write(dpi.getUnitBase().getByteValue());		
		sfBytes.write(ByteUtils.fromShort((short) dpi.getUnitsPerUnitBase()));
		sfBytes.write(ByteUtils.fromShort((short) dpi.getUnitsPerUnitBase()));
		double width = pageWidth * dpi.getValue();
		double height = pageHeight * dpi.getValue();
		sfBytes.write(ByteUtils.from3ByteNumber((int) width));
		sfBytes.write(ByteUtils.from3ByteNumber((int) height));
		sfBytes.write(0x80); // mddflgs
		sfBytes.write(0x03); // triplet length
		sfBytes.write(0x68); // orientation triplet
		sfBytes.write(0x00); // portrait
		
		// mcc structured field
		int mcLength = 14;
		if (duplex) {
			mcLength += 6;
		}
		sfBytes.write(ByteUtils.fromShort((short) mcLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.COPY_COUNT.toByte());
		sfBytes.write(CategoryCode.MEDIUM.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(ByteUtils.fromShort((short) 1)); // starting copy number
		sfBytes.write(ByteUtils.fromShort((short) 1)); // ending copy number
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x01); // first MCCid
		if (duplex) {
			sfBytes.write(ByteUtils.fromShort((short) 2)); // starting copy number
			sfBytes.write(ByteUtils.fromShort((short) 2)); // ending copy number
			sfBytes.write(0x00); // reserved
			sfBytes.write(0x02); // second MCCid
		}
		
		// mmc		
		int mmcLength = 16;
		sfBytes.write(ByteUtils.fromShort((short) mmcLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.CONTROL.toByte());
		sfBytes.write(CategoryCode.MEDIUM.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x01); // MCCid
		sfBytes.write(0xFF); // constant
		sfBytes.write(new byte[] { (byte) 0x90, (byte) 0x00}); // media destination selector - high
		sfBytes.write(new byte[] { (byte) 0x91, (byte) 0x01}); // media destination selector - low
		sfBytes.write(0xF4);
		if (duplex) {
			sfBytes.write(0x02); // duplex
			sfBytes.write(ByteUtils.fromShort((short) mmcLength));
			sfBytes.write(0xd3);
			sfBytes.write(TypeCode.CONTROL.toByte());
			sfBytes.write(CategoryCode.MEDIUM.toByte());
			sfBytes.write(0x00); // flags
			sfBytes.write(0x00); // reserved
			sfBytes.write(0x00); // reserved
			sfBytes.write(0x02); // MCCid (second one)
			sfBytes.write(0xFF); // constant
			sfBytes.write(new byte[] { (byte) 0x90, (byte) 0x00}); // media destination selector - high
			sfBytes.write(new byte[] { (byte) 0x91, (byte) 0x01}); // media destination selector - low
			sfBytes.write(0xF4);
			sfBytes.write(0x02);
		} else {
			sfBytes.write(0x01); // simplex
		}
		
		// EMM structured field
		sfBytes.write(ByteUtils.fromShort((short) mmLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.END.toByte());
		sfBytes.write(CategoryCode.MEDIUM_MAP.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(ByteUtils.getDataEncoding().byteValue(mediumMapName));
		
		return sfBytes.toByteArray();
	}
	
	/**
	 * Creates an Invoke Medium Map structured field given the medium map name.
	 * 
	 * @param name name for the medium map (cannot be blank)
	 * @return byte array containing the invoke medium map structured field
	 * @throws IOException not likely to be thrown as all operations are ByteArrayOutputStream
	 */
	public static byte[] createInvokeMediumMap(final String name) throws IOException {
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();
		
		Validations.notEmpty(name, "Medium Map names cannot be blank.");
				
		String namePad = name + "        ";
		String mediumMapName = namePad.substring(0,8);
		
		// BMM structured field
		int mmLength = 16;
		sfBytes.write(ByteUtils.fromShort((short) mmLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.MAP.toByte());
		sfBytes.write(CategoryCode.MEDIUM_MAP.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(ByteUtils.getDataEncoding().byteValue(mediumMapName));
		
		return sfBytes.toByteArray();
	}
	
	/**
	 * Creates a blank page given the medium map and sheet side.
	 * 
	 * @param pageName name for the page
	 * @param mediumMap medium map to be used for this page
	 * @param sheetSide the sheet side (front or back) from the medium map
	 * @param pageDescriptor the size of the page
	 * @return byte array containing the blank page
	 * @throws IOException not likely to be thrown as all operations are ByteArrayOutputStream
	 */
	public static Page createPage(final String pageName, final MediumMap mediumMap, final SheetGroup sheetSide, final SizeDescriptor pageDescriptor) throws IOException {
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();
		
		Validations.notNull(pageName, "Page name is required for creating a page.");
		Validations.notNull(mediumMap, "MediumMap is required for creating a page.");
		Validations.notNull(sheetSide, "SheetGroup is required for creating a page.");
		Validations.notNull(pageDescriptor, "PageDescriptor is required for creating a page.");
				
		// BPG structured field
		String namePad = pageName + "        ";
		String beginPageName = namePad.substring(0,8);
		
		String mediumMapName = mediumMap.getName().trim();
		int fqnMapLen = mediumMapName.length() + 4;
		int sfLength = 16 + fqnMapLen + 3;
		sfBytes.write(ByteUtils.fromShort((short) sfLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.BEGIN.toByte());
		sfBytes.write(CategoryCode.PAGE.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(ByteUtils.getDataEncoding().byteValue(beginPageName));		
		sfBytes.write(((Integer)fqnMapLen).byteValue());
		sfBytes.write(0x02);
		sfBytes.write(0x8D);
		sfBytes.write(0x00);
		sfBytes.write(ByteUtils.getDataEncoding().byteValue(mediumMapName));
		sfBytes.write(0x03);
		sfBytes.write(0x81);
		sfBytes.write(sheetSide.isBackSide() ? 0x02 : 0x01);
		
		
		// BAG structured field
		sfLength = 8;
		sfBytes.write(ByteUtils.fromShort((short) sfLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.BEGIN.toByte());
		sfBytes.write(CategoryCode.ACTIVE_ENVIRONMENT_GROUP.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		
		// PGD structured field
		int pgdLen = 23;
		sfBytes.write(ByteUtils.fromShort((short) pgdLen));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.DESCRIPTOR.toByte());
		sfBytes.write(CategoryCode.PAGE.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(pageDescriptor.getXDpi().getUnitBase().getByteValue());
		sfBytes.write(pageDescriptor.getYDpi().getUnitBase().getByteValue());
		sfBytes.write(ByteUtils.fromShort((short) pageDescriptor.getXDpi().getUnitsPerUnitBase()));
		sfBytes.write(ByteUtils.fromShort((short) pageDescriptor.getYDpi().getUnitsPerUnitBase()));
		sfBytes.write(ByteUtils.from3ByteNumber(pageDescriptor.getXSize()));
		sfBytes.write(ByteUtils.from3ByteNumber(pageDescriptor.getYSize()));
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		
		// PTD structured field
		int ptdLen = 22;
		sfBytes.write(ByteUtils.fromShort((short) ptdLen));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.MIGRATION.toByte());
		sfBytes.write(CategoryCode.PRESENTATION_TEXT.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(pageDescriptor.getXDpi().getUnitBase().getByteValue());
		sfBytes.write(pageDescriptor.getYDpi().getUnitBase().getByteValue());
		sfBytes.write(ByteUtils.fromShort((short) pageDescriptor.getXDpi().getUnitsPerUnitBase()));
		sfBytes.write(ByteUtils.fromShort((short) pageDescriptor.getYDpi().getUnitsPerUnitBase()));
		sfBytes.write(ByteUtils.from3ByteNumber(pageDescriptor.getXSize()));
		sfBytes.write(ByteUtils.from3ByteNumber(pageDescriptor.getYSize()));
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		
		// EAG structured field
		sfBytes.write(ByteUtils.fromShort((short) sfLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.END.toByte());
		sfBytes.write(CategoryCode.ACTIVE_ENVIRONMENT_GROUP.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		
		// EPG structured field
		sfLength = 16;
		sfBytes.write(ByteUtils.fromShort((short) sfLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.END.toByte());
		sfBytes.write(CategoryCode.PAGE.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(0xFF);
		sfBytes.write(0xFF);
		sfBytes.write(ByteUtils.getDataEncoding().byteValue("      "));
		
		return new Page(sfBytes.toByteArray(), mediumMap, sheetSide);
	}
	
	public static PageGroup createPageGroup(final String pageGroupName, final Page page, final PrintFileResourceGroup printFileResourceGroup) throws IOException {
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();
		
		Validations.notNull(pageGroupName, "Page Group name is required for creating a page group.");
		Validations.notNull(page, "Page is required for creating a page group.");
		Validations.notNull(printFileResourceGroup, "PrintFileResourceGroup is required for creating a page group.");
		
		// BNG structured field
		String namePad = pageGroupName + "        ";
		String beginPageGroupName = namePad.substring(0,8);
		
		String mediumMapName = page.getMediumMap().getName().trim();
		int fqnMapLen = mediumMapName.length() + 4;		
		int sfLength = 16 + fqnMapLen;
		sfBytes.write(ByteUtils.fromShort((short) sfLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.BEGIN.toByte());
		sfBytes.write(CategoryCode.PAGE_GROUP.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(ByteUtils.getDataEncoding().byteValue(beginPageGroupName));
		sfBytes.write(((Integer)fqnMapLen).byteValue());
		sfBytes.write(0x02);
		sfBytes.write(0x8D);
		sfBytes.write(0x00);
		sfBytes.write(ByteUtils.getDataEncoding().byteValue(mediumMapName));
		
		sfBytes.write(new StructuredField(Templates.createInvokeMediumMap(page.getMediumMap().getName())).bytes());
		
		page.writeTo(new StructuredFieldOutputStream(sfBytes, false));
		
		// EPG structured field
		sfLength = 16;
		sfBytes.write(ByteUtils.fromShort((short) sfLength));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.END.toByte());
		sfBytes.write(CategoryCode.PAGE_GROUP.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(0xFF);
		sfBytes.write(0xFF);
		sfBytes.write(ByteUtils.getDataEncoding().byteValue("      "));
		
		String id = "pagegroup";
		InMemPageGroupDataStore pageGroupStore = new InMemPageGroupDataStore();
		pageGroupStore.store(id, sfBytes.toByteArray());
		return new PageGroup(id, pageGroupStore, printFileResourceGroup);
		
	}
	

}
