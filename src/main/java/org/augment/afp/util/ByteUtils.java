package org.augment.afp.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Utility methods for working with byte data. 
 *
 */
public class ByteUtils {

	private ByteUtils() {
		// private constructor
	}
	
	private static CharacterEncoding encodingForData = CharacterEncodingEnum.EBCDIC;
	
	/**
	 * Returns the currently configured data encoding value.
	 * 
	 * @return data encoding value
	 */
	public static CharacterEncoding getDataEncoding() {
		return encodingForData;
	}
	
	/**
	 * Sets the data encoding value to be used for variable data.
	 * 
	 * @param dataEncoding data encoding value
	 */
	public static void setDataEncoding(final CharacterEncoding dataEncoding) {
		encodingForData = dataEncoding;
	}
	
	/**
	 * Returns a byte in the X'xx' format for messages and human-readable errors.
	 * 
	 * @param value byte value
	 * @return X'xx' formatted value
	 */
	public static String toModcaDocumentHex(final byte value) {
		String foundHex = "0" + Integer.toHexString(value).toUpperCase();
		return "X'" + (foundHex.length() == 2 ? foundHex : foundHex.substring(1)) + "'";
	}
	
	/**
	 * Returns the unsigned version of the provided byte.
	 * 
	 * @param b input byte
	 * @return unsigned byte
	 */
	public static int toUnsignedByte(final byte b) {
		return (b & 0xff);
	}
	
	/**
	 * Converts a short defined in a byte array to an integer.
	 * 
	 * @param shortBytes short byte array
	 * @return integer value
	 */
	public static int toShort(final byte[] shortBytes) {
		Validations.notNull(shortBytes, "Byte array cannot be null when converting to short.");
		if (shortBytes.length != 2) {
			throw new IllegalArgumentException("Byte array must be 2 bytes in order to convert to short.");
		} else {
			ByteBuffer buffer = ByteBuffer.allocate(2);
			buffer.put(shortBytes);
			buffer.flip();
			return buffer.getShort();
		}
	}
	
	/**
	 * Converts a short value and returns the byte array representation.
	 * 
	 * @param number short value
	 * @return byte array
	 */
	public static byte[] fromShort(final short number) {
		ByteBuffer buf = ByteBuffer.allocate(2);
		buf.order(ByteOrder.BIG_ENDIAN);
		buf.putShort(number);
		return buf.array();
	}
	
	/**
	 * Returns the unsigned version of the provided short byte array.
	  
	 * @param shortBytes signed short byte array
	 * @return unsigned short integer
	 */
	public static int toUnsignedShort(final byte[] shortBytes) {
		return (toShort(shortBytes) & 0xffff);
	}
	
	/**
	 * Converts the provided signed byte array to an integer.  Handles up to a 4 byte array.
	 * 
	 * @param signedBytes byte array
	 * @return integer value
	 */
	public static int toInt(final byte[] signedBytes) {
		byte[] valueArray = signedBytes;
		if (valueArray.length < 4) {
			valueArray = new byte[4];
			System.arraycopy(signedBytes, 0, valueArray, 4 - signedBytes.length, signedBytes.length);
		} else if (valueArray.length > 4) {
			throw new IllegalArgumentException("This method only supports byte arrays of 4 bytes maximum.");
		}
		ByteBuffer buf = ByteBuffer.allocate(4);
		buf.put(valueArray);
		buf.flip();
		return buf.getInt(0);
	}
	
	/**
	 * Converts a number into a 3-byte array representation.
	 * 
	 * @param number input number
	 * @return 3-byte array
	 */
	public static byte[] from3ByteNumber(final int number) {
		return new byte[] {
				(byte) (number >> 16),
				(byte) (number >> 8),
				(byte) number
		};
	}
	
	/**
	 * Makes a copy of the provided byte array.
	 * 
	 * @param array input array
	 * @return copy of the array
	 */
	public static byte[] arraycopy(final byte[] array) {
		if (array == null) {
			return new byte[0];
		}

		return arraycopy(array, 0, array.length);
	}
	
	/**
	 * Makes a copy of the provided byte array, starting at the start position, and copying the number of bytes
	 * in the length.
	 * 
	 * @param array input array 
	 * @param start starting position
	 * @param length number of bytes to copy
	 * @return copy of the array based on input parameters
	 */
	public static byte[] arraycopy(final byte[] array, final int start, final int length) {
		if (array == null) {
			return new byte[0];
		}
		byte[] copy = new byte[length];
		System.arraycopy(array, start, copy, 0, length);
		return copy;
	}
}
