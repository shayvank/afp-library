package org.augment.afp.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class NamedResourceTemplate {
	
	private NamedResourceTemplate() {
		// private constructor
	}

	public static byte[] createNamedResource(final String name, final ResourceObjectType objType, final byte[] resObject) throws IOException {
		String fixedName = name + "        ";
		fixedName = fixedName.substring(0,8);
		
		ByteArrayOutputStream res = new ByteArrayOutputStream();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos.write(objType.getValue());
		baos.write(new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
		
		// Begin Resource
		res.write(StructuredFieldBuilder.createBuilder(TypeCode.BEGIN, CategoryCode.NAMED_RESOURCE)
					.addData(ByteUtils.getDataEncoding().byteValue(fixedName))
					.addData(0x00)
					.addData(0x00)
					.createTripletBuilder(0x21) //Resource Object Type Triplet
						.addTripletData(baos.toByteArray())
						.addTriplet()
					.build().bytes());
		
		res.write(resObject);
		
		// End Resource
		res.write(StructuredFieldBuilder.createBuilder(TypeCode.END, CategoryCode.NAMED_RESOURCE)
					.addData(ByteUtils.getDataEncoding().byteValue(fixedName))
					.build().bytes());
		
		return res.toByteArray();
	}

}
