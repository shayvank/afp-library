package org.augment.afp.util;

public enum ResourceObjectType {
	
	GRAPHIC(0X03,'G'),
	BARCODE(0X05,'B'),
	IMAGE(0X06, 'I'),
	FONTCHARACTERSET(0X40, 'C'),
	CODEPAGE(0X41, 'T'),
	CODEDFONT(0X42, 'X'),
	OBJECTCONTAINER(0X92,'Z'),
	DOCUMENT(0XA8, 'D'),
	PAGESEGMENT(0XFB, 'S'),
	OVERLAY(0XFC, 'O'),
	PAGEDEF(0XFD, 'P'),
	FORMDEF(0XFE, 'F');
	
	private int value;
	private char prefix;
	
	private ResourceObjectType(final int value, final char prefix) {
		this.value = value;
		this.prefix = prefix;
	}
	
	public int getValue() {
		return value;
	}
	
	public char getFileNamePrefix() {
		return prefix;
	}
	
	public static ResourceObjectType findByValue(final int value) {
		ResourceObjectType returnValue = null;
		for (ResourceObjectType type: ResourceObjectType.values()) {
			if (type.getValue() == value) {
				returnValue = type;
				break;
			}
		}
		return returnValue;
	}
}