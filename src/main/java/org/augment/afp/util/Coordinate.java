package org.augment.afp.util;

/**
 * MO:DCA uses an orthogonal coordinate system based on the fourth quadrant of a
 * standard Cartesian coordinate system.  Both the X axis and Y axis specify positive values, which 
 * is a difference from teh Cartesian system where the Y axis in the fourth quadrant specifies negative values.
 *
 */
public class Coordinate {
	
	private double x;
	private double y;
	
	/**
	 * Create a new coordinate value based on the provided x and y axis.
	 * 
	 * @param x axis
	 * @param y axis
	 */
	public Coordinate(final double x, final double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Returns the x axis value.
	 * 
	 * @return x axis
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Returns the y axis value.
	 * 
	 * @return y axis
	 */
	public double getY() {
		return y;
	}
}
