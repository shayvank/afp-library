package org.augment.afp.util;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Supports writing out StructuredFields to the provided outputstream.
 *
 */
public class StructuredFieldOutputStream extends OutputStream {

	private OutputStream out;
	private boolean ccChar;
	
	/**
	 * Creates a new instance with the optional control character flag.
	 * 
	 * @param outputStream outputstream
	 * @param ccChar true to output 5A cc character, false otherwise
	 */
	public StructuredFieldOutputStream(final OutputStream outputStream, final boolean ccChar) {
		this.out = outputStream;
		this.ccChar = ccChar;
	}
	
	/**
	 * Creates a new instance with 5A cc characters getting provided when writing structured fields.
	 * 
	 * @param outputStream outputstream
	 */
	public StructuredFieldOutputStream(final OutputStream outputStream) {
		this(outputStream, true);
	}
	
	@Override
	public void close() throws IOException {
		flush();
		out.close();
	}
	
	@Override
	public void flush() throws IOException {
		out.flush();
	}
	
	@Override
	public void write(int b) throws IOException {
		out.write(b);
	}
	
	@Override
	public void write(byte[] buf) throws IOException {
		write(buf, 0, buf.length);
	}
	
	@Override
	public void write(byte[] buf, int offset, int len) throws IOException {
		for (int i=0; i < len; i++) {
			write(buf[offset + i]);
		}
	}
	
	/**
	 * Writes out a structured field to the outputstream.
	 * 
	 * @param field structuredfield to write
	 * @throws IOException if any io exception occurs
	 */
	public void writeStructuredField(final StructuredField field) throws IOException {
		if (ccChar) {
			out.write(0x5A);
		}
		out.write(field.bytes());
	}

}
