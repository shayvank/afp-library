package org.augment.afp.util;

/**
 * The structured field category code. It identifies the lowest-level component
 * that can be constructed using the structured field, such as document, active
 * environment group, page, or object. The same category code point assigned to
 * a component's begin structured field also is assigned to that component's end
 * structured field. These code points identify and delimit an entire component
 * within a data stream or an encompassing component.
 *
 */
public enum CategoryCode {

	PAGE_SEGMENT(0x5F),
	OBJECT_AREA(0x6B),
	COLOR_ATTRIBUTE_TABLE(0x77),
	IM_IMAGE(0x7B),
	CODE_PAGE(0x87),
	MEDIUM(0x88),
	FONT(0x89),
	CODED_FONT(0x8A),
	PROCESS_ELEMENT(0x90),
	OBJECT_CONTAINER(0x92),
	PRESENTATION_TEXT(0x9B),
	PRINT_FILE(0xA5),
	INDEX(0xA7),
	DOCUMENT(0xA8),
	PAGE_GROUP(0xAD),
	PAGE(0xAF),
	GRAPHICS(0xBB),
	DATA_RESOURCE(0xC3),
	DOCUMENT_ENVIRONMENT_GROUP(0xC4),
	RESOURCE_GROUP(0xC6),
	OBJECT_ENVIRONMENT_GROUP(0xC7),
	ACTIVE_ENVIRONMENT_GROUP(0xC9),
	MEDIUM_MAP(0xCC),
	FORM_MAP(0xCD),
	NAMED_RESOURCE(0xCE),
	PAGE_OVERLAY(0xD8),
	RESOURCE_ENVIRONMENT_GROUP(0xD9),
	OVERLAY(0xDF),
	DATA_SUPPRESSION(0xEA),
	BAR_CODE(0xEB),
	NO_OPERATION(0xEE),
	IMAGE(0xFB);
	
	private int code;

	private CategoryCode(final int code) {
		this.code = code;
	}

	/**
	 * Returns the byte representation of this CategoryCode.
	 * 
	 * @return integer byte
	 */
	public int toByte() {
		return this.code;
	}

	/**
	 * Returns the CategoryCode value for the given code. If not found, null is
	 * returned.
	 * 
	 * @param inCode
	 *            byte code to convert to CategoryCode
	 * @return CategoryCode or null if not found
	 */
	public static CategoryCode valueOf(final int inCode) {
		CategoryCode found = null;
		for (CategoryCode type : CategoryCode.values()) {
			if (type.code == inCode) {
				found = type;
				break;
			}
		}
		return found;
	}
}