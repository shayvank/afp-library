package org.augment.afp.util;

import java.io.DataInput;
import java.io.IOException;

/**
 * A Structured Field is the core data structure used in MO:DCA data stream.  They are used to envelop
 * document components and provide commands and information to applications using the data stream.
 *
 */
public class StructuredField {

	private static final int INTRODUCER_LENGTH = 8;
	
	private byte[] data;
	
	/**
	 * Creates a Structured Field from the entire structured field data (including introducer).
	 * @param data structured field data
	 */
	public StructuredField(final byte[] data) {
		this.data = data;
	}

	/**
	 * Returns the Type Code definition for this structured field.
	 * 
	 * @return type code
	 */
	public TypeCode getTypeCode() {
		return TypeCode.valueOf(ByteUtils.toUnsignedByte(bytes()[3]));
	}
	
	/**
	 * Returns the Category Code definition for this structured field.
	 * 
	 * @return category code
	 */
	public CategoryCode getCategoryCode() {
		return CategoryCode.valueOf(ByteUtils.toUnsignedByte(bytes()[4]));
	}
	
	/**
	 * Returns the length of this structured field including the the length of the introducer.
	 * 
	 * @return length of structured field
	 */
	public int getLength() {
		byte[] length = new byte[2];
		System.arraycopy(data, 0, length, 0, 2);
		return ByteUtils.toUnsignedShort(length);
	}
	
	/**
	 * Returns the full byte array for the structured field (including introducer).
	 * 
	 * @return byte array of structured field
	 */
	public byte[] bytes() {
		return data;
	}
	
	/**
	 * Returns the data section of the structure field (does not contain the introducer).
	 * 
	 * @return data byte array
	 */
	public byte[] getData() {
		int dataLen = getLength() - INTRODUCER_LENGTH;
		byte[] returnData = new byte[dataLen];
		if (dataLen > 0) {
			System.arraycopy(bytes(), 8, returnData, 0, dataLen);
		}
		
		return returnData;
	}
	
	/**
	 * Given the data input, parses the next structured field.
	 * 
	 * @param dataInput MO:DCA stream data as DataInput
	 * @return next structured field in the stream
	 * @throws IOException if any io issues occur with DataInput
	 */
	public static StructuredField getNext(final DataInput dataInput) throws IOException {
		// now, read the length bytes.
		byte[] lengthBytes = new byte[2];
		dataInput.readFully(lengthBytes);
		int sfLength = ByteUtils.toUnsignedShort(lengthBytes);
		
		byte[] sfBytes = new byte[sfLength];
		System.arraycopy(lengthBytes, 0, sfBytes, 0, lengthBytes.length);
		dataInput.readFully(sfBytes, 2, sfLength-2);
		
		validateStructuredFieldIntroducer(sfBytes);
		
		return new StructuredField(sfBytes);
	}
	
	/**
	 * Validates the structured field introducer.
	 * 
	 * @param structuredfield field to be validated
	 */
	private static void validateStructuredFieldIntroducer(final byte[] structuredfield) {
		// the first 2 bytes were the length, that is how we knew to read the field
		// the third byte is the modca class code (D3)
		if (ByteUtils.toUnsignedByte(structuredfield[2]) != 0xD3) {
			throw new IllegalArgumentException("Structured Field is not set to MODCA class code D3.");
		}
		// else validate type code ??
		// else validate category code ??
	}
	
}
