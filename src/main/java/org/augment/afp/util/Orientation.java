package org.augment.afp.util;

/**
 * Orientation refers to the rotation of a document component and its coordinate system with respect to the coordinate system
 * that contains it.
 *
 */
public enum Orientation {

	/**
	 * Normal Across.
	 */
	DEGREES_0(0, 0, new byte[] {0x00, 0x00}),
	/**
	 * Down.
	 */
	DEGREES_90(90, 11520, new byte[] {0x2D, 0x00}),
	/**
	 * Back.
	 */
	DEGREES_180(180, 23040, new byte[] {0x5A, 0x00}),
	/**
	 * Up.
	 */
	DEGREES_270(270, 34560, new byte[] {(byte) 0x87, 0x00});
	
	private int intValue;
	private int shValue;
	private byte[] byteValue;
	
	Orientation(final int intValue, final int shValue, final byte[] byteValue) {
		this.intValue = intValue;
		this.shValue = shValue;
		this.byteValue = byteValue;
	}
	
	/**
	 * Returns the int value representation of the orientation.
	 * 
	 * @return int value
	 */
	public int getIntValue() {
		return intValue;
	}
	
	/**
	 * Returns the short value representation of the orientation.
	 * 
	 * @return short value
	 */
	public int getShortValue() {
		return shValue;
	}
	
	/**
	 * Returns the byte value representation of the orientation.
	 * 
	 * @return byte value
	 */
	public byte[] getByteValue() {
		return byteValue;
	}
	
	/**
	 * Returns the Orientation given the provided short value.
	 * 
	 * @param value short value
	 * @return Orientation for short value
	 */
	public static Orientation findByValue(final int value) {
		if (value == DEGREES_0.shValue) {
			return DEGREES_0;
		} else if (value == DEGREES_90.shValue) {
			return DEGREES_90;
		} else if (value == DEGREES_180.shValue) {
			return DEGREES_180;
		} else if (value == DEGREES_270.shValue) {
			return DEGREES_270;
		} else {
			throw new IllegalArgumentException("Invalid orientation value provided.");
		}
	}
	
	/**
	 * Returns the Orientation given the provided int value (0, 90, 180, 270).
	 * 
	 * @param value int value
	 * @return Orientation for int value
	 */
	public static Orientation findByInt(final int value) {
		if (value == 0) {
			return DEGREES_0;
		} else if (value == 90) {
			return Orientation.DEGREES_90;
		} else if (value == 180) {
			return Orientation.DEGREES_180;
		} else if (value == 270) {
			return Orientation.DEGREES_270;
		} else {
			throw new IllegalArgumentException("Invalid orientation value provided.");
		}
	}
}
