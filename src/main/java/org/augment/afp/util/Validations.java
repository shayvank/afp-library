package org.augment.afp.util;

/**
 * This static class provides common validation routines.  If not specified, the IllegalArgumentException will be throw for failing validation.
 *
 */
public class Validations {

	private Validations() {
		
	}

	/**
	 * Validates the incoming object is not null.  If null, the errorMsg is thrown.
	 * @param obj to check
	 * @param errorMsg error to provide
	 */
	public static void notNull(final Object obj, final String errorMsg) {
		if (obj == null) {
			throw new IllegalArgumentException(errorMsg);
		}
	}
	
	/**
	 * Validates the incoming object is not empty (length is not zero).  If length is zero, the errorMsg is thrown. 
	 * @param obj to check
	 * @param errorMsg error to provide
	 */
	public static void notEmpty(final String obj, final String errorMsg) {
		notNull(obj, errorMsg);
		if (obj.length() == 0) {
			throw new IllegalArgumentException(errorMsg);
		}
	}
	
	/**
	 * Validates the two incoming objects are equal.  If not, the errorMsg is thrown.
	 * @param obj to check
	 * @param obj2 to check against
	 * @param errorMsg error to provide
	 */
	public static void equals(final Object obj, final Object obj2, final String errorMsg) {
		if (obj != obj2) {
			throw new IllegalArgumentException(errorMsg);
		}
	}
}
