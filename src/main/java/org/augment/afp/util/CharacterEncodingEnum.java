package org.augment.afp.util;

import java.io.UnsupportedEncodingException;

/**
 * A set of predefined character encodings.
 *
 */
public enum CharacterEncodingEnum implements CharacterEncoding {
	
	/**
	 * EBCDIC (Code Page 500).
	 */
	EBCDIC("Cp500", 24832, "T1V10500", 500);  // 24832 = 0x6100

	private String charset;
	private int scheme;
	private String codePageName;
	private int codePage;
	
	private CharacterEncodingEnum(final String charset, final int scheme, final String codePageName, final int codePage) {
		this.charset = charset;
		this.scheme = scheme;
		this.codePageName = codePageName;
		this.codePage = codePage;
	}
	
	@Override
	public String getCharset() {
		return charset;
	}

	@Override
	public int getScheme() {
		return scheme;
	}

	@Override
	public String getCodePageName() {
		return codePageName;
	}

	@Override
	public int getCodePage() {
		return codePage;
	}
	
	@Override
	public String stringValue(final byte[] value) {
		try {
			return new String(value, charset);
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException("Failed to convert charset", e);
		}
	}
	
	@Override
	public byte[] byteValue(final String value) {
		try {
			return value.getBytes(charset);
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException("Failed to convert charset", e);
		}
	}

}
