package org.augment.afp.util;

/**
 * MO:DCA uses flags or "bit sets" to identify supplemental data.  This utility class is used to interact with those bit sets.
 *
 */
public class BigEndianBitSet {
	
	private byte b; 
	
	/**
	 * Create a new instance with a start byte 0x00.
	 */
	public BigEndianBitSet() { 
		b = (byte)0x00; 
	} 
	
	/**
	 * Made private so the static method must be used.
	 * 
	 * @param b beginning byte value
	 */
	private BigEndianBitSet(byte b) { 
		this.b = b;
	}
	
	/**
	 * Create a new instance with a beginning byte value.
	 * 
	 * @param b beginning byte value
	 * @return new instance
	 */
	public static BigEndianBitSet valueOf(byte b) { 
		return new BigEndianBitSet(b); 
	} 
	
	/**
	 * Provides a mechanism to get the correct position of the bit set.
	 * 
	 * @param litEndianIdx little endian index value
	 * @return big endian index value
	 */
	private int indexCalc(final int litEndianIdx) {	
		int[] bigIdx = new int[] { 7, 6, 5, 4, 3, 2, 1, 0 };
		
		return bigIdx[litEndianIdx];
		
	}
	
	/**
	 * Clears the specific bit to zero.
	 * 
	 * @param bitindex specific bit position
	 */
	public void clear(int bitindex) {
		b &= ~(1 << indexCalc(bitindex));
	}  

	/**
	 * Returns the specific bit.
	 * 
	 * @param bitindex specific bit position
	 * @return true or false flag
	 */
	public boolean get(int bitindex) { 
		return ((b >> indexCalc(bitindex)) & 1) == 1;
	} 
	
	/**
	 * Toggles the specific bit.
	 * 
	 * @param bitindex specific bit position
	 */
	public void set(int bitindex) { 
		b |= 1 << indexCalc(bitindex);
	} 
	
	/**
	 * Sets the specific bit value.
	 * 
	 * @param bitindex specific bit position
	 * @param value flag value
	 */
	public void set(int bitindex, boolean value) {
		if (value) {
			set(bitindex);
		} else {
			clear(bitindex);
		} 
	} 
	
	/**
	 * Returns the byte value of this instance.
	 * 
	 * @return byte value
	 */
	public byte toByte() { 
		return b;
	} 

}
