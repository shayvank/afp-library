package org.augment.afp.util;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class StructuredFieldFileParser implements Closeable {
	
	private File file;
	
	private BufferedInputStream bis;
	
	public StructuredFieldFileParser(final File file) throws FileNotFoundException {
		this.file = file;
		this.bis = new BufferedInputStream(new FileInputStream(file));
	}
	
	public File getFile() {
		return file;
	}
	
	public StructuredField readNextSfBytes() throws IOException {
		StructuredField sf = null;
		
		try {
			bis.mark(2);
			
			DataInputStream dis = new DataInputStream(bis);
			
			int a5aByte = dis.readUnsignedByte();
			if (a5aByte != 0x5A) {
				// no 5a, so reseek to beginning
				bis.reset();
			}
			
			sf = StructuredField.getNext(dis);
		} catch (EOFException eof) {
			sf = null;
		}
		return sf;
	}
	
	@Override
	public void close() throws IOException {
		bis.close();
	}

}
