package org.augment.afp.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class StructuredFieldBuilder {

	private CategoryCode categoryCode;
	private TypeCode typeCode;
	ByteArrayOutputStream data;
	
	private StructuredFieldBuilder(final CategoryCode categoryCode, final TypeCode typeCode) {
		this.categoryCode = categoryCode;
		this.typeCode = typeCode;
		this.data = new ByteArrayOutputStream();
	}
	
	public static StructuredFieldBuilder createBuilder(final TypeCode typeCode, final CategoryCode categoryCode) {
		return new StructuredFieldBuilder(categoryCode, typeCode);
	}
	
	public StructuredFieldBuilder addData(final int data) {
		this.data.write(data);
		return this;
	}
	
	public StructuredFieldBuilder addData(final byte data) {
		this.data.write(data);
		return this;
	}
	
	public StructuredFieldBuilder addData(final byte[] data) throws IOException {
		this.data.write(data);
		return this;
	}
	
	public StructuredFieldBuilder addTriplet(final int tripLen, final int tripId, final byte[] tripContents) throws IOException {
		data.write(tripLen);
		data.write(tripId);
		data.write(tripContents);
		return this;
	}
	
	public StructuredFieldBuilder addTriplet(final int tripId, final byte[] tripContents) throws IOException {
		int triplen = 2 + tripContents.length;
		data.write(ByteUtils.toUnsignedByte((byte) triplen));
		data.write(tripId);
		if (tripContents.length > 0) {
			data.write(tripContents);
		}
		return this;
	}
	
	public TripletBuilder createTripletBuilder(final int tripId) {
		return new TripletBuilder(tripId, this);
	}
	
	public StructuredField build() throws IOException {
		byte[] sfData = data.toByteArray();
		int sfLen = 8 + sfData.length;
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();
		sfBytes.write(ByteUtils.fromShort((short) sfLen));
		sfBytes.write(0xd3);
		sfBytes.write(typeCode.toByte());
		sfBytes.write(categoryCode.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		if (sfData.length > 0) {
			sfBytes.write(sfData);
		}
		
		return new StructuredField(sfBytes.toByteArray());
	}

	public class TripletBuilder {
		private int tripId;
		private StructuredFieldBuilder parentBuilder;
		ByteArrayOutputStream tripData;
		
		private TripletBuilder(final int tripId, final StructuredFieldBuilder parentBuilder) {
			this.tripId = tripId;
			this.parentBuilder = parentBuilder;
			this.tripData = new ByteArrayOutputStream();
		}
		
		public TripletBuilder addTripletData(final int data) {
			tripData.write(data);
			return this;
		}
		
		public TripletBuilder addTripletData(final byte data) {
			tripData.write(data);
			return this;
		}
		
		public TripletBuilder addTripletData(final byte[] data) throws IOException {
			tripData.write(data);
			return this;
		}
		
		public StructuredFieldBuilder addTriplet() throws IOException {
			return parentBuilder.addTriplet(tripId, tripData.toByteArray());
		}
	}
}
