package org.augment.afp.util.merge;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.stream.IntStream;

import org.augment.afp.util.AugmentAfpException;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.ResourceObjectType;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldBuilder;
import org.augment.afp.util.Triplet;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.merge.AfpMerger.AfpInput;
import org.augment.afp.util.merge.AfpMerger.MediumMapObj;
import org.augment.afp.util.merge.AfpMerger.ResourceObj;

public class DocumentRenamer {
	
	private DocumentRenamer() {
		// private constructor, static class
	}
		
	public static StructuredField renameResourceEnvelopeSf(final StructuredField inputSf, final String newName) throws IOException {
		StructuredField sf = inputSf;
		sf = renameState(CategoryCode.NAMED_RESOURCE, TypeCode.BEGIN, sf, newName, 0, 10); // this has some additional rules if truetypecollection (TTC)
		sf = renameState(CategoryCode.NAMED_RESOURCE, TypeCode.END, sf, newName, 0, 10);
		sf = renameState(CategoryCode.OVERLAY, TypeCode.BEGIN, sf, newName, 0, 8);
		sf = renameState(CategoryCode.OVERLAY, TypeCode.END, sf, newName, 0, 8);
		sf = renameState(CategoryCode.MEDIUM_MAP, TypeCode.BEGIN, sf, newName, 0, 8);
		sf = renameState(CategoryCode.MEDIUM_MAP, TypeCode.END, sf, newName, 0, 8);
		sf = renameState(CategoryCode.OBJECT_CONTAINER, TypeCode.BEGIN, sf, newName, 0, 8); // this is mocked up.  need an example of a BOC/EOC in order to provide a better test
		sf = renameState(CategoryCode.OBJECT_CONTAINER, TypeCode.END, sf, newName, 0, 8);
		sf = renameState(CategoryCode.PAGE_SEGMENT, TypeCode.BEGIN, sf, newName, 0, 8);
		sf = renameState(CategoryCode.PAGE_SEGMENT, TypeCode.END, sf, newName, 0, 8);
		sf = renameState(CategoryCode.GRAPHICS, TypeCode.BEGIN, sf, newName, 0, 8);
		sf = renameState(CategoryCode.GRAPHICS, TypeCode.END, sf, newName, 0, 8);		
		return sf;
	}
	
	private static StructuredField renameState(final CategoryCode categoryCode, final TypeCode typeCode, final StructuredField sf, final String newName, final int nameStart, final int tripletStart) throws IOException {
		StructuredField returnSf = sf;
		if (sf.getCategoryCode() == categoryCode && sf.getTypeCode() == typeCode) {
			returnSf = replaceName(sf, nameStart, newName);
			returnSf = replaceFQN01(returnSf, tripletStart, newName);
		}
		return returnSf;
	}
	
	public static byte[] renameResourceBuffer(final byte[] buffer, final AfpInput input) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(buffer));
		StructuredField sf = null;
		
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				sf = renameIMM(sf, input);
				sf = renameIOB(sf, input);
				sf = renameIPG(sf);
				sf = renameIPO(sf, input);
				sf = renameIPS(sf, input);
				sf = renameMCF(sf, input);
				sf = renameMCF1(sf);
				sf = renameMDR(sf, input);
				sf = renameMMO(sf, input);
				sf = renameMPG(sf);
				sf = renameMPO(sf, input);
				sf = renameMPS(sf, input);
				
				baos.write(sf.bytes());
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		return baos.toByteArray();
	}
	
	private static StructuredField renameIMM(final StructuredField sf, final AfpInput input) throws IOException {
		StructuredField field = sf;
		if (sf.getTypeCode() == TypeCode.MAP && sf.getCategoryCode() == CategoryCode.MEDIUM_MAP) {
			// we have a medium map that needs to be renamed
			String name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
			MediumMapObj mmo = input.mediumMaps.get(name);
			if (mmo.newName != null) {
				field = replaceName(field, 0, mmo.newName);
				field = replaceFQN01(field, 8, mmo.newName);
			}
		}
		return field;
	}
	
	private static StructuredField renameIOB(final StructuredField sf, final AfpInput input) throws IOException { // need an actual example of IOB in order to properly test.  emulated with some dummy data
		StructuredField field = sf;
		if (sf.getTypeCode() == TypeCode.INCLUDE && sf.getCategoryCode() == CategoryCode.DATA_RESOURCE) {
			// we have an object that needs renamed
			String name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
			ResourceObj res = input.fileResources.get(name);
			if (res != null && res.newName != null) {
				field = replaceName(field, 0, res.newName);
				field = replaceFQN01(field, 27, res.newName);
			}
		}
		return field;
	}
	
	private static StructuredField renameIPG(final StructuredField sf) {
		// let's not figured this one out right now.  if it becomes an issue, we'll deal with it then.
		if (sf.getTypeCode() == TypeCode.INCLUDE && sf.getCategoryCode() == CategoryCode.PAGE) {
			throw new AugmentAfpException("IPG StructuredFields are not supported.");	
		}
		return sf;
	}
	
	private static StructuredField renameIPO(final StructuredField sf, final AfpInput input) throws IOException {
		StructuredField field = sf;
		if (sf.getTypeCode() == TypeCode.INCLUDE && sf.getCategoryCode() == CategoryCode.PAGE_OVERLAY) {
			// we have an overlay that needs renamed
			String name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
			ResourceObj res = input.fileResources.get(name);
			if (res != null && res.newName != null && ResourceObjectType.findByValue(res.type) == ResourceObjectType.OVERLAY) {
				field = replaceName(field, 0, res.newName);
				field = replaceFQN01(field, 16, res.newName);
			}
		}
		return field;
	}
	
	private static StructuredField renameIPS(final StructuredField sf, final AfpInput input) throws IOException {
		StructuredField field = sf;
		if (sf.getTypeCode() == TypeCode.INCLUDE && sf.getCategoryCode() == CategoryCode.PAGE_SEGMENT) {
			// we have a segment that needs renamed
			String name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
			ResourceObj res = input.fileResources.get(name);
			if (res != null && res.newName != null && ResourceObjectType.findByValue(res.type) == ResourceObjectType.PAGESEGMENT) {
				field = replaceName(field, 0, res.newName);
				field = replaceFQN01(field, 14, res.newName);
			}
		}
		return field;
	}
	
	private static StructuredField renameMCF(final StructuredField sf, final AfpInput input) throws IOException {
		StructuredField field = sf;
		
		if (sf.getTypeCode() == TypeCode.MAP && sf.getCategoryCode() == CategoryCode.CODED_FONT) {
			byte[] data = sf.getData();
			
			int [] fqnTypes = { 0x85, 0x86, 0x8E };
			// we have a mapped coded font that needs to be renamed
			StructuredFieldBuilder sfBuilder = StructuredFieldBuilder.createBuilder(sf.getTypeCode(), sf.getCategoryCode());			
			for (int startPos = 0; startPos < data.length; ) {
				int rgLength = ByteUtils.toUnsignedShort(ByteUtils.arraycopy(data, startPos, 2));

				sfBuilder.addData(ByteUtils.arraycopy(data, startPos, 2));
				for (Triplet trip: Triplet.parse(data, (startPos + 2), (rgLength - 2))) {	
					if (trip.getCode() == 0x02 && IntStream.of(fqnTypes).anyMatch(x -> x == ByteUtils.toUnsignedByte(trip.getContents()[0]))) {
						String name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(trip.getContents(), 2, trip.getContents().length - 2));
						ResourceObj res = input.fileResources.get(name);
						if (res != null && res.newName != null) {
							sfBuilder.createTripletBuilder(trip.getCode())
							.addTripletData(ByteUtils.toUnsignedByte(trip.getContents()[0]))
							.addTripletData(ByteUtils.toUnsignedByte(trip.getContents()[1]))
							.addTripletData(ByteUtils.getDataEncoding().byteValue(res.newName))
							.addTriplet();
						} else {
							sfBuilder.createTripletBuilder(trip.getCode())
							.addTripletData(trip.getContents())
							.addTriplet();
						}						
					} else {
						sfBuilder.createTripletBuilder(trip.getCode())
						.addTripletData(trip.getContents())
						.addTriplet();
					}
				}
				
				startPos += rgLength;
			}
						
			field = sfBuilder.build();
		}
		
		
		return field;
	}
	
	private static StructuredField renameMCF1(final StructuredField sf) {
		if (sf.getTypeCode() == TypeCode.MIGRATION && sf.getCategoryCode() == CategoryCode.CODED_FONT) {
			throw new AugmentAfpException("MCF1 StructuredFields are not supported.");	
		}
		return sf;		
	}
	
	private static StructuredField renameMDR(final StructuredField sf, final AfpInput input) throws IOException {
		StructuredField field = sf;
		if (sf.getTypeCode() == TypeCode.MAP && sf.getCategoryCode() == CategoryCode.DATA_RESOURCE) {
			byte[] data = sf.getData();
			
			int [] fqnTypes = { 0x84, 0xCE, 0xDE };
			// we have a mapped coded font that needs to be renamed
			StructuredFieldBuilder sfBuilder = StructuredFieldBuilder.createBuilder(sf.getTypeCode(), sf.getCategoryCode());			
			for (int startPos = 0; startPos < data.length; ) {
				int rgLength = ByteUtils.toUnsignedShort(ByteUtils.arraycopy(data, startPos, 2));

				sfBuilder.addData(ByteUtils.arraycopy(data, startPos, 2));
				for (Triplet trip: Triplet.parse(data, (startPos + 2), (rgLength - 2))) {	
					if (trip.getCode() == 0x02 && IntStream.of(fqnTypes).anyMatch(x -> x == ByteUtils.toUnsignedByte(trip.getContents()[0]))) {
						String name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(trip.getContents(), 2, trip.getContents().length - 2));
						ResourceObj res = input.fileResources.get(name);
						if (res != null && res.newName != null) {
							sfBuilder.createTripletBuilder(trip.getCode())
							.addTripletData(ByteUtils.toUnsignedByte(trip.getContents()[0]))
							.addTripletData(ByteUtils.toUnsignedByte(trip.getContents()[1]))
							.addTripletData(ByteUtils.getDataEncoding().byteValue(res.newName))
							.addTriplet();
						} else {
							sfBuilder.createTripletBuilder(trip.getCode())
							.addTripletData(trip.getContents())
							.addTriplet();
						}						
					} else {
						sfBuilder.createTripletBuilder(trip.getCode())
						.addTripletData(trip.getContents())
						.addTriplet();
					}
				}
				
				startPos += rgLength;
			}
						
			field = sfBuilder.build();
		}
		
		return field;
	}
	
	private static StructuredField renameMMO(final StructuredField sf, final AfpInput input) throws IOException {
		StructuredField field = sf;
		if (sf.getTypeCode() == TypeCode.MIGRATION && sf.getCategoryCode() == CategoryCode.OVERLAY) {			
			// we have an overlay that needs renamed
			
			StructuredFieldBuilder sfBuilder = StructuredFieldBuilder.createBuilder(sf.getTypeCode(), sf.getCategoryCode());
			byte[] data = sf.getData();
			sfBuilder.addData(ByteUtils.arraycopy(data, 0, 4));
			
			for (int i = 4; i < data.length; i += 12) {
				byte[] group = ByteUtils.arraycopy(data, i, 12);
				String name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(group, 4, 8));
				ResourceObj res = input.fileResources.get(name);
				if (res != null && res.newName != null && ResourceObjectType.findByValue(res.type) == ResourceObjectType.OVERLAY) {
					sfBuilder.addData(ByteUtils.arraycopy(group, 0, 4));
					sfBuilder.addData(ByteUtils.getDataEncoding().byteValue(res.newName));
				} else {
					sfBuilder.addData(group);
				}
				
			}
			
			field = sfBuilder.build();
		}
		return field;
	}
	
	private static StructuredField renameMPG(final StructuredField sf) {
		// let's not figured this one out right now.  if it becomes an issue, we'll deal with it then.
		// let's not figured this one out right now.  if it becomes an issue, we'll deal with it then.
		if (sf.getTypeCode() == TypeCode.MAP && sf.getCategoryCode() == CategoryCode.PAGE) {
			throw new AugmentAfpException("MPG StructuredFields are not supported.");	
		}
		return sf;		
	}
	
	private static StructuredField renameMPO(final StructuredField sf, final AfpInput input) throws IOException {
		StructuredField field = sf;
		byte[] data = sf.getData();
		if (sf.getTypeCode() == TypeCode.MAP && sf.getCategoryCode() == CategoryCode.PAGE_OVERLAY) {
			StructuredFieldBuilder sfBuilder = StructuredFieldBuilder.createBuilder(sf.getTypeCode(), sf.getCategoryCode());
			sfBuilder.addData(ByteUtils.arraycopy(data, 0, 2));
			for (Triplet trip: Triplet.parse(data, 2, (data.length - 2))) {				
				if (trip.getCode() == 0x02 && ByteUtils.toUnsignedByte(trip.getContents()[0]) == 0x84) {
					String name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(trip.getContents(), 2, trip.getContents().length - 2));
					ResourceObj res = input.fileResources.get(name);
					if (res != null && res.newName != null && ResourceObjectType.findByValue(res.type) == ResourceObjectType.OVERLAY) {
						sfBuilder.createTripletBuilder(trip.getCode())
						.addTripletData(ByteUtils.toUnsignedByte(trip.getContents()[0]))
						.addTripletData(ByteUtils.toUnsignedByte(trip.getContents()[1]))
						.addTripletData(ByteUtils.getDataEncoding().byteValue(res.newName))
						.addTriplet();
					} else {
						sfBuilder.createTripletBuilder(trip.getCode())
						.addTripletData(trip.getContents())
						.addTriplet();						
					}
				} else {
					sfBuilder.createTripletBuilder(trip.getCode())
						.addTripletData(trip.getContents())
						.addTriplet();				
				}
			}
									
			field = sfBuilder.build();
		}
		return field;
	}
	
	private static StructuredField renameMPS(final StructuredField sf, final AfpInput input) throws IOException {
		StructuredField field = sf;
		if (sf.getTypeCode() == TypeCode.MIGRATION && sf.getCategoryCode() == CategoryCode.PAGE_SEGMENT) {
			// we have a segment that needs renamed
			
			byte[] data = sf.getData();
			int skip = 12;
			int startPos = 4;
			
			StructuredFieldBuilder sfBuilder = StructuredFieldBuilder.createBuilder(sf.getTypeCode(), sf.getCategoryCode());
			sfBuilder.addData(new byte[] { 0x0C, 0x00, 0x00, 0x00});
			
			for (int i = startPos; i < data.length; i += skip) {
				sfBuilder.addData(new byte[] {0x00, 0x00, 0x00, 0x00});	
				String name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(data, startPos+4, 8));
				ResourceObj res = input.fileResources.get(name);				
				if (res != null && res.newName != null && ResourceObjectType.findByValue(res.type) == ResourceObjectType.PAGESEGMENT) {
					name = res.newName;
				}
				sfBuilder.addData(ByteUtils.getDataEncoding().byteValue(name));
				
				startPos += skip;
			}
			
			field = sfBuilder.build();
		}
		return field;
	}
	
	private static StructuredField replaceName(final StructuredField sf, final int nameStart, final String newName) throws IOException {
		if (newName == null) {
			return sf;
		}
		
		byte[] data = sf.getData();
		if (data.length < (nameStart + 8)) {
			return sf;
		}
		
		StructuredFieldBuilder sfBuilder = StructuredFieldBuilder.createBuilder(sf.getTypeCode(), sf.getCategoryCode());
		int start = 0;
		if (nameStart > 0) {
			sfBuilder.addData(ByteUtils.arraycopy(data, start, nameStart));
		}
		String newNewName = newName + "        ";
		sfBuilder.addData(ByteUtils.getDataEncoding().byteValue(newNewName.substring(0, 8)));
		start = nameStart + 8;
		if (start < data.length) {
			sfBuilder.addData(ByteUtils.arraycopy(data, start, data.length - start));
		}
		return sfBuilder.build();
	}
	
	private static StructuredField replaceFQN01(final StructuredField sf, final int tripletStart, final String newName) throws IOException {
		if (newName == null) {
			return sf;
		}
		byte[] data = sf.getData();
		if (data.length < tripletStart + 2) { // this will check for at least 1 length and 1 code
			return sf;
		}
		
		StructuredFieldBuilder sfBuilder = StructuredFieldBuilder.createBuilder(sf.getTypeCode(), sf.getCategoryCode());
		sfBuilder.addData(ByteUtils.arraycopy(sf.getData(), 0, tripletStart));
		
		for (Triplet trip: Triplet.parse(data, tripletStart, (data.length - tripletStart))) {
			// FQN
			if (trip.getCode() == 0x02 && ByteUtils.toUnsignedByte(trip.getContents()[0]) == 0x01) {
				// this is the FQN Replace First GID name.  We will replace with our new name
				sfBuilder.createTripletBuilder(trip.getCode())
					.addTripletData(ByteUtils.toUnsignedByte(trip.getContents()[0]))
					.addTripletData(ByteUtils.toUnsignedByte(trip.getContents()[1]))
					.addTripletData(ByteUtils.getDataEncoding().byteValue(newName))
					.addTriplet();
			} else {
				sfBuilder.createTripletBuilder(trip.getCode())
					.addTripletData(trip.getContents())
					.addTriplet();				
			}
		}
		
		return sfBuilder.build();	
	}
}
