package org.augment.afp.util.merge;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.augment.afp.AfpFileSerializer;
import org.augment.afp.data.NamedResource;
import org.augment.afp.data.PageGroup;
import org.augment.afp.data.PrintFileResourceGroup;
import org.augment.afp.data.StateEnvelope;
import org.augment.afp.data.formmap.FormMapResource;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldOutputStream;
import org.augment.afp.util.Triplet;
import org.augment.afp.util.TypeCode;

public class AfpMerger implements Closeable {
	
	static class ResourceObj {
		String name;
		String newName;
		int type;
		StateEnvelope envelope;
		byte[] buffer;
		int hash;
		
		void setFromNamedResource(final NamedResource res) throws IOException {
			name = res.getName();
			StructuredField brs = null;
			StructuredField ers = null;
			ByteArrayOutputStream bufferBytes = new ByteArrayOutputStream();
			byte[] resourceData = res.getData();
			if (resourceData != null) {
				DataInputStream dis = new DataInputStream(new ByteArrayInputStream(resourceData));
				StructuredField sf = null;
				try {
					while ((sf = StructuredField.getNext(dis)) != null) {
						if (sf.getCategoryCode() == CategoryCode.NAMED_RESOURCE && sf.getTypeCode() == TypeCode.BEGIN) {
								brs = sf;
						} else if (sf.getCategoryCode() == CategoryCode.NAMED_RESOURCE && sf.getTypeCode() == TypeCode.END) {
								ers = sf;
						} else {
							bufferBytes.write(sf.bytes());
						}
					}
				} catch (EOFException eof) {
					// end of stream
				}
				// find the resource type tripleet
				for (Triplet triplet: Triplet.parse(brs.getData(), 10, brs.getData().length - 10)) {
					if (triplet.getCode() == 0x21) {
						type = ByteUtils.toUnsignedByte(triplet.getContents()[0]);
					}
				}
				envelope = new StateEnvelope(brs, ers);
				buffer = bufferBytes.toByteArray();
				hash = Arrays.hashCode(buffer);
			}
		}
	}
	
	static class MediumMapObj {
		String name;
		String newName;
		StateEnvelope envelope;
		byte[] buffer;
		int hash;
		
		void setFromMediumMapBytes(final byte[] mmBytes) throws IOException {
			StructuredField bmm = null;
			StructuredField emm = null;
			ByteArrayOutputStream bufferBytes = new ByteArrayOutputStream();
			if (mmBytes != null) {
				DataInputStream dis = new DataInputStream(new ByteArrayInputStream(mmBytes));
				StructuredField sf = null;
				try {
					while ((sf = StructuredField.getNext(dis)) != null) {
						if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.BEGIN) {
							name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
							bmm = sf;
						} else if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.END) {
							emm = sf;
							break;
						} else {
							bufferBytes.write(sf.bytes());
						}
					}
				} catch (EOFException eof) {
					// end of stream
				}
				envelope = new StateEnvelope(bmm, emm);
				buffer = bufferBytes.toByteArray();
				hash = Arrays.hashCode(buffer);
			}
		}
	}
	
	static class AfpInput {
		AfpFileSerializer serializer;
		Map<String, ResourceObj> fileResources = new LinkedHashMap<>();
		Map<String, MediumMapObj> mediumMaps = new LinkedHashMap<>();
		FormMapResource formMapResource;
	}
	
	private List<AfpInput> afpInputs;
	private AfpFileSerializer primarySerializer;
	private StructuredFieldOutputStream out;
	private List<String> uniqueResourceNames = new LinkedList<>();
	private List<String> uniqueMediumMapNames = new LinkedList<>();
	
	private AfpMerger() {
		afpInputs = new LinkedList<>();
	}
	
	public AfpMerger(final List<AfpFileSerializer> inputFiles, final OutputStream output) throws IOException {
		this();
		int counter = 0;
		for (AfpFileSerializer serializer: inputFiles) {
			if (counter == 0) {
				primarySerializer = serializer;
				counter++;
			}
			AfpInput input = new AfpInput();
			input.serializer = serializer;
			this.afpInputs.add(input);
		}
		if (output instanceof StructuredFieldOutputStream) {
			out = (StructuredFieldOutputStream) output;
		} else {
			out = new StructuredFieldOutputStream(output);
		}
		
		gatherPrintFileResourceGroups();
		
		compareResources();
		renameResourceBuffer();
		compareResources();  // run again after renaming object items in the data of the resource, just in case
		
		compareFormdefs();
		renameMediumMapBuffer();
		compareFormdefs();
		
		primarySerializer.writeBeginPrintFileEnvelopeTo(out);
		writeResourceGroup();
		

		primarySerializer.writeBeginDocumentEnvelopeTo(out);
		
	}
	
	@Override
	public void close() throws IOException {
		while (primarySerializer.next() != null) {
			// do nothing, ensure finished)
		}
		primarySerializer.writeEndDocumentEnvelopeTo(out);
		primarySerializer.writeEndPrintFileEnvelopeTo(out);
		
		for (AfpInput afpInput: afpInputs) {
			afpInput.serializer.close();
		}
		
		out.close();		
	}
	
	public static AfpMerger createFromFiles(final List<File> afpFiles, final OutputStream output) throws IOException {
		List<AfpFileSerializer> fserializers = new LinkedList<>();
		if (afpFiles != null) {
			for (File file: afpFiles) {
				fserializers.add(new AfpFileSerializer(file));
			}
		}
		return new AfpMerger(fserializers, output);
	}
	
	private void gatherPrintFileResourceGroups() throws IOException {
		for (AfpInput afpInput: afpInputs) {
			PrintFileResourceGroup resourceGroup = afpInput.serializer.getPrintFileResourceGroup();
			for (NamedResource resource: resourceGroup.getResources()) {
				if (resource instanceof FormMapResource) {
					afpInput.formMapResource = (FormMapResource) resource;					
					for (String name: afpInput.formMapResource.getFormMap().getMediumMaps().keySet()) {
						byte[] mMap = afpInput.formMapResource.getMediumMapData(name);
						MediumMapObj mmObj = new MediumMapObj();
						mmObj.setFromMediumMapBytes(mMap);
						afpInput.mediumMaps.put(mmObj.name, mmObj);
						if (!uniqueMediumMapNames.contains(mmObj.name)) {
							uniqueMediumMapNames.add(mmObj.name);
						}
					}
				} else {
					ResourceObj resObj = new ResourceObj();
					resObj.setFromNamedResource(resource);
					afpInput.fileResources.put(resObj.name, resObj);
					if (!uniqueResourceNames.contains(resObj.name)) {
						uniqueResourceNames.add(resObj.name);
					}
				}
			}
		}
	}
	
	private void compareResources() throws IOException {
		Map<String, Map<Integer, ResourceObj>> resourceLibrary = new HashMap<>();
		for (AfpInput afpInput: afpInputs) {
			for (ResourceObj res: afpInput.fileResources.values()) {
				if (resourceLibrary.containsKey(res.name)) {
					Map<Integer, ResourceObj> reses = resourceLibrary.get(res.name);
					ResourceObj versioned = reses.get(res.hash);
					if (versioned == null) {
						// the hash didn't exist for the name and newName is null, so this is a new one that needs to be renamed
						if (res.newName == null) {
							// generate new name if not already generated for this resource
							String newName = createNewResourceName(res);
							res.newName = newName;
						}
						reses.put(res.hash, res);
					} else if (versioned.newName != null) {
						// this is the same hash, so its "assumed" to be the same object.  so, copy over whatever is in the newName property
						res.newName = versioned.newName;
					}
				} else {
					Map<Integer, ResourceObj> version = new HashMap<>();
					version.put(res.hash, res);
					resourceLibrary.put(res.name, version);
				}
			}
		}
	}
	
	private void compareFormdefs() throws IOException {
		Map<String, Map<Integer, MediumMapObj>> mediummapLibrary = new HashMap<>();
		for (AfpInput afpInput: afpInputs) {
			for (MediumMapObj mm: afpInput.mediumMaps.values()) {
				if (mediummapLibrary.containsKey(mm.name)) {
					Map<Integer, MediumMapObj> mms = mediummapLibrary.get(mm.name);
					MediumMapObj versioned = mms.get(mm.hash);
					if (versioned == null) {
						// the hash didn't exist for the name and newName is null, so this is a new one that needs to be renamed
						if (mm.newName == null) {
							// generate new name if not aready generated for this resource
							String newName = createNewMediumMapName(mm);
							mm.newName = newName;
						}
						mms.put(mm.hash, mm);
					} else if (versioned.newName != null) {
						// this is the same hash, so its "assumed" to be the same object.  so, copy over whatever is in the newName property
						mm.newName = versioned.newName;
					}
				} else {
					Map<Integer, MediumMapObj> version = new HashMap<>();
					version.put(mm.hash, mm);
					mediummapLibrary.put(mm.name, version);
				}
			}
		}
	}
	
	private String createNewResourceName(final ResourceObj resObj) {
		String newPrefix = resObj.name.substring(0,2);
		
		boolean success = false;
		String newName = null;
		while (!success) {
			try {
				Thread.sleep(102); // sleep a little past a deci-second
			} catch (InterruptedException e) {
				// do nothing
			}
			LocalTime now = LocalTime.now();
			int secs = now.toSecondOfDay();
			
			newName = newPrefix + "0" + String.format("%05d", secs);
			
			if (!uniqueResourceNames.contains(newName)) {
				uniqueResourceNames.add(newName);
				success = true;
			}
		}
		
		return newName;
	}
	
	private String createNewMediumMapName(final MediumMapObj mmObj) {
		String prefix = "MM";
		
		boolean success = false;
		String newName = null;
		while (!success) {
			try {
				Thread.sleep(102); // sleep a little past a deci-second
			} catch (InterruptedException e) {
				// do nothing
			}
			LocalTime now = LocalTime.now();
			long nanos = now.toNanoOfDay();
			int decis = (int)(nanos / 100000000);
			
			newName = prefix + Integer.toString(decis);
			
			if (!uniqueMediumMapNames.contains(newName)) {
				uniqueMediumMapNames.add(newName);
				success = true;
			}
		}
		
		return newName;
	}
	
	private void renameResourceBuffer() throws IOException {
		for (AfpInput afpInput: afpInputs) {
			for (ResourceObj res: afpInput.fileResources.values()) {								
				
				res.buffer = DocumentRenamer.renameResourceBuffer(res.buffer, afpInput);
												
				if (res.newName != null) {
					StateEnvelope env = new StateEnvelope(DocumentRenamer.renameResourceEnvelopeSf(res.envelope.getBegin(), res.newName));
					env.setEnd(DocumentRenamer.renameResourceEnvelopeSf(res.envelope.getEnd(), res.newName));
					res.envelope = env;
				}
			}
		}
	}
	
	private void renameMediumMapBuffer() throws IOException {
		for (AfpInput afpInput: afpInputs) {
			for (MediumMapObj mm: afpInput.mediumMaps.values()) {
				
				mm.buffer = DocumentRenamer.renameResourceBuffer(mm.buffer, afpInput);
				
				if (mm.newName != null) {
					StateEnvelope env = new StateEnvelope(DocumentRenamer.renameResourceEnvelopeSf(mm.envelope.getBegin(), mm.newName));
					env.setEnd(DocumentRenamer.renameResourceEnvelopeSf(mm.envelope.getEnd(), mm.newName));
					mm.envelope = env;
				}
			}
		}
	}
	
	private void writeFormDef() throws IOException {
		int counter = 0;
		List<String> writtenMediumMap = new ArrayList<>();
		StateEnvelope resState = null;
		StateEnvelope fmState = null;
		for (AfpInput afpInput: afpInputs) {
			if (counter == 0) {
				resState = afpInput.formMapResource.getResourceStateEnvelope();
				fmState = afpInput.formMapResource.getFormMapState();
				out.writeStructuredField(resState.getBegin());
				out.writeStructuredField(fmState.getBegin());
				writeSfByteArray(afpInput.formMapResource.getDocumentEnvironmentGroupData());
			}
			
			for (MediumMapObj mm: afpInput.mediumMaps.values()) {
				String mediumMapName = mm.name;
				if (mm.newName != null) {
					mediumMapName = mm.newName;
				}
				
				if (uniqueMediumMapNames.contains(mediumMapName) && !writtenMediumMap.contains(mediumMapName)) {
					out.writeStructuredField(mm.envelope.getBegin());
					writeSfByteArray(mm.buffer);
					out.writeStructuredField(mm.envelope.getEnd());
					writtenMediumMap.add(mediumMapName);
				}
			}
			counter++;
		}
		out.writeStructuredField(fmState.getEnd());
		out.writeStructuredField(resState.getEnd());
	}
	
	private void writeResourceGroup() throws IOException {
		int counter = 0;
		List<String> writtenResource = new ArrayList<>();
		StateEnvelope resState = null;
		for (AfpInput afpInput: afpInputs) {
			if (counter == 0) {
				resState = afpInput.serializer.getPrintFileResourceGroup().getEnvelope();
				out.writeStructuredField(resState.getBegin());
				writeFormDef();
			}
			
			for (ResourceObj res: afpInput.fileResources.values()) {
				String resourceName = res.name;
				if (res.newName != null) {
					resourceName = res.newName;
				}
				
				if (uniqueResourceNames.contains(resourceName) && !writtenResource.contains(resourceName)) {
					out.writeStructuredField(res.envelope.getBegin());
					writeSfByteArray(res.buffer);
					out.writeStructuredField(res.envelope.getEnd());
					writtenResource.add(resourceName);
				}
			}
			counter++;
		}
		out.writeStructuredField(resState.getEnd());
	}
	
	public void addPageGroup(final PageGroup pageGroup) throws IOException {
		if (!pageGroup.isLoaded()) {
			pageGroup.load();
		}
		
		for (AfpInput input: afpInputs) {
			if (input.serializer.getPrintFileResourceGroup() == pageGroup.getPrintFileResourceGroup()) {
				writeSfByteArray(DocumentRenamer.renameResourceBuffer(pageGroup.getRaw(), input));
				break;
			}
		}
	}
	
	public void mergeAll() throws IOException {
		for (AfpInput input: afpInputs) {
			PageGroup group;
			while ((group = input.serializer.next()) != null) {
				addPageGroup(group);
			}
		}
	}
	
	private void writeSfByteArray(final byte[] buffer) throws IOException {
		if (buffer != null) {
			DataInputStream dis = new DataInputStream(new ByteArrayInputStream(buffer));
			StructuredField sf = null;
			try {
				while ((sf = StructuredField.getNext(dis)) != null) {
					out.writeStructuredField(sf);
				}
			} catch (EOFException eof) {
				// end of input
			}
		}
		
	}

}
