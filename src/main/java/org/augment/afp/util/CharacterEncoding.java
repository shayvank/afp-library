package org.augment.afp.util;

/**
 * CharacterEncoding is used to define the needed information for the character encoding in use 
 * for the variable text data. 
 *
 */
public interface CharacterEncoding {

	/**
	 * Name of the charset.
	 * 
	 * @return charset name
	 */
	String getCharset();
	
	/**
	 * Encoding scheme id associated with the code page.
	 * 
	 * @return encoding scheme id.
	 */
	int getScheme();
	
	/**
	 * Code Page Name.
	 * 
	 * @return code page name
	 */
	String getCodePageName();
	
	/**
	 * Numeric id of the code page
	 * @return code page id
	 */
	int getCodePage();
	
	/**
	 * Helper method to convert bytes in the character encoding to a string.
	 * 
	 * @param value character encoded bytes
	 * @return string value
	 */
	String stringValue(byte[] value);
	
	/**
	 * Helper method to convert a string into character encoded bytes.
	 * 
	 * @param value string value
	 * @return character encoded bytes
	 */
	byte[] byteValue(String value);
}
