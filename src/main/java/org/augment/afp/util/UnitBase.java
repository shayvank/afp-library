package org.augment.afp.util;

/**
 * A Unit Base represents the length of the measurement base.  It is specified as a one-byte coded value.
 *
 */
public enum UnitBase {

	/**
	 * 10 inches.
	 */
	TEN_INCHES(0x00),
	/**
	 * 10 centimeters.
	 */
	TEN_CENTIMETERS(0x01);
	
	private int byteValue;
	
	UnitBase(final int byteValue) {
		this.byteValue = byteValue;
	}
	
	/**
	 * Returns the one-byte coded value for this unit base.
	 * 
	 * @return coded value
	 */
	public int getByteValue() {
		return byteValue;
	}
	
	/**
	 * Given the one-byte coded value, returns the enum that represents the unit base.
	 * 
	 * @param b coded value
	 * @return unit base
	 */
	public static UnitBase valueOf(final int b) {
		if (b == TEN_INCHES.byteValue) {
			return TEN_INCHES;
		} else if (b == TEN_CENTIMETERS.byteValue) {
			return TEN_CENTIMETERS;
		} else {
			throw new IllegalArgumentException("Not a valid unit base.");
		}
	}
	
}
