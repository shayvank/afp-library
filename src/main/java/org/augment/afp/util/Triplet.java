package org.augment.afp.util;

import java.util.ArrayList;
import java.util.List;

/**
 * A triplet is a self-identifying parameter in a structured field that contains three components: the length of the triplet, an ID
 * identifying the triplet, and the associated parameters.
 *
 */
public class Triplet {

	private byte[] data;
	
	/**
	 * Creates a triplet from the entire triplet data (including length and id).
	 * @param data triplet data
	 */
	public Triplet(final byte[] data) {
		this.data = data;
	}
	
	/**
	 * Length of the triplet, including the length of the length of the triplet.
	 * 
	 * @return length of the triplet
	 */
	public int getLength() {
		return ByteUtils.toUnsignedByte(data[0]);
	}
	
	/**
	 * Identifies the triplet. See the MO:DCA documentation to find the valid values.
	 * 
	 * @return triplet id
	 */
	public int getCode() {
		return ByteUtils.toUnsignedByte(data[1]);
	}
	
	/**
	 * Contents of the triplet as identified by the MO:DCA architecture.
	 * 
	 * @return contents of the triplet
	 */
	public byte[] getContents() {
		int len = getLength() - 2;
		byte[] bytes = new byte[len];
		System.arraycopy(data, 2, bytes, 0, len);
		return bytes;
	}
	
	/**
	 * Given a data array, the starting read location in the array, and the number of bytes to read, generates
	 * a list of triplets.
	 * 
	 * @param data data array, likely from a structured field
	 * @param readIndex start position in the data array to begin reading triplet data
	 * @param readLength length of the data array to read triplet data
	 * @return list of triplets
	 */
	public static List<Triplet> parse(final byte[] data, final int readIndex, final int readLength) {		
		List<Triplet> tripletList = new ArrayList<>();
		
		int indexNow = readIndex;
		
		int lengthToRead = readLength + indexNow;
		
		while (indexNow < lengthToRead) {
			int tripLen = ByteUtils.toUnsignedByte(data[indexNow]);						
			byte[] tripData = ByteUtils.arraycopy(data, indexNow, tripLen);			
			tripletList.add(new Triplet(tripData));
			
			indexNow += tripLen;
		}
				
		return tripletList;
	}
}
