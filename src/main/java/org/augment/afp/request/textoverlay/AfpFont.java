package org.augment.afp.request.textoverlay;

public class AfpFont implements Font {

	private String codePageName;
	private String characterSetName;
	private String codedFontName;
	private int lid;
	
	private boolean pair;
	
	public AfpFont(final String characterSetName, final String codePageName) {
		this.pair = true;
		this.characterSetName = characterSetName;
		this.codePageName = codePageName;
	}
	
	public AfpFont(final String characterSetName, final String codePageName, final int lid) {
		this(characterSetName, codePageName);
		this.lid = lid;
	}
	
	public AfpFont(final String codedFontName) {
		this.pair = false;
		this.codedFontName = codedFontName;
	}
	
	public AfpFont(final String codedFontName, final int lid) {
		this(codedFontName);
		this.lid = lid;
	}
	
	public String getCharacterSetName() {
		return characterSetName;
	}
	
	public String getCodePageName() {
		return codePageName;
	}
	
	public String getCodedFontName() {
		return codedFontName;
	}
	
	@Override
	public int getLid() {
		return lid;
	}
	
	@Override
	public void setLid(final int lid) {
		this.lid = lid;
	}
	
	public boolean isPair() {
		return pair;
	}
}
