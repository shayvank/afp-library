package org.augment.afp.request.textoverlay;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.Triplet;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.Validations;

public class MapCodedFont {

	private List<AfpFont> afs;
	
	public MapCodedFont() {
		this.afs = new ArrayList<>();
	}
	
	void addAfpFont(final AfpFont af) {
		this.afs.add(af);
	}
	
	public AfpFont findAfpFont(final AfpFont afpFont) {
		AfpFont font = null;
		
		for (AfpFont af: afs) {
			if (af.isPair() && af.getCharacterSetName().equals(afpFont.getCharacterSetName()) && af.getCodePageName().equals(afpFont.getCodePageName())) {
				font = af;
				break;
			} else if (!af.isPair() && af.getCodedFontName().equals(afpFont.getCodedFontName())) {
				font = af;
				break;
			}
		}
		
		return font;
	}
	
	public int getMaxLid() {
		int lid = 0;
		for (AfpFont af: afs) {
			if (af.getLid() > lid) {
				lid = af.getLid();
			}
		}
		return lid;
	}
	
	public List<AfpFont> getAfpFonts() {
		return afs;
	}
	
	public static boolean is(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.CODED_FONT && sf.getTypeCode() == TypeCode.MAP;
	}
	
	public static MapCodedFont parse(final StructuredField sf) {
		Validations.notNull(sf, "StructuredField object must not be null.");
		
		MapCodedFont mcf = new MapCodedFont();
		
		// load repeating group information
		byte[] data = sf.getData();
		int index = 0;
		
		while (index < data.length) {
			int rgLen = ByteUtils.toShort(ByteUtils.arraycopy(data, index, 2));
			List<Triplet> triplets = Triplet.parse(data, index+2, rgLen - 2);
			
			String characterSetName = "";
			String codePageName = "";
			String codedFontName = "";
			boolean pair = true;
			int lid = 0;
			
			for (Triplet triplet: triplets) {
				if (triplet.getCode() == 0x02) {
					byte fqnType = triplet.getContents()[0];
					if (fqnType == (byte)0x85) {
						byte[] nameBytes = ByteUtils.arraycopy(triplet.getContents(), 2, triplet.getLength() - 4);
						codePageName = ByteUtils.getDataEncoding().stringValue(nameBytes);
					} else if (fqnType == (byte)0x86) {
						byte[] nameBytes = ByteUtils.arraycopy(triplet.getContents(), 2, triplet.getLength() - 4);
						characterSetName = ByteUtils.getDataEncoding().stringValue(nameBytes);						
					} else if (fqnType == (byte)0x8E) {
						byte[] nameBytes = ByteUtils.arraycopy(triplet.getContents(), 2, triplet.getLength() - 4);
						codedFontName = ByteUtils.getDataEncoding().stringValue(nameBytes);
						pair = false;
					}
				} else if (triplet.getCode() == 0x24 && triplet.getContents()[0] == (byte)0x05) {
					lid = ByteUtils.toUnsignedByte(triplet.getContents()[1]);
				}
			}
			
			AfpFont font = null;
			if (pair) {
				font = new AfpFont(characterSetName, codePageName, lid);
			} else {
				font = new AfpFont(codedFontName, lid);
			}
			
			mcf.addAfpFont(font);
			index += rgLen;
		}
		
		return mcf;
	}
	
	public static byte[] format(final MapCodedFont mapCodedFont) throws IOException {
		Validations.notNull(mapCodedFont, "MapCodedFont object must not be null.");
		
		ByteArrayOutputStream rpGrp = new ByteArrayOutputStream();
				
		for (AfpFont af: mapCodedFont.getAfpFonts()) {
			ByteArrayOutputStream grp = new ByteArrayOutputStream();
			
			// write out the group entries
			
			if (af.isPair()) {
				// 0x02 - Code Page Name Reference (in ebcdic - minus T1)
				byte[] codePageName = ByteUtils.getDataEncoding().byteValue(af.getCodePageName());
				int cpnrLen = 4 + codePageName.length;
				grp.write(new byte[] {(byte) cpnrLen, 0x02, (byte)0x85, 0x00}); 
				grp.write(codePageName);
				
				// 0x02 - character set name reference (in ebcdic - minus C1)
				byte[] characterSetName = ByteUtils.getDataEncoding().byteValue(af.getCharacterSetName());
				int csnrLen = 4 + characterSetName.length;
				grp.write(new byte[] {(byte) csnrLen, 0x02, (byte)0x86, 0x00}); 
				grp.write(characterSetName);
			} else {
				// 0x02 - coded font name reference (in ebcdic - minus X1)
				byte[] codedFontName = ByteUtils.getDataEncoding().byteValue(af.getCodedFontName());
				int cfnrLen = 4 + codedFontName.length;
				grp.write(new byte[] {(byte) cfnrLen, 0x02, (byte)0x8E, 0x00}); 
				grp.write(codedFontName);				
			}
			// 0x02 - Fully Qualified Name
			grp.write(new byte[] {0x04, 0x24, (byte) 0x05, (byte) af.getLid()});
			
			byte[] grpBytes = grp.toByteArray();
			rpGrp.write(ByteUtils.fromShort((short) (grpBytes.length + 2)));
			rpGrp.write(grpBytes);
		}
		
		byte[] rpGrpBytes = rpGrp.toByteArray();
		
		// at this point repeating group has all the repeating triplets.
		// we need to provide for the 2 byte length of the triplets
		
		// sf length should be 8 + 2 (for the len of repeating group) + rpgrp length
		int sfLength = 8 + rpGrpBytes.length;
		
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();
		sfBytes.write(ByteUtils.fromShort((short) (sfLength)));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.MAP.toByte());
		sfBytes.write(CategoryCode.CODED_FONT.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
				
		sfBytes.write(rpGrpBytes);
		
		return sfBytes.toByteArray();
	}

}
