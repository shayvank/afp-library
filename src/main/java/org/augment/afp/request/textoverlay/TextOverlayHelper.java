package org.augment.afp.request.textoverlay;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.List;

import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.request.comment.Comment;
import org.augment.afp.request.comment.CommentHelper;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.Coordinate;
import org.augment.afp.util.Orientation;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldBuilder;
import org.augment.afp.util.Triplet;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.Validations;

public class TextOverlayHelper {

	private TextOverlayHelper() {
		
	}
	
	public static TextOverlay parse(final byte[] textOverlayObject, final SizeDescriptor size) throws IOException {
		TextOverlay returnValue = null;
		TextOverlay.Builder builder = new TextOverlay.Builder();
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(textOverlayObject));
		
		boolean isTextOverlay = false;
		StringBuilder commentBuilder = new StringBuilder();
		
		StructuredField sf = null;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getCategoryCode() == CategoryCode.PRESENTATION_TEXT && sf.getTypeCode() == TypeCode.DATA) {
					byte[] ptxData = sf.getData();
					// we need to iterate over the triplets to get the needed values
					// [0] - 2B
					// [1] - D3					
					
					List<Triplet> triplets = Triplet.parse(ptxData, 2, ptxData.length - 2);
					
					double x = 0.0;
					double y = 0.0;
					
					Orientation orientation = Orientation.DEGREES_0;
					
					for (Triplet triplet: triplets) {
						if (triplet.getCode() == 0xF7) { // STO
							orientation = Orientation.findByValue(ByteUtils.toUnsignedShort(ByteUtils.arraycopy(triplet.getContents(), 0, 2)));
							builder.withDirection(orientation.getIntValue());
						} else if (triplet.getCode() == 0xC7) { // AMI
							int xAmi = ByteUtils.toShort(ByteUtils.arraycopy(triplet.getContents(), 0, 2));
							x = xAmi * 1.0 / size.getXDpi().getValue();
						} else if (triplet.getCode() == 0xD3) { // AMB
							int yAmb = ByteUtils.toShort(ByteUtils.arraycopy(triplet.getContents(), 0, 2));
							y = yAmb * 1.0 / size.getYDpi().getValue();							
						} else if (triplet.getCode() == 0xDB) { // TRN
							byte[] trn = triplet.getContents();
							builder.withText(ByteUtils.getDataEncoding().stringValue(trn));
						}
					}
					
					double newX;
					double newY;
					switch(orientation) {
					case DEGREES_90:
						newX = size.getXSizeInUnits() - y;
						newY = x;		
						break;
					case DEGREES_180:
						newX = size.getXSizeInUnits() - x;
						newY = size.getYSizeInUnits() - y;
						break;
					case DEGREES_270:
						newX = y;
						newY = size.getYSizeInUnits() - x;
						break;
					default: 
						newX = x;
						newY = y;	
					}
					
					builder.withPosition(new Coordinate(newX, newY));
				} else if (Comment.is(sf)) {
					Comment comment = CommentHelper.parse(sf);
					if (comment.getValue().equals("TextOverlay")) {
						isTextOverlay = true;
					} else {
						commentBuilder.append(comment.getValue());
					}
				}
			}
		} catch (EOFException eof) {
			// done reading
		}
		
		if (isTextOverlay) {
			builder.withDescription(commentBuilder.toString());
			returnValue = builder.build();
		}
		
		return returnValue;
	}
	
	public static byte[] format(final TextOverlay textOverlay, final SizeDescriptor size) throws IOException {
		
		Validations.notNull(textOverlay, "TextOverlay object must not be null.");
		
		String text = textOverlay.getText();
		
		Validations.notEmpty(text, "This api doesn't support creating TextOverlay with empty text value.");
		
		/*
		 * BPT
		 *  NOP
		 *  NOP
		 *  PTX
		 * EPT
		 */
		
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();
		
		// BPT structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.BEGIN, CategoryCode.PRESENTATION_TEXT).build().bytes());
		
		Comment nopTextOverlay = new Comment("TextOverlay");
		sfBytes.write(CommentHelper.format(nopTextOverlay).bytes());
		
		// if there is a description in the textoverlay, it will be placed here
		String description = textOverlay.getDescription();
		if (description != null && description.length() > 0) {
			Comment nopDescription = new Comment(description);
			sfBytes.write(CommentHelper.format(nopDescription).bytes());
		}
		
		// need to gather the presentation text here
		ByteArrayOutputStream ptxData = new ByteArrayOutputStream();
		ptxData.write(0x2B); // control sequence prefix
		ptxData.write(0xD3); // control sequence class
		// STO
		ptxData.write(0x06);
		ptxData.write(0xF7);
		Orientation orientation = Orientation.findByInt(textOverlay.getDirection());
		ptxData.write(orientation.getByteValue());
		int bOrntion = textOverlay.getDirection() + 90;
		if (bOrntion > 270) {
			bOrntion = 0;
		}
		ptxData.write(Orientation.findByInt(bOrntion).getByteValue());
		// SCFL
		ptxData.write(0x03);
		ptxData.write(0xF1);
		ptxData.write(textOverlay.getFont().getLid()); 
		
		double x = textOverlay.getPosition().getX();
		double y = textOverlay.getPosition().getY();
		double newX;
		double newY;
		switch(orientation) {
		case DEGREES_90:
			newX = y;
			newY = size.getXSizeInUnits() - x;		
			break;
		case DEGREES_180:
			newX = size.getXSizeInUnits() - x;
			newY = size.getYSizeInUnits() - y;
			break;
		case DEGREES_270:
			newX = size.getYSizeInUnits() - y;
			newY = x;
			break;
		default: 
			newX = x;
			newY = y;	
		}
		
		// AMI
		ptxData.write(0x04);
		ptxData.write(0xC7);
		ptxData.write(ByteUtils.fromShort((short) (newX * size.getXDpi().getValue())));
		// AMB
		ptxData.write(0x04);
		ptxData.write(0xD3);
		ptxData.write(ByteUtils.fromShort((short) (newY * size.getYDpi().getValue())));
		// TRN
		int trnLen = text.length() + 2;
		ptxData.write(ByteUtils.toUnsignedByte((byte) trnLen));
		ptxData.write(0xDB);
		ptxData.write(ByteUtils.getDataEncoding().byteValue(text));
		// NOP
		ptxData.write(0x02);
		ptxData.write(0xF8);
		
		// PTX structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DATA, CategoryCode.PRESENTATION_TEXT)
				.addData(ptxData.toByteArray())
				.build().bytes());
		
		// EPT structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.END, CategoryCode.PRESENTATION_TEXT).build().bytes());
		
		return sfBytes.toByteArray();
	}
	
	public static boolean isBPT(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.PRESENTATION_TEXT && sf.getTypeCode() == TypeCode.BEGIN;
	}
	
	public static boolean isEPT(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.PRESENTATION_TEXT && sf.getTypeCode() == TypeCode.END;
	}
	
}
