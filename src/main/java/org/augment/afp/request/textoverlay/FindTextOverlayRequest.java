package org.augment.afp.request.textoverlay;

import java.util.List;

public class FindTextOverlayRequest implements TextOverlayRequest {

	private String textSearch;
	
	private String descriptionSearch;
	
	private List<TextOverlay> result;
	
	private FindTextOverlayRequest(final String textSearch, final String descrptionSearch) {
		this.textSearch = textSearch;
		this.descriptionSearch = descrptionSearch;		
	}
	
	public String getTextSearch() {
		return textSearch;
	}
	
	public String getDescriptionSearch() {
		return descriptionSearch;
	}
	
	public List<TextOverlay> getResult() {
		return result;
	}
	
	public void setResult(final List<TextOverlay> result) {
		this.result = result;
	}
	
	@Override
	public boolean isUpdate() {
		return false;
	}
	
	public static FindTextOverlayRequest findByText(final String matcher) {
		return new FindTextOverlayRequest(matcher, ".*");
	}
	
	public static FindTextOverlayRequest findByDescription(final String matcher) {
		return new FindTextOverlayRequest(".*", matcher);
	}
	
	public static FindTextOverlayRequest findByTextAndDescription(final String textMatcher, final String descriptionMatcher) {
		return new FindTextOverlayRequest(textMatcher, descriptionMatcher);
	}

}
