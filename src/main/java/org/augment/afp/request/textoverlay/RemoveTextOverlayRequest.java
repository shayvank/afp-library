package org.augment.afp.request.textoverlay;

public class RemoveTextOverlayRequest implements TextOverlayRequest {

	private boolean success = false;
	
	private String textSearch;
	
	private String descriptionSearch;
	
	public RemoveTextOverlayRequest(final String textSearch, final String descriptionSearch) {
		this.textSearch = textSearch;
		this.descriptionSearch = descriptionSearch;
	}
	
	public String getTextSearch() {
		return textSearch;
	}
	
	public String getDescriptionSearch() {
		return descriptionSearch;
	}
	
	public void setSuccess(final boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}
	
	public static RemoveTextOverlayRequest removeByText(final String matcher) {
		return new RemoveTextOverlayRequest(matcher, ".*");
	}
	
	public static RemoveTextOverlayRequest removeByDescription(final String matcher) {
		return new RemoveTextOverlayRequest(".*", matcher);
	}	

}
