package org.augment.afp.request.textoverlay;

import org.augment.afp.util.Coordinate;

public class TextOverlay {

	private String text;
	private Font font;
	private int direction;
	private Coordinate position;
	private String description;
	
	private TextOverlay(final String text, final Font font, final int direction, final Coordinate position, final String description) {
		this.text = text;
		this.font = font;
		this.direction = direction;
		this.position = position;
		this.description = description;
	}
	
	public String getText() {
		return text;
	}
	
	public Font getFont() {
		return font;
	}
	
	public int getDirection() {
		return direction;
	}
	
	public Coordinate getPosition() {
		return position;
	}
	
	public String getDescription() {
		return description;
	}
	
	public static class Builder {
		private String text;
		private Font font;
		private int direction;
		private Coordinate position;
		private String description;
		
		public Builder() {
			// we will set everything using the methods below
		}
		
		public Builder withText(final String text) {
			if (text != null && (text.contains("\n") || text.contains("\r"))) {
				throw new IllegalArgumentException("Newline characters are not supported by TextOverlay.");
			}
			this.text = text;
			return this;
		}
		
		public Builder withFont(final Font font) {
			this.font = font;
			return this;
		}
		
		public Builder withDirection(final int direction) {
			if (direction != 0 && direction != 90 && direction != 180 && direction != 270) {
				throw new IllegalArgumentException("TextOverlay direction must be either 0, 90, 180, or 270");
			}
			this.direction = direction;
			return this;
		}
		
		public Builder withPosition(final Coordinate position) {
			this.position = position;
			return this;
		}
		
		public Builder withDescription(final String description) {
			this.description = description;
			return this;
		}
		
		public TextOverlay build() {
			return new TextOverlay(text, font, direction, position, description);
		}
	}
}
