package org.augment.afp.request.textoverlay;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.Triplet;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.Validations;

public class MapDataResource {
	
	private List<TrueTypeFont> ttfs;
	
	public MapDataResource() {
		this.ttfs = new ArrayList<>();
	}
	
	void addTrueTypeFont(final TrueTypeFont ttf) {
		this.ttfs.add(ttf);
	}
	
	public TrueTypeFont findTrueTypeFont(final String name, final int pt) {
		TrueTypeFont font = null;
		
		for (TrueTypeFont tt: ttfs) {
			if (tt.getName().equals(name) && tt.getPt() == pt) {
				font = tt;
				break;
			}
		}
		return font;
	}
	
	public int getMaxLid() {
		int lid = 0;
		for (TrueTypeFont tt: ttfs) {
			if (tt.getLid() > lid) {
				lid = tt.getLid();
			}
		}
		return lid;
	}
	
	public List<TrueTypeFont> getTrueTypeFonts() {
		return ttfs;
	}
	
	public static boolean is(StructuredField sf) {
		return (sf.getCategoryCode() == CategoryCode.DATA_RESOURCE && sf.getTypeCode() == TypeCode.MAP);
	}
	
	public static MapDataResource parse(final StructuredField sf) {
		Validations.notNull(sf, "StructuredField object must not be null.");
		
		MapDataResource resource = new MapDataResource();
		
		// load repeating group information
		byte[] data = sf.getData();
		int index = 0;
		
		while (index < data.length) {
			int rgLen = ByteUtils.toShort(ByteUtils.arraycopy(data, index, 2));
			List<Triplet> triplets = Triplet.parse(data, index+2, rgLen - 2);
			
			int pt = 0;
			String name = "";
			int lid = 0;
			
			for (Triplet triplet: triplets) {
				if (triplet.getCode() == 0x8b) {
					int pts = ByteUtils.toShort(ByteUtils.arraycopy(triplet.getContents(), 2, 2));
					pt = pts / 20;
				} else if (triplet.getCode() == 0x02) {
					byte fqnType = triplet.getContents()[0];
					if (fqnType == (byte)0xDE) {
						byte[] nameBytes = ByteUtils.arraycopy(triplet.getContents(), 2, triplet.getLength() - 4);
						name = ByteUtils.getDataEncoding().stringValue(nameBytes);
					} else if (fqnType == (byte)0xBE) {
						lid = ByteUtils.toUnsignedByte(triplet.getContents()[2]);
					}
				}
			}
	
			resource.addTrueTypeFont(new TrueTypeFont(name, pt, lid));
			index += rgLen;
		}
		
		return resource;
	}
	
	public static byte[] format(final MapDataResource mapDataResource) throws IOException {
		Validations.notNull(mapDataResource, "MapDataResource object must not be null.");
		
		ByteArrayOutputStream rpGrp = new ByteArrayOutputStream();
		
		int sfLength = 8;
		
		
		for (TrueTypeFont ttf: mapDataResource.getTrueTypeFonts()) {
			ByteArrayOutputStream grp = new ByteArrayOutputStream();
			
			// write out the group entries
			
			// 0x10 - object class
			grp.write(new byte[] {0x18, 0x10, 0x00, 0x41, 0x00, 0x00, (byte) 0xA8, 0x00, 0x06, 0x07, 0x2B, 0x12, 0x00, 0x04, 0x01, 0x01, 0x33, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });
			// 0x50 - Encoding Scheme ID
			grp.write(new byte[] {0x04, 0x50});
			grp.write(ByteUtils.fromShort((short) ByteUtils.getDataEncoding().getScheme()));
			// 0x02 - Code Page Name Reference (in ebcdic - minus T1)
			byte[] codePageName = ByteUtils.getDataEncoding().byteValue(ByteUtils.getDataEncoding().getCodePageName());
			int cpnrLen = 4 + codePageName.length;
			grp.write(new byte[] {(byte) cpnrLen, 0x02, (byte)0x85, 0x00}); //(byte)0xE3, (byte)0xF1, (byte)0xE5, (byte)0xF1, (byte)0xF0, (byte)0xF5, (byte)0xF0, (byte)0xF0});
			grp.write(codePageName);
			// 0x8B - Data-Object Font Descriptor (pt size)
			grp.write(0x10);
			grp.write(0x8B);
			grp.write(0x00);
			grp.write(0x20);
			int ptSize = ttf.getPt() * 20;
			grp.write(ByteUtils.fromShort((short) ptSize));
			grp.write(new byte[] {0x00, 0x00});
			grp.write(new byte[] {0x00, 0x00});
			grp.write(new byte[] {0x00, 0x03});
			grp.write(new byte[] {0x00, 0x01});
			grp.write(new byte[] {0x00, 0x00});
			// 0x01 - Coded Graphic Character Set Global Identifier
			grp.write(new byte[] {0x06, 0x01, 0x00, 0x00});
			grp.write(ByteUtils.fromShort((short) ByteUtils.getDataEncoding().getCodePage()));
			// 0x02 - Fully Qualified Name
			byte[] fontName = ByteUtils.getDataEncoding().byteValue(ttf.getName());
			int deLen = 4 + fontName.length;
			grp.write(deLen);
			grp.write(0x02);
			grp.write(0xDE);
			grp.write(0x00);
			grp.write(fontName);
			// 0x02 - Fully Qualified Name
			grp.write(new byte[] {0x05, 0x02, (byte) 0xBE, 0x00, (byte) ttf.getLid()});
			
			byte[] grpBytes = grp.toByteArray();
			int grpLen = grpBytes.length + 2;
			rpGrp.write(ByteUtils.fromShort((short) (grpLen)));
			rpGrp.write(grp.toByteArray());
			
			sfLength += grpLen;
		}
		
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();
		sfBytes.write(ByteUtils.fromShort((short) (sfLength)));
		sfBytes.write(0xd3);
		sfBytes.write(TypeCode.MAP.toByte());
		sfBytes.write(CategoryCode.DATA_RESOURCE.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		
		sfBytes.write(rpGrp.toByteArray());
		
		return sfBytes.toByteArray();
	}
	
}
