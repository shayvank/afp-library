package org.augment.afp.request.textoverlay;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.request.ActionRequest;
import org.augment.afp.request.RequestHandler;
import org.augment.afp.request.comment.Comment;
import org.augment.afp.request.comment.CommentHelper;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class TextOverlayRequestHandler implements RequestHandler {

	@Override
	public String getName() {
		return TextOverlayRequestHandler.class.getSimpleName();
	}
	
	@Override
	public boolean canHandle(final ActionRequest request) {
		return request instanceof TextOverlayRequest;
	}
	
	@Override
	public byte[] handle(final ActionRequest request, final byte[] data) throws IOException {
		byte[] returnBytes = null;
		
		if (request instanceof InsertTextOverlayRequest) {
			returnBytes = handleInsertTextOverlayRequest((InsertTextOverlayRequest) request, data);
		} else if (request instanceof RemoveTextOverlayRequest) {
			returnBytes = handleRemoveTextOverlayRequest((RemoveTextOverlayRequest) request, data);
		} else if (request instanceof FindTextOverlayRequest) {
			handleFindTextOverlayRequest((FindTextOverlayRequest) request, data);
			returnBytes = data;
		} else {
			throw new IllegalArgumentException("No handler defined for request provided: " + request.getClass().getSimpleName());
		}
		
		return returnBytes;
	}
	
	private SizeDescriptor getPageDescriptor(DataInputStream dis) throws IOException {
		dis.mark(0);
		SizeDescriptor sizeDes = null;
		try {
			StructuredField sf = null;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (SizeDescriptor.isPageDescriptor(sf)) {
					sizeDes = SizeDescriptor.parse(sf);
					break;
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		dis.reset();
		
		return sizeDes;
	}
	
	@SuppressWarnings("resource")
	private byte[] handleInsertTextOverlayRequest(final InsertTextOverlayRequest request, final byte[] data) throws IOException {
		boolean success = false;
		
		TextOverlay textOverlay = request.getValue();
		
		boolean trueType = textOverlay.getFont() instanceof TrueTypeFont;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		StructuredField sf = null;
		
		SizeDescriptor size = getPageDescriptor(dis);
				
		int mcfMaxLid = 0;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (MapCodedFont.is(sf)) {
					MapCodedFont mcf = MapCodedFont.parse(sf);
					int tmpLid = mcf.getMaxLid();
					if (mcfMaxLid < tmpLid) {
						mcfMaxLid = tmpLid;
					}
				} else if (MapDataResource.is(sf)) {
					MapDataResource mapDataResource = MapDataResource.parse(sf);
					int tmpLid = mapDataResource.getMaxLid();
					if (mcfMaxLid < tmpLid) {
						mcfMaxLid = tmpLid;
					}
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		dis = new DataInputStream(new ByteArrayInputStream(data));
		sf = null;
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		boolean mdrFound = false;
		boolean mcfFound = false;
		
		ByteArrayOutputStream aeg = new ByteArrayOutputStream();
		
		ByteArrayOutputStream output = out;
		
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if(sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.PAGE_GROUP) {
					throw new IllegalArgumentException("Cannot add text overlays to page groups.");
				} else if (sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.PAGE) {
					output.write(TextOverlayHelper.format(textOverlay, size));
					output.write(sf.bytes());
					success = true;
				} else if (MapDataResource.is(sf) && trueType) {
					mdrFound = true;
					MapDataResource mapDataResource = MapDataResource.parse(sf);
					TrueTypeFont requestFont = (TrueTypeFont) textOverlay.getFont();
					TrueTypeFont alreadyMapped = mapDataResource.findTrueTypeFont(requestFont.getName(), requestFont.getPt());
					if (alreadyMapped == null) {
						int lid = mcfMaxLid + 1;
						requestFont.setLid(lid);
						mapDataResource.addTrueTypeFont(requestFont);
						output.write(MapDataResource.format(mapDataResource));
					} else {
						textOverlay.getFont().setLid(alreadyMapped.getLid());
						output.write(sf.bytes());
					}
				} else if (MapCodedFont.is(sf) && !trueType) {
					mcfFound = true;
					MapCodedFont mapCodedFont = MapCodedFont.parse(sf);
					AfpFont requestFont = (AfpFont) textOverlay.getFont();
					AfpFont alreadyMapped = mapCodedFont.findAfpFont(requestFont);
					if (alreadyMapped == null) {
						int lid = mcfMaxLid + 1;
						requestFont.setLid(lid);
						mapCodedFont.addAfpFont(requestFont);
						output.write(MapCodedFont.format(mapCodedFont));
					} else {
						textOverlay.getFont().setLid(alreadyMapped.getLid());
						output.write(sf.bytes());
					}
				} else if (sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.ACTIVE_ENVIRONMENT_GROUP) {
					output.write(sf.bytes());
					output = aeg;
				} else if (sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.ACTIVE_ENVIRONMENT_GROUP) {
					if (trueType && !mdrFound) {
						MapDataResource mapDataResource = new MapDataResource();
						int lid = mcfMaxLid + 1;
						TrueTypeFont requestFont = (TrueTypeFont) textOverlay.getFont();
						requestFont.setLid(lid);
						mapDataResource.addTrueTypeFont(requestFont);
						output.write(MapDataResource.format(mapDataResource));
					} else if (!trueType && !mcfFound) {
						MapCodedFont mapCodedFont = new MapCodedFont();
						int lid = mcfMaxLid + 1;
						AfpFont requestFont = (AfpFont) textOverlay.getFont();
						requestFont.setLid(lid);
						mapCodedFont.addAfpFont(requestFont);
						out.write(MapCodedFont.format(mapCodedFont));						
					}
					output = out;
					output.write(aeg.toByteArray());
					output.write(sf.bytes());
				} else {
					output.write(sf.bytes());
				}
				// once we know we are in the active environment group, we need to do the following:
				// 1) We need to track MCF font Lids, these can't be reused.
				// 2) If we have a MDR, we need to see if our font is already provided, if so, get the Lid.
				// 3) If we get through all MDR and our font isn't listed, we need to add a MDR to the AEG.
				// 4) We need to determine what the resolution is in the PGD and PTD.  If those don't exist, we need to define them in the AEG.
			}
		} catch (EOFException eof) {
			// end of stream
		} finally {
			try { aeg.close(); } catch (Exception e) {}
		}
		
		request.setSuccess(success);
		
		return out.toByteArray();
	}
	
	private byte[] handleRemoveTextOverlayRequest(final RemoveTextOverlayRequest request, final byte[] data) throws IOException {
		byte[] returnData = data;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		SizeDescriptor size = getPageDescriptor(dis);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StructuredField sf = null;
		ByteArrayOutputStream textOverlay = new ByteArrayOutputStream();
		boolean collectSF = false;
		boolean commentFound = false;
		boolean found = false;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (TextOverlayHelper.isBPT(sf)) {
					textOverlay = new ByteArrayOutputStream();
					collectSF = true;
					textOverlay.write(sf.bytes());
				} else if (collectSF) {
					textOverlay.write(sf.bytes());

					if (Comment.is(sf) && !commentFound) {
						Comment comment = CommentHelper.parse(sf);
						commentFound = comment.getValue().equals("TextOverlay");		
					} else if (TextOverlayHelper.isEPT(sf)) {
						collectSF = false;
						
						if (commentFound) {
							commentFound = false;
							TextOverlay overlay = TextOverlayHelper.parse(textOverlay.toByteArray(), size);
							if (overlay.getText() != null && overlay.getText().matches(request.getTextSearch()) && overlay.getDescription().matches(request.getDescriptionSearch())) {
								// do nothing, which effectively removes
								found = true;
							} else {
								out.write(textOverlay.toByteArray());
							}
						} else {
							out.write(textOverlay.toByteArray());
						}
					}
				} else {
					out.write(sf.bytes());
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(found);
		if (found) {
			returnData = out.toByteArray();
		}
		
		return returnData;
	}
	
	private void handleFindTextOverlayRequest(final FindTextOverlayRequest request, final byte[] data) throws IOException {
		List<TextOverlay> result = new ArrayList<>();
		
		// we need to iterate over the structured fields and find the presentation text sections that are text overlays
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		boolean collectSF = false;
		boolean commentFound = false;
		StructuredField sf = null;
		SizeDescriptor size = null;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (SizeDescriptor.isPageDescriptor(sf)) {
					size = SizeDescriptor.parse(sf);
				} else if (TextOverlayHelper.isBPT(sf)) {
					out = new ByteArrayOutputStream();
					collectSF = true;
					out.write(sf.bytes());
				} else if (collectSF) {
					out.write(sf.bytes());
					
					if (Comment.is(sf) && !commentFound) {
						Comment comment = CommentHelper.parse(sf);
						commentFound = comment.getValue().equals("TextOverlay");
					} else if (TextOverlayHelper.isEPT(sf)) {
						collectSF = false;
						
						if (commentFound) {
							TextOverlay overlay = TextOverlayHelper.parse(out.toByteArray(), size);
							if (overlay.getText() != null && overlay.getText().matches(request.getTextSearch()) && overlay.getDescription().matches(request.getDescriptionSearch())) {
								result.add(overlay);
							}
						}
						commentFound = false;
					}
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setResult(result);
	}
}
