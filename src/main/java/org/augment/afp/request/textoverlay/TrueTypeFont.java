package org.augment.afp.request.textoverlay;


public class TrueTypeFont implements Font {

	private String name;
	private int pt;
	private int lid;
	
	public TrueTypeFont(final String name, final int pt) {
		this.name = name;
		this.pt = pt;
	}
	
	public TrueTypeFont(final String name, final int pt, final int lid) {
		this.name = name;
		this.pt = pt;
		this.lid = lid;
	}
	
	public String getName() {
		return name;
	}
	
	public int getPt() {
		return pt;
	}
	
	@Override
	public void setLid(final int lid) {
		this.lid = lid;
	}
	
	@Override
	public int getLid() {
		return lid;
	}
}
