package org.augment.afp.request.textoverlay;

public class InsertTextOverlayRequest implements TextOverlayRequest {

	private final TextOverlay textOverlay;
	private boolean success = false;
	
	public InsertTextOverlayRequest(final TextOverlay textOverlay) {
		this.textOverlay = textOverlay;
	}
		
	public TextOverlay getValue() {
		return textOverlay;
	}
		
	public void setSuccess(final boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}

}
