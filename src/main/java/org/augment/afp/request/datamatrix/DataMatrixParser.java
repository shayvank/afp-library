package org.augment.afp.request.datamatrix;

import org.augment.afp.request.comment.Comment;
import org.augment.afp.request.comment.CommentHelper;
import org.augment.afp.util.BigEndianBitSet;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.Coordinate;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class DataMatrixParser {

	private DataMatrixBarcode.Builder barcodeBuilder;
	private StringBuilder commentBuilder;
	
	private int pageDpi;
		
	public DataMatrixParser(final int pageDpi) {
		this.pageDpi = pageDpi;
		barcodeBuilder = new DataMatrixBarcode.Builder();
		commentBuilder = new StringBuilder();
	}
	
	public void addSFtoParse(final StructuredField sf) {
		if (sf.getCategoryCode() == CategoryCode.BAR_CODE) {
			parseBarcodeSF(sf);
		} else if (sf.getCategoryCode() == CategoryCode.OBJECT_AREA && sf.getTypeCode() == TypeCode.POSITION) {
			//// we can get the x and y position
			byte[] xArray = ByteUtils.arraycopy(sf.getData(), 3, 2);
			double xPos = (double)ByteUtils.toShort(xArray);					
			byte[] yArray = ByteUtils.arraycopy(sf.getData(), 6, 2);
			double yPos = (double)ByteUtils.toShort(yArray);
			Coordinate coord = new Coordinate(xPos / pageDpi, yPos / pageDpi);
			barcodeBuilder.withLocation(coord);
		} else if (Comment.is(sf)) {
			// this is a comment			
			commentBuilder.append(CommentHelper.parse(sf).getValue());
		}
		// CategoryCode.ObjectEnvironmentGroup - do nothing, there is nothing in these sf to parse to an object
		// CategoryCode.ObjectArea - do nothing, these are used to define the page the barcode is on and should be need for finding or removing data matrix barcodes
	}
	
	public DataMatrixBarcode build() {
		return barcodeBuilder.withDescription(commentBuilder.toString()).build();
	}
	
	private void parseBarcodeSF(final StructuredField sf) {								
		if (sf.getTypeCode() == TypeCode.DESCRIPTOR) { // BDD
			byte[] data = sf.getData();
			// units per base 
			int resolution = ByteUtils.toShort(ByteUtils.arraycopy(data, 2, 2)) / 10;
			int ybcUPUBRes = ByteUtils.toShort(ByteUtils.arraycopy(data, 4, 2)) / 10;
			if (ybcUPUBRes > resolution) {
				resolution = ybcUPUBRes;
			}
			barcodeBuilder.withResolution(resolution);
			// presentation space isn't important for barcode object.  we only care about location with this object.
			//barcodeType -  do we want to validate here, again? we would already be validating it prior in the find request code
			// module width in mils
			barcodeBuilder.withElementSizeInMils(ByteUtils.toUnsignedByte(data[17]));
		} else if (sf.getTypeCode() == TypeCode.DATA) { // BDA
			byte[] data = sf.getData();
			
			// ebcdic2ascii
			BigEndianBitSet ctrlFlag = BigEndianBitSet.valueOf(data[5]);
			barcodeBuilder.withEbcdic2Ascii(ctrlFlag.get(0));
			// rows size, number of rows
			barcodeBuilder.withGridSize(ByteUtils.toShort(ByteUtils.arraycopy(data, 6, 2)), ByteUtils.toShort(ByteUtils.arraycopy(data, 8, 2)));
			// byte 10, 11, 12, 13, 14 ignore
			// payload
			barcodeBuilder.withPayload(ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(data, 15, data.length - 15)));
		}
		// BBC is skipped, nothing to do with it
		// MBC is skipped, nothing to do with it
		// EBC is skipped, nothing to do with it
	}

}
