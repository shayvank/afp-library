package org.augment.afp.request.datamatrix;

import java.util.List;

public class FindDataMatrixBarcodeRequest implements DataMatrixBarcodeRequest {
	
	private String payloadSearch;
	
	private String descriptionSearch;
	
	private List<DataMatrixBarcode> result;
	
	private FindDataMatrixBarcodeRequest(final String payloadSearch, final String descriptionSearch) {
		this.payloadSearch = payloadSearch;
		this.descriptionSearch = descriptionSearch;
	}
	
	public String getPayloadSearch() {
		return payloadSearch;
	}
	
	public String getDescriptionSearch() {
		return descriptionSearch;
	}
	
	public List<DataMatrixBarcode> getResult() {
		return result;
	}
	
	public void setResult(final List<DataMatrixBarcode> result) {
		this.result = result;
	}
	
	@Override
	public boolean isUpdate() {
		return false;
	}
	
	public static FindDataMatrixBarcodeRequest findByPayload(final String matcher) {
		return new FindDataMatrixBarcodeRequest(matcher, ".*");
	}
	
	public static FindDataMatrixBarcodeRequest findByDescription(final String matcher) {
		return new FindDataMatrixBarcodeRequest(".*", matcher);
	}
	
	public static FindDataMatrixBarcodeRequest findByPayloadAndDescription(final String payloadMatcher, final String descriptionMatcher) {
		return new FindDataMatrixBarcodeRequest(payloadMatcher, descriptionMatcher);
	}	

}
