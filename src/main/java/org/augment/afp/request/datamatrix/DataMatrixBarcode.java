package org.augment.afp.request.datamatrix;

import org.augment.afp.util.Coordinate;

public class DataMatrixBarcode {

	private String payload;	
	private int elementSizeInMils;
	private int numberOfColumns;
	private int numberOfRows;	
	private Coordinate location;
	private boolean ebcdic2Ascii;
	private String description;
	private int resolution;
	private int quietZoneInElements;
	
	private DataMatrixBarcode(final String payload, final int elementSize, final int numberOfColumns,
			final int numberOfRows, final Coordinate location, final boolean ebcdic2Ascii, final int resolution, final String description, final int quietZoneInElements) {
		this.payload = payload;
		this.elementSizeInMils = elementSize;
		this.numberOfColumns = numberOfColumns;
		this.numberOfRows = numberOfRows;
		this.location = location;
		this.ebcdic2Ascii = ebcdic2Ascii;
		this.resolution = resolution;
		this.description = description;
		this.quietZoneInElements = quietZoneInElements;
	}
	
	public String getPayload() {
		return payload;
	}
	
	public int getElementSizeInMils() {
		return elementSizeInMils;
	}
	
	public int getNumberOfColumns() {
		return numberOfColumns;
	}
	
	public int getNumberOfRows() {
		return numberOfRows;
	}
	
	public Coordinate getLocation() {
		return location;
	}
	
	public boolean isEbcdic2Ascii() {
		return ebcdic2Ascii;
	}
	
	public String getDescription() {
		return description;
	}
	
	public int getResolution() {
		return resolution;
	}
	
	public int getQuietZoneInElements() {
		return quietZoneInElements;
	}
	
	public static class Builder {
		private String payload;
		private int elementSize = 255;
		private int numberOfColumns = 0;
		private int numberOfRows = 0;
		private Coordinate location = new Coordinate(0.0, 0.0);
		private boolean ebcdic2Ascii = true;
		private int resolution = 240;
		private String description;
		private int quietZoneInElements = 0;
		
		public Builder() {
			// we will set everything using the methods below
		}
		
		public Builder withPayload(final String payload) {
			this.payload = payload;
			return this;
		}
		
		public Builder withLocation(final Coordinate location) {
			this.location = location;
			return this;
		}
		
		public Builder withGridSize(final int numberOfColumns, final int numberOfRows) {
			this.numberOfColumns = numberOfColumns;
			this.numberOfRows = numberOfRows;
			return this;
		}
		
		public Builder withElementSizeInMils(final int size) {
			this.elementSize = size;
			return this;
		}
		
		public Builder withEbcdic2Ascii(final boolean e2a) {
			this.ebcdic2Ascii = e2a;
			return this;
		}
		
		public Builder withResolution(final int resolution) {
			this.resolution = resolution;
			return this;
		}
		
		public Builder withDescription(final String description) {
			this.description = description;
			return this;
		}
		
		public Builder withQuietZoneInElements(final int quietZoneInElements) {
			this.quietZoneInElements = quietZoneInElements;
			return this;
		}
		
		public DataMatrixBarcode build() {
			return new DataMatrixBarcode(payload, elementSize, numberOfColumns, numberOfRows, location, ebcdic2Ascii, resolution, description, quietZoneInElements);
		}
		
	}
}
