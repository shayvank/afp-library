package org.augment.afp.request.datamatrix;

public class RemoveDataMatrixBarcodeRequest implements DataMatrixBarcodeRequest {
	
	private boolean success = false;
	
	private String payloadSearch;
	
	private String descriptionSearch;
	
	public RemoveDataMatrixBarcodeRequest(final String payloadSearch, final String descriptionSearch) {
		this.payloadSearch = payloadSearch;
		this.descriptionSearch = descriptionSearch;
	}
		
	public String getPayloadSearch() {
		return payloadSearch;
	}
	
	public String getDescriptionSearch() {
		return descriptionSearch;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}
	
	public static RemoveDataMatrixBarcodeRequest removeByPayload(final String matcher) {
		return new RemoveDataMatrixBarcodeRequest(matcher, ".*");
	}
	
	public static RemoveDataMatrixBarcodeRequest removeByDescription(final String matcher) {
		return new RemoveDataMatrixBarcodeRequest(".*", matcher);
	}	
}
