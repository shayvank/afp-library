package org.augment.afp.request.datamatrix;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

import org.augment.afp.request.comment.Comment;
import org.augment.afp.request.comment.CommentHelper;
import org.augment.afp.util.BigEndianBitSet;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldBuilder;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.Validations;

public class DataMatrixBarcodeHelper {

	private DataMatrixBarcodeHelper() {
		
	}
	
	public static DataMatrixBarcode parse(final byte[] modcaBarcodeObject, final int pageDpi) throws IOException {
		// we need to re-read the structured fields for the barcode here and create a datamatrix
		DataMatrixParser parser = new DataMatrixParser(pageDpi);

		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(modcaBarcodeObject));
		
		StructuredField sf = null;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				parser.addSFtoParse(sf);
			}
		} catch (EOFException eof) {
			// done reading
		}
		
		return parser.build();
	}
	
	public static byte[] format(final DataMatrixBarcode dataMatrix, final int pageDpi) throws IOException {
		
		Validations.notNull(dataMatrix, "DataMatrix object must not be null.");
		
		String text = dataMatrix.getPayload();
		
		Validations.notEmpty(text, "This api doesn't support creating DataMatrix with empty payload value.");
		
		/*
		 * BBC
		 *  BOG
		 *   OBD - Object Area Descriptor
		 *   OBP - Object Area Position
		 *   MBC - Map Bar Code Object
		 *   BDD - Barcode Data Descriptor
		 *  EOG
		 *  BDA - Bar Code Data
		 * EBC
		 */
		
		double elementSize = (dataMatrix.getElementSizeInMils() * 1.0 / 1000);
		
		short presSpacePerBaseUnit = (short) (dataMatrix.getResolution() * 10);
		//short objectXSize = (short)(elementSize * dataMatrix.getNumberOfColumns() * dataMatrix.getResolution());
		//short objectYSize = (short)(elementSize * dataMatrix.getNumberOfRows() * dataMatrix.getResolution());
		
		short objectXSizeWithQuietZone = (short)(elementSize * (dataMatrix.getNumberOfColumns() + (2 * dataMatrix.getQuietZoneInElements())) * dataMatrix.getResolution());
		short objectYSizeWithQuietZone = (short)(elementSize * (dataMatrix.getNumberOfRows() + (2 * dataMatrix.getQuietZoneInElements())) * dataMatrix.getResolution());
		
		short xbc = (short)(dataMatrix.getQuietZoneInElements() * elementSize * dataMatrix.getResolution());
		short ybc = (short)(dataMatrix.getQuietZoneInElements() * elementSize * dataMatrix.getResolution());
		
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();		
				
		// BBC structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.BEGIN, CategoryCode.BAR_CODE).build().bytes());
		
		// if there is a description in the barcode, it will be placed here
		String description = dataMatrix.getDescription();
		if (description != null && description.length() > 0) {
			Comment comment = new Comment(description);
			sfBytes.write(CommentHelper.format(comment).bytes());
		}
		
		// BOG structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.BEGIN, CategoryCode.OBJECT_ENVIRONMENT_GROUP).build().bytes());
		
		// OBD structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DESCRIPTOR, CategoryCode.OBJECT_AREA)
				.createTripletBuilder(0x43) // descriptor position triplet id
					.addTripletData(0x01) // object area position id
					.addTriplet()
				.createTripletBuilder(0x4B)
					.addTripletData(0x00) // presentation space unit base for X - 10 inches
					.addTripletData(0x00) // presentation space unit base for Y - 10 inches
					.addTripletData(ByteUtils.fromShort(presSpacePerBaseUnit))
					.addTripletData(ByteUtils.fromShort(presSpacePerBaseUnit))
					.addTriplet()
				.createTripletBuilder(0x4C) //object area size triplet id
					.addTripletData(0x02) // object area size type
					.addTripletData(0x00) // object area extend for x axis - 3 bytes, this line is leading zero
					.addTripletData(ByteUtils.fromShort(objectXSizeWithQuietZone))
					.addTripletData(0x00) // object area extend for y axis - 3 bytes, this line is leading zero
					.addTripletData(ByteUtils.fromShort(objectYSizeWithQuietZone))
					.addTriplet()
				.createTripletBuilder(0x70) // presentation space reset mixing triplet
					.addTripletData(0x01) // mixing flag
					.addTriplet()
				.createTripletBuilder(0x4E) // color specification
					.addTripletData(0x00) // reserved
					.addTripletData(0x40) // oca color
					.addTripletData(new byte[] {0x00, 0x00, 0x00, 0x00}) // reserved
					.addTripletData(0x10) // default for oca color
					.addTripletData(0x00) // default for oca color
					.addTripletData(0x00) // default for oca color
					.addTripletData(0x00) // default for oca color
					.addTripletData(new byte[] {(byte)0xFF, 0x08}) // color in oca for medium
					.addTriplet()
				.build().bytes());
		
		// OBP structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.POSITION, CategoryCode.OBJECT_AREA)
				.addData(0x01) // object area position id
				.addData(0x17) // total length of repeating group
				.addData(0x00)
				.addData(ByteUtils.fromShort((short) (pageDpi * dataMatrix.getLocation().getX()))) // x-axis origin of the object area (AreaPosition)
				.addData(0x00)
				.addData(ByteUtils.fromShort((short) (pageDpi * dataMatrix.getLocation().getY()))) // y-axis origin of the object area (AreaPosition)
				.addData(new byte[] {0x00, 0x00}) // object area's x-axis rotation from the x-axis (AreaXYrotation)
				.addData(new byte[] {0x2d, 0x00}) // object area's y-axis rotation from the y-axis (AreaXYrotation)
				.addData(0x00) // reserved
				.addData(new byte[] {0x00, 0x00, 0x00}) // x-axis origin for object content (ContentPosition)
				.addData(new byte[] {0x00, 0x00, 0x00}) // y-axis origin for object content (ContentPosition)
				.addData(new byte[] {0x00, 0x00}) // object content x-axis rotation from x-axis (ContentXYrotation)
				.addData(new byte[] {0x2d, 0x00}) // object content y-axis rotation from y-axis (ContentXYrotation)
				.addData(0x01) // reference coordinate system - Page or overlay defined by IPS
				.build().bytes());
		
		// MBC structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.MAP, CategoryCode.BAR_CODE)
				.addData(ByteUtils.fromShort((short) 5)) // length of repeating group including self
				.createTripletBuilder(0x04) // triplet id - mapping option
					.addTripletData(0x00) // value - position
					.addTriplet()
				.build().bytes());	
				
		// BDD structured field		
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DESCRIPTOR, CategoryCode.BAR_CODE)
				.addData(0x00) // 10 inches
				.addData(0x00) // reserved
				.addData(ByteUtils.fromShort(presSpacePerBaseUnit))
				.addData(ByteUtils.fromShort(presSpacePerBaseUnit))
				.addData(ByteUtils.fromShort(objectXSizeWithQuietZone))
				.addData(ByteUtils.fromShort(objectYSizeWithQuietZone))
				.addData(0x00) // reserved
				.addData(0x00) // reserved
				.addData(0x1C) // data matrix type
				.addData(0x00) // modifier value
				.addData(0xff) // font for hri (not used - just use default)
				.addData(ByteUtils.fromShort((short) 0)) // color: device default
				.addData(ByteUtils.toUnsignedByte((byte) dataMatrix.getElementSizeInMils()))
				.addData(ByteUtils.fromShort((short) 65535)) // element height - use default (ffff = 65535)
				.addData(0x01) // height multiplier
				.addData(ByteUtils.fromShort((short) 65535)) // wide-to-narrow ratio - use default (ffff = 65535)
				.build().bytes());
		
		// EOG structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.END, CategoryCode.OBJECT_ENVIRONMENT_GROUP).build().bytes());
		
		//BDA structured field
		byte[] payloadBytes = ByteUtils.getDataEncoding().byteValue(dataMatrix.getPayload());
		BigEndianBitSet ctrlFlag = new BigEndianBitSet();
		ctrlFlag.set(0, dataMatrix.isEbcdic2Ascii());
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DATA, CategoryCode.BAR_CODE)	
			.addData(0x00) // special flags for BSA
			.addData(ByteUtils.fromShort(xbc))
			.addData(ByteUtils.fromShort(ybc))
			.addData(ctrlFlag.toByte())
			.addData(ByteUtils.fromShort((short) dataMatrix.getNumberOfColumns()))
			.addData(ByteUtils.fromShort((short) dataMatrix.getNumberOfRows()))
			.addData(0x00) // structured append sequence indicator (not using this)
			.addData(0x00) // total symbols in structured append (not using this)
			.addData(0xfe) // high-order byte of structured append
			.addData(0xfe) // low-order byte of structured append
			.addData(0x00) // reserved (special-functions we don't use)
			.addData(payloadBytes)
			.build().bytes());
		
		// EBC structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.END, CategoryCode.BAR_CODE).build().bytes());
		
		return sfBytes.toByteArray();
	}
	
	public static boolean isBBC(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.BAR_CODE && sf.getTypeCode() == TypeCode.BEGIN;
	}
	
	public static boolean isEBC(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.BAR_CODE && sf.getTypeCode() == TypeCode.END;
	}

}
