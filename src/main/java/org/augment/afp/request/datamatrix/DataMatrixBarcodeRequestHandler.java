package org.augment.afp.request.datamatrix;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.request.ActionRequest;
import org.augment.afp.request.RequestHandler;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class DataMatrixBarcodeRequestHandler implements RequestHandler {

	@Override
	public String getName() {
		return DataMatrixBarcodeRequestHandler.class.getSimpleName();
	}

	@Override
	public boolean canHandle(final ActionRequest request) {
		return request instanceof DataMatrixBarcodeRequest;
	}

	@Override
	public byte[] handle(final ActionRequest request, final byte[] data) throws IOException {
		byte[] returnBytes = null;
		if (request instanceof InsertDataMatrixBarcodeRequest) {
			returnBytes = handleInsertDataMatrixBarcodeRequest((InsertDataMatrixBarcodeRequest) request, data);
		} else if (request instanceof RemoveDataMatrixBarcodeRequest) {
			returnBytes = removeDataMatrixBarcodeRequest((RemoveDataMatrixBarcodeRequest) request, data);
		} else if (request instanceof FindDataMatrixBarcodeRequest) {
			handleFindDataMatrixBarcodeRequest((FindDataMatrixBarcodeRequest) request, data);
			returnBytes = data;		
		} else {
			throw new IllegalArgumentException("No handler defined for request provided: " + request.getClass().getSimpleName());
		}
		
		return returnBytes;
	}
	
	private SizeDescriptor getPageDescriptor(DataInputStream dis) throws IOException {
		dis.mark(0);
		SizeDescriptor sizeDes = null;
		try {
			StructuredField sf = null;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (SizeDescriptor.isPageDescriptor(sf)) {
					sizeDes = SizeDescriptor.parse(sf);
					break;
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		dis.reset();
		
		return sizeDes;
	}
	
	private byte[] handleInsertDataMatrixBarcodeRequest(final InsertDataMatrixBarcodeRequest request, final byte[] data) throws IOException {
		boolean success = false;
				
		StructuredField sf = null;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		SizeDescriptor pageSize = getPageDescriptor(dis);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.PAGE_GROUP) {
					throw new IllegalArgumentException("Cannot add data matrix barcodes to page groups.");
				} else if (sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.PAGE) {
					out.write(DataMatrixBarcodeHelper.format(request.getValue(), pageSize.getXDpi().getValue()));
					success = true;
				}				
				
				out.write(sf.bytes());
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(success);
		
		return out.toByteArray();
	}
	
	private byte[] removeDataMatrixBarcodeRequest(final RemoveDataMatrixBarcodeRequest request, final byte[] data) throws IOException {
		byte[] returnData = data;
		
				
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		SizeDescriptor pageSize = getPageDescriptor(dis);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StructuredField sf = null;
		ByteArrayOutputStream barcode = new ByteArrayOutputStream();
		boolean collectSF = false;
		boolean found = false;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (DataMatrixBarcodeHelper.isBBC(sf)) {
					barcode = new ByteArrayOutputStream();
					collectSF = true;
					barcode.write(sf.bytes());
				} else if (collectSF) {
					barcode.write(sf.bytes());
					
					if (sf.getCategoryCode() == CategoryCode.BAR_CODE && sf.getTypeCode() == TypeCode.DESCRIPTOR && sf.getData()[12] != 0x1C) {
						// we need to check the BDD structured field for 1C in the offset 12 position
						// if not 1C, its not a data matrix barcode and should be skipped
						collectSF = false;
						// write what we collected to out
						out.write(barcode.toByteArray());
					} else if (DataMatrixBarcodeHelper.isEBC(sf)) {
						collectSF = false;
						
						DataMatrixBarcode theBarcode = DataMatrixBarcodeHelper.parse(barcode.toByteArray(), pageSize.getXDpi().getValue());
						if (theBarcode.getPayload() != null && theBarcode.getPayload().matches(request.getPayloadSearch()) && theBarcode.getDescription().matches(request.getDescriptionSearch())) {
							// do nothing, which effectively removes
							found = true;
						} else {
							// this is not what we are looking for, write it out
							out.write(barcode.toByteArray());
						}
					}
				} else {
					out.write(sf.bytes());
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}

		request.setSuccess(found);		
		if (found) {
			returnData = out.toByteArray();
		}
		
		return returnData;

	}
	
	private void handleFindDataMatrixBarcodeRequest(final FindDataMatrixBarcodeRequest request, final byte[] data) throws IOException {
		List<DataMatrixBarcode> result = new ArrayList<>();
		
		// we need to iterate over the structured fields and find the barcode sections that are data matrix.
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		SizeDescriptor pageSize = getPageDescriptor(dis);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		boolean collectSF = false;
		StructuredField sf = null;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (DataMatrixBarcodeHelper.isBBC(sf)) {
					out = new ByteArrayOutputStream();
					collectSF = true;
					out.write(sf.bytes());
				} else if (collectSF) {
					out.write(sf.bytes());
					
					if (sf.getCategoryCode() == CategoryCode.BAR_CODE && sf.getTypeCode() == TypeCode.DESCRIPTOR && sf.getData()[12] != 0x1C) {
						// we need to check the BDD structured field for 1C in the offset 12 position
						// if not 1C, its not a data matrix barcode and should be skipped
						collectSF = false;
					} else if (DataMatrixBarcodeHelper.isEBC(sf)) {
						collectSF = false;
						
						DataMatrixBarcode barcode = DataMatrixBarcodeHelper.parse(out.toByteArray(), pageSize.getXDpi().getValue());
						if (barcode.getPayload() != null && barcode.getPayload().matches(request.getPayloadSearch()) && barcode.getDescription().matches(request.getDescriptionSearch())) {
							result.add(barcode);
						}
					}
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
				
		request.setResult(result);
	}

}
