package org.augment.afp.request.datamatrix;

public class InsertDataMatrixBarcodeRequest implements DataMatrixBarcodeRequest {

	private final DataMatrixBarcode barcode;
	private boolean success = false;

	public InsertDataMatrixBarcodeRequest(final DataMatrixBarcode barcode) {
		this.barcode = barcode;
	}
	
	public DataMatrixBarcode getValue() {
		return barcode;
	}
	
	public void setSuccess(final boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}

}
