package org.augment.afp.request;

import java.io.IOException;

/**
 * RequestHandler provides the interface for defining a handler for augmentation requests.  The handler 
 * should be able to determine if it supports the request prior to handling it.
 *
 */
public interface RequestHandler {

	/**
	 * Name of the request handler for use by the handler loader.
	 * 
	 * @return request handler name
	 */
	String getName();
	
	/**
	 * Determines if this handler can handle the provided request.
	 * 
	 * @param request request to be handled
	 * @return true if this handler can handle it, false otherwise
	 */
	boolean canHandle(ActionRequest request);
	
	/**
	 * Handles the action request and acts on the provided data.  The augmented data is then returned.
	 * 
	 * @param request action request to be handled
	 * @param data data to augment
	 * @return augmented data
	 * @throws IOException if an io exception occurs
	 */
	byte[] handle(ActionRequest request, byte[] data) throws IOException;
}
