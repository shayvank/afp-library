package org.augment.afp.request.tle;

import java.io.IOException;
import java.util.List;

import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldBuilder;
import org.augment.afp.util.Triplet;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.Validations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaggedLogicalElementHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TaggedLogicalElementHelper.class);
	
	private TaggedLogicalElementHelper() {
		
	}

	public static TaggedLogicalElement parse(final StructuredField sf) {
		Validations.notNull(sf, "StructuredField object must not be null.");
		
		TaggedLogicalElement tle = null;			
		
		byte[] data = sf.getData();
		String name = null;
		String value = null;
		
		List<Triplet> triplets = Triplet.parse(data, 0, data.length);		
		for (Triplet triplet: triplets) {
			if (triplet.getCode() == 0x02) {
				
				if (triplet.getContents()[0] != 0x0b) {
					throw new IllegalArgumentException("FQNType must be X'0B' - Attribute Name - for TLE.  Found " + ByteUtils.toModcaDocumentHex(triplet.getContents()[0]));
				}
				
				if (triplet.getContents()[1] != 0x00) {
					throw new IllegalArgumentException("FQNFmt must be X'00' - Character String - for TLE.  Found " + ByteUtils.toModcaDocumentHex(triplet.getContents()[1]));
				}	
				
				byte[] nameBytes = ByteUtils.arraycopy(triplet.getContents(), 2, triplet.getLength() - 4);
				name = ByteUtils.getDataEncoding().stringValue(nameBytes);
			} else if (triplet.getCode() == 0x36) {
				// offset 2-3 are always zero
				byte[] valueBytes = ByteUtils.arraycopy(triplet.getContents(), 2, triplet.getLength() - 4);
				value = ByteUtils.getDataEncoding().stringValue(valueBytes);	
			} else {
				LOGGER.warn("Ignored triplet: {}", ByteUtils.toModcaDocumentHex((byte)triplet.getCode()));
			}
		}				
		
		if (name != null) {
			tle = new TaggedLogicalElement(name, value); 
		}
		
		return tle;
	}
	
	public static StructuredField format(final TaggedLogicalElement tle) throws IOException {
		Validations.notNull(tle, "TLE object must not be null.");
		
		String name = tle.getQualifiedName();
		String value = tle.getAttrValue();
		
		Validations.notEmpty(name, "Tagged Logical Elements must have a qualified name.");
				
		if (value != null && value.length() > 250) {
			throw new IllegalArgumentException("TLE value cannot be longer than 250 characters.");
		}
		
		if (value == null) {
			value = "";
		}
		
		byte[] nameBytes = ByteUtils.getDataEncoding().byteValue(name);
		
		return StructuredFieldBuilder.createBuilder(TypeCode.ATTRIBUTE, CategoryCode.PROCESS_ELEMENT)
				.createTripletBuilder(0x02) // code: fully qualified name triplet code
					.addTripletData(0x0b) // fqn: Attribute GID
					.addTripletData(0x00) // fqnFmt: Character String
					.addTripletData(nameBytes)
					.addTriplet()
				.createTripletBuilder(0x36) // code: attribute value triplet
					.addTripletData(0x00) // reserved
					.addTripletData(0x00) // reserved
					.addTripletData(ByteUtils.getDataEncoding().byteValue(value))
					.addTriplet()
				.build();
	}
	
	public static boolean isTaggedLogicalElement(final StructuredField sf) {
		return sf.getTypeCode() == TypeCode.ATTRIBUTE && sf.getCategoryCode() == CategoryCode.PROCESS_ELEMENT;
	}
}
