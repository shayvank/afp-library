package org.augment.afp.request.tle;

public class InsertTleRequest implements TleRequest {

	private final TaggedLogicalElement tle;
	private boolean success = false;
	
	public InsertTleRequest(final TaggedLogicalElement tle) {
		this.tle = tle;
	}
	
	public TaggedLogicalElement getValue() {
		return tle;
	}
	
	public void setSuccess(final boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}
}

