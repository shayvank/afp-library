package org.augment.afp.request.tle;

public class RemoveTleRequest implements TleRequest {

	private String nameSearch;
	
	private boolean success = false;
	
	private RemoveTleRequest(final String name) {
		this.nameSearch = name;
	}
	
	public String getNameSearch() {
		return nameSearch;
	}	
	
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}
	
	public static RemoveTleRequest removeByName(final String matcher) {
		return new RemoveTleRequest(matcher);
	}
}
