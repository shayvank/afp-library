package org.augment.afp.request.tle;

import java.util.List;

public class FindTleRequest implements TleRequest {

	private String nameSearch;
	private String valueSearch;
	
	private List<TaggedLogicalElement> result;
	
	private FindTleRequest(final String nameSearch, final String valueSearch) {
		this.nameSearch = nameSearch;
		this.valueSearch = valueSearch;
	}
	
	public String getNameSearch() {
		return nameSearch;
	}
	
	public String getValueSearch() {
		return valueSearch;
	}
	
	public List<TaggedLogicalElement> getResult() {
		return result;
	}
	
	public void setResult(final List<TaggedLogicalElement> result) {
		this.result = result;
	}
	
	@Override
	public boolean isUpdate() {
		return false;
	}
	
	public static FindTleRequest findByName(final String matcher) {
		return new FindTleRequest(matcher, ".*");
	}
	
	public static FindTleRequest findByValue(final String matcher) {
		return new FindTleRequest(".*", matcher);
	}
	
}
