package org.augment.afp.request.tle;

public class TaggedLogicalElement {

	private String qualifiedName;
	private String attrValue;
		
	public TaggedLogicalElement(final String qualifiedName, final String attrValue) {
		this.qualifiedName = qualifiedName;
		this.attrValue = attrValue;
	}
	
	public String getQualifiedName() {
		return qualifiedName;
	}
	
	public String getAttrValue() {
		return attrValue;
	}
}
