package org.augment.afp.request.tle;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.augment.afp.request.ActionRequest;
import org.augment.afp.request.RequestHandler;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class TleRequestHandler implements RequestHandler {

	@Override
	public String getName() {
		return TleRequestHandler.class.getSimpleName();
	}

	@Override
	public boolean canHandle(final ActionRequest request) {
		return request instanceof TleRequest;
	}

	@Override
	public byte[] handle(final ActionRequest request, final byte[] data) throws IOException {
		byte[] returnBytes = null;
		if (request instanceof InsertTleRequest) {
			returnBytes = handleInsertTleRequest((InsertTleRequest) request, data);
		} else if (request instanceof RemoveTleRequest) {
			returnBytes = handleRemoveTleRequest((RemoveTleRequest) request, data);
		} else if (request instanceof FindTleRequest) {
			handleFindTleRequest((FindTleRequest) request, data);
			returnBytes = data;
		} else {
			throw new IllegalArgumentException("No handler defined for request provided :" + request.getClass().getSimpleName());
		}
		
		return returnBytes;
	}

	private byte[] handleInsertTleRequest(final InsertTleRequest request, final byte[] data) throws IOException {
		// for the pagegroup level, we can add it anywhere after the BNG item.
		boolean success = false;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));				
		
		StructuredField sf = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				out.write(sf.bytes());
				if (!success) {
					if (sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.PAGE_GROUP) {
							out.write(TaggedLogicalElementHelper.format(request.getValue()).bytes());
							success = true;
					} else if (sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.PAGE) {
						sf = StructuredField.getNext(dis);
						out.write(sf.bytes());
						if (sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.ACTIVE_ENVIRONMENT_GROUP) {
							// read to end of the active environment group
							while ((sf = StructuredField.getNext(dis)) != null) {
								out.write(sf.bytes());
								if (sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.ACTIVE_ENVIRONMENT_GROUP) {
									out.write(TaggedLogicalElementHelper.format(request.getValue()).bytes());
									success = true;
								}
							}
						}
					}
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(success);
		
		return out.toByteArray();
	}
	
	private byte[] handleRemoveTleRequest(final RemoveTleRequest request, final byte[] data) throws IOException {
		byte[] returnData = data;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		StructuredField sf = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		boolean found = false;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (TaggedLogicalElementHelper.isTaggedLogicalElement(sf)) {
					TaggedLogicalElement tle = TaggedLogicalElementHelper.parse(sf);
					
					if (tle.getQualifiedName().matches(request.getNameSearch())) {
						found = true;						
					} else {
						out.write(sf.bytes());
					}
				} else {
					out.write(sf.bytes());
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(found);		
		if (found) {
			returnData = out.toByteArray();
		}
		
		return returnData;
	}
	
	private void handleFindTleRequest(final FindTleRequest request, final byte[] data) throws IOException {
		// we need to iterate over the structured fields and find the tle fields.
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
				
		StructuredField sf = null;
		
		List<TaggedLogicalElement> tles = new ArrayList<>();
		
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (TaggedLogicalElementHelper.isTaggedLogicalElement(sf)) {
					// TLE!
					TaggedLogicalElement tle = TaggedLogicalElementHelper.parse(sf);
					
					if (tle.getQualifiedName().matches(request.getNameSearch()) && tle.getAttrValue().matches(request.getValueSearch())) {
						tles.add(tle);						
					}
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setResult(tles);
	}

}
