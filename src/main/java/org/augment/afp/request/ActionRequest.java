package org.augment.afp.request;

/**
 * Provides the base action request for augmenting afp.
 *
 */
public interface ActionRequest {

	/**
	 * Determines if this action request updates the AFP or is just a query.
	 * 
	 * @return true if update, false if query
	 */
	boolean isUpdate();
}
