package org.augment.afp.request.comment;

import org.augment.afp.request.ActionRequest;

/**
 * CommentRequest is the base interface for comment actions to the comment request handler.
 *
 */
public interface CommentRequest extends ActionRequest {

}
