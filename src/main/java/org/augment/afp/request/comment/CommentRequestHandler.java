package org.augment.afp.request.comment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.augment.afp.request.ActionRequest;
import org.augment.afp.request.RequestHandler;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class CommentRequestHandler implements RequestHandler {

	@Override
	public String getName() {
		return CommentRequestHandler.class.getSimpleName();
	}
	
	@Override
	public boolean canHandle(final ActionRequest request) {
		return request instanceof CommentRequest;
	}
	
	@Override
	public byte[] handle(final ActionRequest request, final byte[] data) throws IOException {
		byte[] returnBytes = null;
		if (request instanceof InsertCommentRequest) {
			returnBytes = handleInsertCommentRequest((InsertCommentRequest) request, data);
		} else if (request instanceof RemoveCommentRequest) {
			returnBytes = handleRemoveCommentRequest((RemoveCommentRequest) request, data);
		} else if (request instanceof FindCommentRequest) {
			handleFindCommentRequest((FindCommentRequest) request, data);
			returnBytes = data;
		} else {
			throw new IllegalArgumentException("No handler defined for request provided :" + request.getClass().getSimpleName());
		}
		
		return returnBytes;
	}

	private byte[] handleInsertCommentRequest(final InsertCommentRequest request, final byte[] data) throws IOException {
		// for the pagegroup level, we can add it anywhere after the BNG item.
		boolean success = false;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));				
		
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			// we will first, read the next structured field, which will be assumed to be a container.
			StructuredField sf = StructuredField.getNext(dis);
			if (sf.getTypeCode() != TypeCode.BEGIN) {
				throw new IllegalArgumentException("Expected a BEGIN Structured Field type.");
			}
			
			out.write(sf.bytes());
			out.write(CommentHelper.format(request.getValue()).bytes());
			success = true;			
			
			while ((sf = StructuredField.getNext(dis)) != null) {
				out.write(sf.bytes());
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(success);
		
		return out.toByteArray();
	}
	
	private byte[] handleRemoveCommentRequest(final RemoveCommentRequest request, final byte[] data) throws IOException {
		byte[] returnData = data;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		StructuredField sf = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		boolean found = false;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (Comment.is(sf)) {
					Comment comment = CommentHelper.parse(sf);
					
					if (comment.getValue() != null && comment.getValue().matches(request.getValueSearch())) {
						found = true;						
					} else {
						out.write(sf.bytes());
					}
				} else {
					out.write(sf.bytes());
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(found);		
		if (found) {
			returnData = out.toByteArray();
		}
		
		return returnData;
	}
	
	private void handleFindCommentRequest(final FindCommentRequest request, final byte[] data) throws IOException {
		// we need to iterate over the structured fields and find the comment fields.
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
				
		StructuredField sf = null;
		
		List<Comment> comments = new ArrayList<>();
		
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (Comment.is(sf)) {
					// comment
					Comment comment = CommentHelper.parse(sf);
					
					if (comment.getValue().matches(request.getValueSearch())) {
						comments.add(comment);						
					}
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setResult(comments);
	}
	
}
