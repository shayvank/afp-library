package org.augment.afp.request.comment;

/**
 * Request to insert a comment.
 *
 */
public class InsertCommentRequest implements CommentRequest {

	private final Comment comment;
	private boolean success = false;
	
	/**
	 * Creates a request to insert a comment.
	 * 
	 * @param comment comment to insert
	 */
	public InsertCommentRequest(final Comment comment) {
		this.comment = comment;
	}
	
	/**
	 * Returns the comment for the request
	 * 
	 * @return comment
	 */
	public Comment getValue() {
		return comment;
	}
	
	/**
	 * Sets the success (by the handler).
	 * 
	 * @param success true if performed, false otherwise
	 */
	void setSuccess(final boolean success) {
		this.success = success;
	}
	
	/**
	 * Returns the success flag of the request.
	 * 
	 * @return true if performed, false otherwise
	 */
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}
	
}

