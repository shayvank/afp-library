package org.augment.afp.request.comment;

import java.util.List;

/**
 * Request to find a comment.
 *
 */
public class FindCommentRequest implements CommentRequest {

	private String valueSearch;
	
	private List<Comment> result;
	
	private FindCommentRequest(final String valueSearch) {
		this.valueSearch = valueSearch;
	}
	
	/**
	 * Returns the value search payload.
	 * 
	 * @return value search
	 */
	public String getValueSearch() {
		return valueSearch;
	}
	
	/**
	 * Returns the list of comments that matched the search.
	 * 
	 * @return list of matching comments
	 */
	public List<Comment> getResult() {
		return result;
	}
	
	/**
	 * Sets the list of comments that matched the search.
	 * 
	 * @param result list of matching comments
	 */
	void setResult(final List<Comment> result) {
		this.result = result;
	}
	
	@Override
	public boolean isUpdate() {
		return false;
	}
	
	/**
	 * Creates a request to find a comment by value.
	 * 
	 * @param matcher regex value
	 * @return instance of FindCommentRequest for given matcher
	 */
	public static FindCommentRequest findByValue(final String matcher) {
		return new FindCommentRequest(matcher);
	}		
}
