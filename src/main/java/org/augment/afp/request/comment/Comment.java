package org.augment.afp.request.comment;

import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

/**
 * Represents a Comment (No Operation in MO:DCA terms) embedded in the data stream. 
 *
 */
public class Comment {

	private String value;
	
	/**
	 * Creates a new comment given the value.
	 * 
	 * @param value comment value
	 */
	public Comment(final String value) {
		this.value = value;
	}
	
	/**
	 * Returns the comment value.
	 * 
	 * @return comment value
	 */
	public String getValue() {
		return value;
	}
	
	public static boolean is(final StructuredField sf) {
		return sf.getTypeCode() == TypeCode.DATA && sf.getCategoryCode() == CategoryCode.NO_OPERATION;
	}
}
