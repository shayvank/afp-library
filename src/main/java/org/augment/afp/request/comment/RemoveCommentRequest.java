package org.augment.afp.request.comment;

/**
 * Request to remove a comment.  Note, the value must be the exact equal 
 * value of the comment in order to be removed.
 *
 */
public class RemoveCommentRequest implements CommentRequest {

	private String valueSearch;
	
	private boolean success = false;

	/**
	 * Create a request to remove a comment.
	 * 
	 * @param valueSearch search value
	 */
	public RemoveCommentRequest(final String valueSearch) {
		this.valueSearch = valueSearch;
	}
	
	/**
	 * Returns the comment value.
	 * 
	 * @return comment value
	 */
	public String getValueSearch() {
		return valueSearch;
	}	
	
	/**
	 * Sets the success (by the handler).
	 * 
	 * @param success true if performed, false otherwise
	 */
	void setSuccess(boolean success) {
		this.success = success;
	}
	
	/**
	 * Returns the success flag of the request.
	 * 
	 * @return true if performed, false otherwise
	 */
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}
}
