package org.augment.afp.request.comment;

import java.io.IOException;

import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldBuilder;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.Validations;

public class CommentHelper {
	
	private CommentHelper() {
		
	}
	
	public static Comment parse(final StructuredField sf) {
		Validations.notNull(sf, "StructuredField object must not be null.");
				
		return new Comment(ByteUtils.getDataEncoding().stringValue(sf.getData()));
	}
	
	public static StructuredField format(final Comment comment) throws IOException {
		Validations.notNull(comment, "Comment object must not be null.");
		
		String value = comment.getValue();
		
		Validations.notNull(value, "This api doesn't support creating Comments with null values.");
		
		if (value.length() > 32759) {
			throw new IllegalArgumentException("Comment value cannot be longer than 32759 characters.");
		}
		
		byte[] valueBytes = ByteUtils.getDataEncoding().byteValue(value);
		
		// structuredfield
		return StructuredFieldBuilder.createBuilder(TypeCode.DATA, CategoryCode.NO_OPERATION)
			.addData(valueBytes).build();
	}
	
}
