package org.augment.afp.request;

import java.util.HashMap;
import java.util.Map;

import org.augment.afp.request.blockout.BlockoutRequestHandler;
import org.augment.afp.request.comment.CommentRequestHandler;
import org.augment.afp.request.datamatrix.DataMatrixBarcodeRequestHandler;
import org.augment.afp.request.imageoverlay.ImageOverlayRequestHandler;
import org.augment.afp.request.textoverlay.TextOverlayRequestHandler;
import org.augment.afp.request.tle.TleRequestHandler;

/**
 * A loader for defined request handlers.
 *
 */
public final class RequestHandlerLoader {
	
	/**
	 * Static class.
	 */
	private RequestHandlerLoader() {
		// private constructor, static class
	}

	private static final Map<String, RequestHandler> _HANDLERS = new HashMap<>();
	
	static {
		RequestHandlerLoader.register(new CommentRequestHandler());
		RequestHandlerLoader.register(new TleRequestHandler());
		RequestHandlerLoader.register(new DataMatrixBarcodeRequestHandler());
		RequestHandlerLoader.register(new TextOverlayRequestHandler());
		RequestHandlerLoader.register(new BlockoutRequestHandler());
		RequestHandlerLoader.register(new ImageOverlayRequestHandler());
	}
	
	/**
	 * Registers the handler with the loader.
	 * 
	 * @param handler request handler to register
	 */
	public static void register(final RequestHandler handler) {
		_HANDLERS.put(handler.getName(), handler);
	}
	
	/**
	 * Finds a handler based on an action request.
	 * 
	 * @param request request to be handled
	 * @return request handler that can handle the request or null if not found
	 */
	public static RequestHandler findHandler(final ActionRequest request) {
		RequestHandler handler = null;
		
		for (RequestHandler rhandler: _HANDLERS.values()) {
			if (rhandler.canHandle(request)) {
				handler = rhandler;
				break;
			}
		}
		
		return handler;
	}
}
