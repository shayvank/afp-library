package org.augment.afp.request.imageoverlay;

public class InsertImageOverlayRequest implements ImageOverlayRequest {

	private final ImageOverlay overlay;
	private boolean success = false;
	
	public InsertImageOverlayRequest(final ImageOverlay overlay) {
		this.overlay = overlay;
	}
	
	public ImageOverlay getValue() {
		return overlay;
	}
	
	public void setSuccess(final boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}

}
