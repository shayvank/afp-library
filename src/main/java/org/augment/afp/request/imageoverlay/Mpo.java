package org.augment.afp.request.imageoverlay;

public class Mpo {

	private String name;
	private int lid;
	
	public Mpo(final String name, final int lid) {
		this.name = name;
		this.lid = lid;
	}
	
	public String getName() {
		return name;
	}
	
	public int getLid() {
		return lid;
	}
}
