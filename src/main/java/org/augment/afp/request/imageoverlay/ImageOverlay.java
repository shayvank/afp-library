package org.augment.afp.request.imageoverlay;

import org.augment.afp.util.Coordinate;

public class ImageOverlay {

	private Coordinate position;
	private int direction;
	private String name;
	
	private ImageOverlay(final String name, final Coordinate position, final int direction) {
		this.name = name;
		this.position = position;
		this.direction = direction;
	}
	
	public String getQualifiedName() {
		return name;
	}
	
	public Coordinate getPosition() {
		return position;
	}
	
	public int getDirection() {
		return direction;
	}
	
	public static class Builder {
		private String name;
		private Coordinate position;
		private int direction = 0;
		
		public Builder() {
			// we will set everything using methods below
		}
		
		public Builder withName(final String name) {
			this.name = name;
			return this;
		}
		
		public Builder withPosition(final Coordinate position) {
			this.position = position;
			return this;			
		}
		
		public Builder withDirection(final int direction) {
			if (direction != 0 && direction != 90 && direction != 180 && direction != 270) {
				throw new IllegalArgumentException("ImageOverlay direction must be either 0, 90, 180, or 270");
			}
			this.direction = direction;
			return this;
		}
		
		public ImageOverlay build() {
			return new ImageOverlay(name, position, direction);
		}
	}
}
