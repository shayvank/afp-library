package org.augment.afp.request.imageoverlay;

import java.util.List;

public class FindImageOverlayRequest implements ImageOverlayRequest {

	private String nameSearch;
	
	private List<ImageOverlay> result;
	
	private FindImageOverlayRequest(final String nameSearch) {
		this.nameSearch = nameSearch;
	}
	
	public String getNameSearch() {
		return nameSearch;
	}
	
	public List<ImageOverlay> getResult() {
		return result;
	}
	
	public void setResult(final List<ImageOverlay> result) {
		this.result = result;
	}
	
	@Override
	public boolean isUpdate() {
		return false;
	}
	
	public static FindImageOverlayRequest findByName(final String matcher) {
		return new FindImageOverlayRequest(matcher);
	}

}
