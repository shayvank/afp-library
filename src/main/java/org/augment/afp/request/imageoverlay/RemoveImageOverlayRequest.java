package org.augment.afp.request.imageoverlay;

public class RemoveImageOverlayRequest implements ImageOverlayRequest {

	private boolean success = false;
	
	private String nameSearch;
	
	public RemoveImageOverlayRequest(final String nameSearch) {
		this.nameSearch = nameSearch;
	}
		
	public String getNameSearch() {
		return nameSearch;
	}
		
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	@Override
	public boolean isUpdate() {
		return true;
	}
	
	public static RemoveImageOverlayRequest removeByName(final String matcher) {
		return new RemoveImageOverlayRequest(matcher);
	}

}
