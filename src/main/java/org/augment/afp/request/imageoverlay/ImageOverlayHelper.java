package org.augment.afp.request.imageoverlay;

import java.io.IOException;
import java.util.List;

import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.Coordinate;
import org.augment.afp.util.Orientation;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldBuilder;
import org.augment.afp.util.Triplet;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.Validations;

public class ImageOverlayHelper {
	
	private ImageOverlayHelper() {
		// static class
	}
	
	public static ImageOverlay parseIPO(final StructuredField sf, final SizeDescriptor size) {
		ImageOverlay.Builder builder = new ImageOverlay.Builder();
		byte[] data = sf.getData();
		
		
		byte[] nameBytes = ByteUtils.arraycopy(data, 0, 8);
		builder.withName(ByteUtils.getDataEncoding().stringValue(nameBytes));
		
		int xAxis = ByteUtils.toInt(ByteUtils.arraycopy(data, 8, 3));
		
		int yAxis = ByteUtils.toInt(ByteUtils.arraycopy(data, 11, 3));
		
		builder.withPosition(new Coordinate((xAxis * 1.0 / size.getXDpi().getValue()), (yAxis * 1.0 / size.getYDpi().getValue())));
		
		Orientation orientation = Orientation.findByValue(ByteUtils.toUnsignedShort(ByteUtils.arraycopy(data, 14, 2)));
		builder.withDirection(orientation.getIntValue());
		
		return builder.build();
	}
	
	public static StructuredField formatIPO(final ImageOverlay overlay, final int pageDpi) throws IOException {
		Validations.notNull(overlay, "Image Overlay object must not be null.");

		String name = overlay.getQualifiedName();
		Validations.notNull(name, "Image Overlay requires a Qualified Name");
		
		String namePad = name + "        ";
		name = namePad.substring(0,8);
		byte[] nameBytes = ByteUtils.getDataEncoding().byteValue(name);
		
		return StructuredFieldBuilder.createBuilder(TypeCode.INCLUDE, CategoryCode.PAGE_OVERLAY)
				.addData(nameBytes) // overlay name
				.addData(ByteUtils.from3ByteNumber((short) (pageDpi * overlay.getPosition().getX()))) // X-axis origin for the page overlay
				.addData(ByteUtils.from3ByteNumber((short) (pageDpi * overlay.getPosition().getY()))) // Y-axis origin for the page overlay
				.addData(Orientation.findByInt(overlay.getDirection()).getByteValue())
				.build();
	}
	
	public static Mpo parseMPO(final StructuredField sf) {
		String overlayName = null;
		int lid = 0;
		
		byte[] data = sf.getData();
		
		List<Triplet> triplets = Triplet.parse(data, 2, data.length - 2);
		for (Triplet triplet: triplets) {
			if (triplet.getCode() == 0x02) { // fully qualified name
				byte[] contents = triplet.getContents();
				overlayName = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(contents, 2, contents.length - 2));
			} else if (triplet.getCode() == 0x24) { // res lid
				lid = triplet.getContents()[1];
			}
		}
		
		return new Mpo(overlayName, lid);
	}
	
	public static StructuredField formatMPO(final Mpo mpo) throws IOException {	
		Validations.notNull(mpo, "MPO object must not be null.");
		
		String name = mpo.getName();
		Validations.notNull(name, "MPO requires a Name");
		
		String namePad = name + "        ";
		name = namePad.substring(0,8);		
		byte[] nameBytes = ByteUtils.getDataEncoding().byteValue(name);
		
		return StructuredFieldBuilder.createBuilder(TypeCode.MAP, CategoryCode.PAGE_OVERLAY)
				.addData(0x00).addData(0x12) // length of repeating group including self
				.createTripletBuilder(0x02) // code: fully qualified name triplet code
					.addTripletData(0x84) // fqn: begin resource object reference
					.addTripletData(0x00) // fqnFmt: Character String
					.addTripletData(nameBytes)
					.addTriplet()
				.createTripletBuilder(0x24) // code: resource local identifier
					.addTripletData(0x02) // restype: Page Overlay resource
					.addTripletData(mpo.getLid()) // reslid: unique resource object local id
					.addTriplet()
				.build();
	}
	
	public static boolean isMPO(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.PAGE_OVERLAY && sf.getTypeCode() == TypeCode.MAP;
	}
	
	public static boolean isIPO(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.PAGE_OVERLAY && sf.getTypeCode() == TypeCode.INCLUDE;
	}
}
