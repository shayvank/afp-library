package org.augment.afp.request.imageoverlay;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.request.ActionRequest;
import org.augment.afp.request.RequestHandler;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class ImageOverlayRequestHandler implements RequestHandler {

	@Override
	public String getName() {
		return ImageOverlayRequestHandler.class.getSimpleName();
	}

	@Override
	public boolean canHandle(final ActionRequest request) {
		return request instanceof ImageOverlayRequest;
	}

	@Override
	public byte[] handle(final ActionRequest request, byte[] data) throws IOException {
		byte[] returnBytes = null;
		
		if (request instanceof InsertImageOverlayRequest) {
			returnBytes = handleInsertImageOverlayRequest((InsertImageOverlayRequest) request, data);
		} else if (request instanceof RemoveImageOverlayRequest) {
			returnBytes = handleRemoveImageOverlayRequest((RemoveImageOverlayRequest) request, data);
		} else if (request instanceof FindImageOverlayRequest) {
			handleFindImageOverlayRequest((FindImageOverlayRequest) request, data);
		} else {
			throw new IllegalArgumentException("No handler defined for request provided: " + request.getClass().getSimpleName());
		}
		
		return returnBytes;
	}
	
	private SizeDescriptor getPageDescriptor(DataInputStream dis) throws IOException {
		dis.mark(0);
		SizeDescriptor sizeDes = null;
		try {
			StructuredField sf = null;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (SizeDescriptor.isPageDescriptor(sf)) {
					sizeDes = SizeDescriptor.parse(sf);
					break;
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		dis.reset();
		
		return sizeDes;
	}	

	private byte[] handleInsertImageOverlayRequest(final InsertImageOverlayRequest request, final byte[] data) throws IOException {
		boolean success = false;
		
		ImageOverlay imageOverlay = request.getValue();
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));

		StructuredField sf = null;
		
		SizeDescriptor size = getPageDescriptor(dis);
		
		int maxLid = 0;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (ImageOverlayHelper.isMPO(sf)) {
					Mpo mpo = ImageOverlayHelper.parseMPO(sf);
					if (maxLid < mpo.getLid()) {
						maxLid = mpo.getLid();
					}
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		dis = new DataInputStream(new ByteArrayInputStream(data));
		sf = null;
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.PAGE_GROUP) {
					throw new IllegalArgumentException("Cannot add image overlay to page groups.");
				} else if (sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.ACTIVE_ENVIRONMENT_GROUP) {
					// output the sf, then output the mpo
					out.write(sf.bytes());
					int lid = maxLid + 1;
					out.write(ImageOverlayHelper.formatMPO(new Mpo(imageOverlay.getQualifiedName(), lid)).bytes());
				} else if (sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.ACTIVE_ENVIRONMENT_GROUP) {
					// output the sf, then output the ipo
					out.write(sf.bytes());
					out.write(ImageOverlayHelper.formatIPO(imageOverlay, size.getXDpi().getValue()).bytes());
					success = true;
				} else {
					out.write(sf.bytes());
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(success);
		
		return out.toByteArray();
	}
	
	private byte[] handleRemoveImageOverlayRequest(final RemoveImageOverlayRequest request, final byte[] data) throws IOException {
		byte[] returnData = data;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		SizeDescriptor size = getPageDescriptor(dis);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StructuredField sf = null;
		boolean found = false;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (ImageOverlayHelper.isMPO(sf)) {
					Mpo mpo = ImageOverlayHelper.parseMPO(sf);
					if (mpo.getName().matches(request.getNameSearch())) {
						found = true;
					} else {
						out.write(sf.bytes());
					}
				} else if (ImageOverlayHelper.isIPO(sf)) {
					ImageOverlay overlay = ImageOverlayHelper.parseIPO(sf, size);
					if (overlay.getQualifiedName().matches(request.getNameSearch())) {
						found = true;
					} else {
						out.write(sf.bytes());
					}
				} else {
					out.write(sf.bytes());
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(found);
		if (found) {
			returnData = out.toByteArray();
		}
		
		return returnData;
	}
	
	
	private void handleFindImageOverlayRequest(final FindImageOverlayRequest request, final byte[] data) throws IOException {
		List<ImageOverlay> result = new ArrayList<>();
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		SizeDescriptor size = getPageDescriptor(dis);
		
		StructuredField sf;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (ImageOverlayHelper.isIPO(sf)) {
					ImageOverlay overlay = ImageOverlayHelper.parseIPO(sf, size);
					
					if (overlay.getQualifiedName().matches(request.getNameSearch())) {
						result.add(overlay);
					}
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setResult(result);
	}
}
