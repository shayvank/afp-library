package org.augment.afp.request.blockout;

import org.augment.afp.util.Coordinate;

public class Blockout {

	public enum Color {
		BLACK(8),
		WHITE(7),
		MEDIUM(65288);
		
		private int code;
		
		private Color(final int code) {
			this.code = code;
		}
		
		public int getCode() {
			return code;
		}
	}
	
	private int color;
	private Coordinate position;
	private String description;
	private double height;
	private double width;
	private boolean compressed;
	
	private Blockout(final int color, final Coordinate position, final double height, final double width, final String description, final boolean compressed) {
		this.color = color;
		this.position = position;
		this.description = description;
		this.height = height;
		this.width = width;
		this.compressed = compressed;
	}

	public int getColor() {
		return color;
	}
	
	public String getDescription() {
		return description;
	}
	
	public double getHeight() {
		return height;
	}
	
	public double getWidth() {
		return width;
	}
	
	public Coordinate getPosition() {
		return position;
	}
	
	public boolean isCompressed() {
		return compressed;
	}
	
	public static class Builder {
		private int color;
		private Coordinate position;
		private String description;
		private double height;
		private double width;
		private boolean compressed = true;
		
		public Builder() {
			// we will set everything using methods below
		}
		
		public Builder withColor(final int color) {
			this.color = color;
			return this;
		}
		
		public Builder withPosition(final Coordinate position) {
			this.position = position;
			return this;			
		}
		
		public Builder withDescription(final String description) {
			this.description = description;
			return this;
		}
		
		public Builder withSize(final double height, final double width) {
			this.height = height;
			this.width = width;
			return this;
		}
		
		public Builder withCompression(final boolean compression) {
			this.compressed = compression;
			return this;
		}
		
		public Blockout build() {
			return new Blockout(color, position, height, width, description, compressed);
		}
		
		
	}
}
