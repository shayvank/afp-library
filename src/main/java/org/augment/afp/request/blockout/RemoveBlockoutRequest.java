package org.augment.afp.request.blockout;

public class RemoveBlockoutRequest implements BlockoutRequest {

	private boolean success = false;
	
	private String descriptionSearch;
	
	public RemoveBlockoutRequest(final String descriptionSearch) {
		this.descriptionSearch = descriptionSearch;
	}
	
	public String getDescriptionSearch() {
		return descriptionSearch;
	}
	
	public void setSuccess(final boolean success) {
		this.success = success;
	}
	
	public boolean isSuccess() {
		return success;
	}

	@Override
	public boolean isUpdate() {
		return true;
	}
	
	public static RemoveBlockoutRequest removeByDescription(final String matcher) {
		return new RemoveBlockoutRequest(matcher);
	}

}
