package org.augment.afp.request.blockout;

public class InsertBlockoutRequest implements BlockoutRequest {

	private final Blockout blockout;
	private boolean success = false;
	
	public InsertBlockoutRequest(final Blockout blockout) {
		this.blockout = blockout;
	}	
	
	public Blockout getValue() {
		return blockout;
	}
	
	public void setSuccess(final boolean success) {
		this.success = success;	
	}
	
	public boolean isSuccess() {
		return success;
	}

	@Override
	public boolean isUpdate() {
		return true;
	}

}
