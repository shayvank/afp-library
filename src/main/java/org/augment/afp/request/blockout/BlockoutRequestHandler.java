package org.augment.afp.request.blockout;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.request.ActionRequest;
import org.augment.afp.request.RequestHandler;
import org.augment.afp.request.comment.Comment;
import org.augment.afp.request.comment.CommentHelper;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class BlockoutRequestHandler implements RequestHandler {

	@Override
	public String getName() {
		return BlockoutRequestHandler.class.getSimpleName();
	}

	@Override
	public boolean canHandle(final ActionRequest request) {
		return request instanceof BlockoutRequest;
	}

	@Override
	public byte[] handle(final ActionRequest request, final byte[] data) throws IOException {
		byte[] returnBytes = null;
		if (request instanceof InsertBlockoutRequest) {
			returnBytes = handleInsertBlockoutRequest((InsertBlockoutRequest) request, data);
		} else if (request instanceof RemoveBlockoutRequest) {
			returnBytes = removeBlockoutRequest((RemoveBlockoutRequest) request, data);
		} else if (request instanceof FindBlockoutRequest) {
			handleFindBlockoutRequest((FindBlockoutRequest) request, data);
			returnBytes = data;		
		} else {
			throw new IllegalArgumentException("No handler defined for request provided: " + request.getClass().getSimpleName());
		}
		
		return returnBytes;
	}
	
	private SizeDescriptor getPageDescriptor(DataInputStream dis) throws IOException {
		dis.mark(0);
		SizeDescriptor sizeDes = null;
		try {
			StructuredField sf = null;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (SizeDescriptor.isPageDescriptor(sf)) {
					sizeDes = SizeDescriptor.parse(sf);
					break;
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		dis.reset();
		
		return sizeDes;
	}
	
	private byte[] handleInsertBlockoutRequest(final InsertBlockoutRequest request, final byte[] data) throws IOException {
		boolean success = false;
				
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		SizeDescriptor pageSize = getPageDescriptor(dis);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			StructuredField sf = null;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.PAGE_GROUP) {
					throw new IllegalArgumentException("Cannot add block out to page groups.");
				} else if (sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.PAGE) {
					out.write(BlockoutHelper.format(request.getValue(), pageSize.getXSizeInUnits(), pageSize.getYSizeInUnits(), pageSize.getXDpi().getValue()));
					success = true;
				}
				
				out.write(sf.bytes());
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(success);
		
		return out.toByteArray();
	}
	
	private byte[] removeBlockoutRequest(final RemoveBlockoutRequest request, final byte[] data) throws IOException {
		byte[] returnData = data;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		SizeDescriptor pageSize = getPageDescriptor(dis);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StructuredField sf = null;
		ByteArrayOutputStream ioca = new ByteArrayOutputStream();
		boolean collectSF = false;
		boolean commentFound = false;
		boolean found = false;
		
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (BlockoutHelper.isBIM(sf)) {
					ioca = new ByteArrayOutputStream();
					collectSF = true;
					ioca.write(sf.bytes());
				} else if (collectSF) {
					ioca.write(sf.bytes());
					
					if (Comment.is(sf) && !commentFound) {
						Comment comment = CommentHelper.parse(sf);
						commentFound = comment.getValue().equals("Blockout");
					} else if (BlockoutHelper.isEIM(sf)) {
						collectSF = false;
						
						if (commentFound) {
							Blockout blockout = BlockoutHelper.parse(ioca.toByteArray(), pageSize.getXDpi().getValue());
							if (blockout.getDescription().matches(request.getDescriptionSearch())) {
								// do nothing, which effectively removes
								found = true;
							} else {
								out.write(ioca.toByteArray());
							}
						} else {
							out.write(ioca.toByteArray());
						}
						commentFound = false;
					}
				} else {
					out.write(sf.bytes());
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setSuccess(found);
		if (found) {
			returnData = out.toByteArray();
		}
		
		return returnData;
	}
	
	private void handleFindBlockoutRequest(final FindBlockoutRequest request, final byte[] data) throws IOException {
		List<Blockout> result = new ArrayList<>();
		
		// we need to iterate over the structured fields and find the presentation text sections that are text overlays
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		SizeDescriptor pageSize = getPageDescriptor(dis);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		boolean collectSF = false;
		boolean commentFound = false;
		StructuredField sf;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (BlockoutHelper.isBIM(sf)) {
					out = new ByteArrayOutputStream();
					collectSF = true;
					out.write(sf.bytes());
				} else if (collectSF) {
					out.write(sf.bytes());
					
					if (Comment.is(sf) && !commentFound) {
						Comment comment = CommentHelper.parse(sf);
						commentFound = comment.getValue().equals("Blockout");
					} else if (BlockoutHelper.isEIM(sf)) {
						collectSF = false;
						if (commentFound) {
							Blockout blockout = BlockoutHelper.parse(out.toByteArray(), pageSize.getXDpi().getValue());
							if (blockout.getDescription().matches(request.getDescriptionSearch())) {
								result.add(blockout);
							}
						}
						commentFound = false;
					}
				}
			}
		} catch (EOFException eof) {
			// end of stream
		}
		
		request.setResult(result);
	}

}
