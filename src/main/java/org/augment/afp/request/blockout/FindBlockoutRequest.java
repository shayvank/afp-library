package org.augment.afp.request.blockout;

import java.util.List;

public class FindBlockoutRequest implements BlockoutRequest {

	private String descriptionSearch;
	
	private List<Blockout> result;
	
	private FindBlockoutRequest(final String descriptionSearch) {
		this.descriptionSearch = descriptionSearch;
	}
	
	public String getDescriptionSearch() {
		return descriptionSearch;
	}
	
	public List<Blockout> getResult() {
		return result;
	}
	
	public void setResult(final List<Blockout> result) {
		this.result = result;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}
	
	public static FindBlockoutRequest findByDescription(final String matcher) {
		return new FindBlockoutRequest(matcher);
	}

}
