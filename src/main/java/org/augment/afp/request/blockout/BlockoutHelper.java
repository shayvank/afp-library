package org.augment.afp.request.blockout;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;

import org.augment.afp.request.comment.Comment;
import org.augment.afp.request.comment.CommentHelper;
import org.augment.afp.util.BigEndianBitSet;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.Coordinate;
import org.augment.afp.util.Dpi;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldBuilder;
import org.augment.afp.util.Triplet;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.UnitBase;
import org.augment.afp.util.Validations;

public class BlockoutHelper {

	private BlockoutHelper() {
		// static class
	}

	public static Blockout parse(final byte[] iocaBlockoutObject, final int pageDpi) throws IOException {
		Blockout returnValue = null;
		Blockout.Builder builder = new Blockout.Builder();
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(iocaBlockoutObject));
		
		boolean isBlockout = false;
		StringBuilder commentBuilder = new StringBuilder();
		
		StructuredField sf = null;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				
				if (sf.getCategoryCode() == CategoryCode.OBJECT_AREA && sf.getTypeCode() == TypeCode.POSITION) {
					// OBP
					//// we can get the x and y position
					byte[] xArray = ByteUtils.arraycopy(sf.getData(), 3, 2);
					double xPos = (double)ByteUtils.toShort(xArray);					
					byte[] yArray = ByteUtils.arraycopy(sf.getData(), 6, 2);
					double yPos = (double)ByteUtils.toShort(yArray);
					// we need the page dpi here
					Coordinate coord = new Coordinate(xPos / (double)pageDpi, yPos / (double)pageDpi);
					builder.withPosition(coord);
				} else if (sf.getCategoryCode() == CategoryCode.OBJECT_AREA && sf.getTypeCode() == TypeCode.DESCRIPTOR) {
					// OBD
					//// we can get the height, width, color
					List<Triplet> triplets = Triplet.parse(sf.getData(), 0, sf.getData().length);
					for (Triplet trip: triplets) {
						if (trip.getCode() == 0x4E) {
							byte[] colorArray = ByteUtils.arraycopy(trip.getContents(), 10, 2);
							builder.withColor(ByteUtils.toShort(colorArray));
						} else if (trip.getCode() == 0x4C) {
							byte[] wArray = ByteUtils.arraycopy(trip.getContents(), 2, 2);
							double width = (double)ByteUtils.toShort(wArray);					
							byte[] hArray = ByteUtils.arraycopy(trip.getContents(), 5, 2);
							double height = (double)ByteUtils.toShort(hArray);
							
							Dpi graphicDpi = new Dpi(UnitBase.TEN_INCHES, 14400);
							builder.withSize(height / graphicDpi.getValue(), width / graphicDpi.getValue());
						}
					}
					
				}
				
				
				
				if (Comment.is(sf)) {
					Comment comment = CommentHelper.parse(sf);
					if (comment.getValue().equals("Blockout")) {
						isBlockout = true;
					} else {
						commentBuilder.append(comment.getValue());
					}
				}
			}
		} catch (IOException eof) {
			// done reading
		}
		
		if (isBlockout) {
			builder.withDescription(commentBuilder.toString());
			returnValue = builder.build();
		}
		
		return returnValue;
	}
	
	public static byte[] format(final Blockout blockout, final double sheetWidth, final double sheetHeight, final int pageDpi) throws IOException {
		Validations.notNull(blockout, "Blockout object must not be null.");
		
		Dpi graphicDpi = new Dpi(UnitBase.TEN_INCHES, 14400);
		
		Dpi imageDpi = new Dpi(UnitBase.TEN_INCHES, 3000);

		short objectXSize = (short) (graphicDpi.getValue() * blockout.getWidth());
		short objectYSize = (short) (graphicDpi.getValue() * blockout.getHeight());
		
		short objectXSizeInImageDpi = (short) (imageDpi.getValue() * blockout.getWidth());
		short objectYSizeInImageDpi = (short) (imageDpi.getValue() * blockout.getHeight());
		

		
		/*
		 * BIM
		 *  BOG
		 *   OBD
		 *   OBP
		 *   IDD
		 *  EOG
		 *  IPD
		 *  IPD
		 * EIM
		 */
		
		
		
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();

		// BIM structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.BEGIN, CategoryCode.IMAGE).build().bytes());
		
		Comment nopBlockout = new Comment("Blockout");
		sfBytes.write(CommentHelper.format(nopBlockout).bytes());
		
		// if there is a description in the blockout, it will be placed here
		String description = blockout.getDescription();
		if (description != null && description.length() > 0) {
			Comment comment = new Comment(description);
			sfBytes.write(CommentHelper.format(comment).bytes());
		}
		
		// BOG structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.BEGIN, CategoryCode.OBJECT_ENVIRONMENT_GROUP).build().bytes());
		
		// OBD structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DESCRIPTOR, CategoryCode.OBJECT_AREA)
				.createTripletBuilder(0x43) // descriptor position triplet id
					.addTripletData(0x01) // object area position id
					.addTriplet()
				.createTripletBuilder(0x4B)
					.addTripletData(0x00) // presentation space unit base for X - 10 inches
					.addTripletData(0x00) // presentation space unit base for Y - 10 inches
					.addTripletData(ByteUtils.fromShort((short) graphicDpi.getUnitsPerUnitBase()))
					.addTripletData(ByteUtils.fromShort((short) graphicDpi.getUnitsPerUnitBase()))
					.addTriplet()
				.createTripletBuilder(0x4C) //object area size triplet id
					.addTripletData(0x02) // object area size type
					.addTripletData(0x00) // object area extend for x axis - 3 bytes, this line is leading zero
					.addTripletData(ByteUtils.fromShort(objectXSize))
					.addTripletData(0x00) // object area extend for y axis - 3 bytes, this line is leading zero
					.addTripletData(ByteUtils.fromShort(objectYSize))
					.addTriplet()
				.createTripletBuilder(0x70) // presentation space reset mixing triplet
					.addTripletData(0x01) // mixing flag
					.addTriplet()
				.createTripletBuilder(0x4E) // color specification
					.addTripletData(0x00) // reserved
					.addTripletData(0x40) // oca color
					.addTripletData(new byte[] {0x00, 0x00, 0x00, 0x00}) // reserved
					.addTripletData(0x10) // default for oca color
					.addTripletData(0x00) // default for oca color
					.addTripletData(0x00) // default for oca color
					.addTripletData(0x00) // default for oca color
					.addTripletData(ByteUtils.fromShort((short) blockout.getColor())) // color in oca
					.addTriplet()
				.build().bytes());
				
		
		// OBP structured field		
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.POSITION, CategoryCode.OBJECT_AREA)
				.addData(0x01) // object area position id
				.addData(0x17) // total length of repeating group
				.addData(0x00)
				.addData(ByteUtils.fromShort((short) (pageDpi * blockout.getPosition().getX()))) // x-axis origin of the object area (AreaPosition)
				.addData(0x00)
				.addData(ByteUtils.fromShort((short) (pageDpi * blockout.getPosition().getY()))) // y-axis origin of the object area (AreaPosition)
				.addData(new byte[] {0x00, 0x00}) // object area's x-axis rotation from the x-axis (AreaXYrotation)
				.addData(new byte[] {0x2d, 0x00}) // object area's y-axis rotation from the y-axis (AreaXYrotation)
				.addData(0x00) // reserved
				.addData(new byte[] {0x00, 0x00, 0x00}) // x-axis origin for object content (ContentPosition)
				.addData(new byte[] {0x00, 0x00, 0x00}) // y-axis origin for object content (ContentPosition)
				.addData(new byte[] {0x00, 0x00}) // object content x-axis rotation from x-axis (ContentXYrotation)
				.addData(new byte[] {0x2d, 0x00}) // object content y-axis rotation from y-axis (ContentXYrotation)
				.addData(0x01) // reference coordinate system - Page or overlay defined by IPS
				.build().bytes());		
		
		// IDD structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DESCRIPTOR, CategoryCode.IMAGE)
				.addData(0x00) // unit base (10 inches)
				.addData(ByteUtils.fromShort((short) imageDpi.getUnitsPerUnitBase())) // xresol (h)
				.addData(ByteUtils.fromShort((short) imageDpi.getUnitsPerUnitBase())) // yresol (v)
				.addData(ByteUtils.fromShort(objectXSizeInImageDpi)) // h-size in xres
				.addData(ByteUtils.fromShort(objectYSizeInImageDpi)) // v-size in yres
				.addData(0xF6) // bilevel color
				.addData(0x04) // length
				.addData(0x00) // foreground
				.addData(0x00) // reserved
				.addData(ByteUtils.fromShort((short) blockout.getColor())) // color in oca
				.build().bytes());
		
		// EOG structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.END, CategoryCode.OBJECT_ENVIRONMENT_GROUP).build().bytes());

		// IPD	
		byte compressionSetting = blockout.isCompressed() ? (byte) 0x82 : 0x03;
		
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DATA, CategoryCode.IMAGE)
				.addData(0x70) // begin segment
				.addData(0x00) // length
				.addData(0x91) // begin image content
				.addData(0x01) // length
				.addData(0xFF) // ioca
				.addData(0x94) // image size parameter
				.addData(0x09) // length
				.addData(0x00) // unit base
				.addData(ByteUtils.fromShort((short) imageDpi.getUnitsPerUnitBase())) // xresol (h)
				.addData(ByteUtils.fromShort((short) imageDpi.getUnitsPerUnitBase())) // yresol (v)
				.addData(ByteUtils.fromShort(objectXSizeInImageDpi)) // h-size in xres
				.addData(ByteUtils.fromShort(objectYSizeInImageDpi)) // v-size in yres
				.addData(0x95) // image encoding parameter
				.addData(0x02) // length
				.addData(compressionSetting) // compression id
				.addData(0x01) // ridic
				.addData(0x96) // ide size parameter
				.addData(0x01) // length
				.addData(0x01) // 1bit
				.addData(new byte[] {(byte)0x97, 0x01, 0x00}) // retired iamge lut-id parameter
				.build().bytes());
		
		if (blockout.isCompressed()) {
			int iddY = objectYSizeInImageDpi;
			ByteArrayOutputStream compressedBytes = new ByteArrayOutputStream();
			while (iddY >= 8) {
				compressedBytes.write(0xFF);
				iddY -= 8;
			}
			
			if (iddY > 0) {
				BigEndianBitSet lastByte = new BigEndianBitSet();
				for (int i=0; i < iddY; i++) {
					lastByte.set(i);
				}
				compressedBytes.write(lastByte.toByte());
			}
			compressedBytes.write(new byte[] { 0x00, 0x10, 0x01} ); // end-of-facsimile block (EOFB)
			sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DATA, CategoryCode.IMAGE)
					.addData(new byte[] {(byte)0xFE, (byte)0x92}) // image data
					.addData(ByteUtils.fromShort((short) compressedBytes.size()))
					.addData(compressedBytes.toByteArray())
					.build().bytes());
		} else {
			int maxDataLen = 8180;
			
			int totalImagePointBits = objectXSizeInImageDpi * objectYSizeInImageDpi;
			int totalImageBytes = totalImagePointBits / 8;
			
			while (totalImageBytes > maxDataLen) {
				ByteArrayOutputStream imageBytes = new ByteArrayOutputStream();
				for (int i = 0; i < maxDataLen; i++) {
					imageBytes.write(0x00);
				}
				sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DATA, CategoryCode.IMAGE)
						.addData(new byte[] {(byte)0xFE, (byte)0x92}) // image data
						.addData(ByteUtils.fromShort((short) imageBytes.size()))
						.addData(imageBytes.toByteArray())
						.build().bytes());
				totalImageBytes -= maxDataLen;
			}
	
			if (totalImageBytes > 0) {
				ByteArrayOutputStream imageBytes = new ByteArrayOutputStream();
				for (int i = 0; i < totalImageBytes; i++) {
					imageBytes.write(0x00);
				}
				sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DATA, CategoryCode.IMAGE)
						.addData(new byte[] {(byte)0xFE, (byte)0x92}) // image data
						.addData(ByteUtils.fromShort((short) imageBytes.size()))
						.addData(imageBytes.toByteArray())
						.build().bytes());
			}
		}
		
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.DATA, CategoryCode.IMAGE)
				.addData(0x93) // end image
				.addData(0x00) // length
				.addData(0x71) // end segment
				.addData(0x00) // length
				.build().bytes());
		
		// EIM structured field
		sfBytes.write(StructuredFieldBuilder.createBuilder(TypeCode.END, CategoryCode.IMAGE).build().bytes());
		
		return sfBytes.toByteArray();
	}
	
	public static boolean isBIM(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.IMAGE && sf.getTypeCode() == TypeCode.BEGIN;
	}
	
	public static boolean isEIM(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.IMAGE && sf.getTypeCode() == TypeCode.END;
	}
}
