package org.augment.afp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.augment.afp.data.PrintFile;
import org.augment.afp.data.PrintFileBuilder;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredFieldFileParser;
import org.augment.afp.util.TypeCode;

/**
 * The AfpFileReader is the starting point of the augmentation process.  This class is used to read and parse an 
 * initial AFP file into data stores and provides a PrintFile for iterating through the pagegroups for augmentation requests.
 *
 */
public class AfpFileReader extends AbstractFileHeader {
	/*
	 * AfpFile idea
	 * * let's not support document indexes for now
	 */
	
	private final File file;
	
	/**
	 * Creates a new instance given an AFP file and a PrintFileBuilder instance.
	 * 
	 * @param file AFP file 
	 * @param fileBuilder provides the implementation for storing MO:DCA data int a datastore
	 */
	public AfpFileReader(final File file, final PrintFileBuilder fileBuilder) {
		super(fileBuilder);
		this.file = file;	
	}
	
	/**
	 * Parses the AFP file into objects and returns the PrintFile object that can be used for working with augmentation processing.
	 * 
	 * @return printfile object
	 * @throws IOException if any io exception occurs
	 */
	public PrintFile parse() throws IOException {		
		try (StructuredFieldFileParser reader = new StructuredFieldFileParser(file)) {
			while (readNextField(reader) != null) {						
				if (field.getCategoryCode() == CategoryCode.PRINT_FILE) {
					if (field.getTypeCode() == TypeCode.BEGIN) {
						fileBuilder.storeBPF(field);
					} else {
						fileBuilder.storeEPF(field);
					}
				}
				
				if (field.getTypeCode() == TypeCode.BEGIN && field.getCategoryCode() == CategoryCode.RESOURCE_GROUP) {
					fileBuilder.storeBRG(field);
					loadFileLevelResourceGroup(reader);
				}
				
				if (field.getTypeCode() == TypeCode.BEGIN && field.getCategoryCode() == CategoryCode.DOCUMENT) {						
					fileBuilder.storeBDT(field);
					loadPageGroups(reader);
				}
			}
		}
		
		return ((PrintFileBuilder)fileBuilder).build();
	}
	
	/**
	 * Loads the pagegroups.
	 * 
	 * @param reader afp file parser
	 * @throws IOException if any io exception occurs
	 */
	private void loadPageGroups(final StructuredFieldFileParser reader)  throws IOException {		
		ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();
		while (readNextField(reader) != null) {
			if (field.getTypeCode() == TypeCode.BEGIN && field.getCategoryCode() == CategoryCode.PAGE_GROUP) {
				outputBuffer = new ByteArrayOutputStream();
				outputBuffer.write(field.bytes());
			} else if (field.getTypeCode() == TypeCode.END && field.getCategoryCode() == CategoryCode.PAGE_GROUP) {
				outputBuffer.write(field.bytes());
				fileBuilder.storePageGroup(outputBuffer.toByteArray());
				outputBuffer.close();
			} else if (field.getTypeCode() == TypeCode.END && field.getCategoryCode() == CategoryCode.DOCUMENT) {
				fileBuilder.storeEDT(field);
				break;
			} else {
				outputBuffer.write(field.bytes());
			}
		}
	}
	
}
