package org.augment.afp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.augment.afp.data.BasicPrintFileBuilder;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldFileParser;
import org.augment.afp.util.TypeCode;

public abstract class AbstractFileHeader {

	protected StructuredField field;
	protected BasicPrintFileBuilder fileBuilder;
	
	AbstractFileHeader(final BasicPrintFileBuilder fileBuilder) {
		this.fileBuilder = fileBuilder;
	}
	
	/**
	 * Reads the next structuredfield in the parser.
	 * 
	 * @param reader structuredfieldfileparser
	 * @return structured fiel or null if end of file
	 * @throws IOException if any io exception occurs
	 */
	protected StructuredField readNextField(StructuredFieldFileParser reader) throws IOException {
		field = reader.readNextSfBytes();
		return field;
	}
	
	/**
	 * Loads the file level resource group.
	 * 
	 * @param reader afp file parser
	 * @throws IOException if any io exception occurs
	 */
	protected void loadFileLevelResourceGroup(final StructuredFieldFileParser reader) throws IOException {
		ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();
		String name = null;
		while (readNextField(reader) != null) {
			if (field.getTypeCode() == TypeCode.BEGIN && field.getCategoryCode() == CategoryCode.NAMED_RESOURCE) {
				name = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(field.getData(), 0, 8));
				outputBuffer = new ByteArrayOutputStream();
				outputBuffer.write(field.bytes());				
			} else if (field.getTypeCode() == TypeCode.END && field.getCategoryCode() == CategoryCode.NAMED_RESOURCE) {
				outputBuffer.write(field.bytes());
				fileBuilder.storeResource(name, outputBuffer.toByteArray());
				outputBuffer.close();
				name = null;
			} else if (field.getTypeCode() == TypeCode.END && field.getCategoryCode() == CategoryCode.RESOURCE_GROUP) {				
				fileBuilder.storeERG(field);
				break;
			} else {
				outputBuffer.write(field.bytes());
			}
		}
	}
}
