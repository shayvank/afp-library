package org.augment.afp.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldOutputStream;

public class PrintFileDocumentGroup {

	private StateEnvelope envelope = null;
	private PrintFileResourceGroup resourceGroup;
	private List<PageGroup> pageGroups;
	
	public PrintFileDocumentGroup(final PrintFileResourceGroup resourceGroup) {
		this.resourceGroup = resourceGroup;
		this.pageGroups = new ArrayList<>();
	}
	
	public void addPageGroup(final byte[] pagegroup, final PageGroupDataStore pageGroupDataStore) {
		UUID uuid = UUID.randomUUID();
		String uuidStr = uuid.toString().replace("-", "");
		pageGroups.add(new org.augment.afp.data.PageGroup(uuidStr, pageGroupDataStore, resourceGroup));
		pageGroupDataStore.store(uuidStr, pagegroup);
	}
	
	public int getPageGroupCount() {
		return pageGroups.size();
	}

	public PageGroup getPageGroupData(final int index) {
		// we need to load the pagegroup data (parse) and determine the page boundaries as well as the plex, etc.
		
		if (index < 0) {
			throw new IllegalArgumentException("Index is less than zero.");
		}		
		
		return pageGroups.get(index);
	}
	
	void setStart(final StructuredField start) {
		envelope = new StateEnvelope(start);
	}
	
	void setEnd(final StructuredField end) {
		envelope.setEnd(end);
	}

	public void writeBeginEnvelopeTo(StructuredFieldOutputStream outputStream) throws IOException {
		outputStream.writeStructuredField(envelope.getBegin());
	}
	
	public void writeEndEnvelopeTo(StructuredFieldOutputStream outputStream) throws IOException {
		outputStream.writeStructuredField(envelope.getEnd());
	}

}
