package org.augment.afp.data;

import java.io.IOException;

import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldOutputStream;

public abstract class BasicPrintFileBuilder {
	
	protected StateEnvelope printFileEnvelope;
	
	// building the resource group
	protected PrintFileResourceGroup resourceGroup;
	protected PrintFileResourceFactory resFactory;
	
	public BasicPrintFileBuilder(final ResourceDataStore resourceDataStore) {
		this.resourceGroup = new PrintFileResourceGroup();
		resFactory = new PrintFileResourceFactory(resourceDataStore);
	}
	
	PrintFileResourceGroup getResourceGroup() {
		return resourceGroup;
	}
	
	public void storeBPF(StructuredField bpfField) {
		printFileEnvelope = new StateEnvelope(bpfField);
	}
	
	public void storeEPF(StructuredField epfField) {
		printFileEnvelope.setEnd(epfField);
	}
	
	public void storeBRG(final StructuredField brgField) {
		if (resourceGroup.getEnvelope() != null) {
			throw new IllegalArgumentException("Duplicate Begin Resource Group structured field.");
		}
		resourceGroup.setStart(brgField);
	}
	
	public void storeERG(final StructuredField ergField) {
		if (resourceGroup.getEnvelope() != null && resourceGroup.getEnvelope().getEnd() != null) {
			throw new IllegalArgumentException("Duplicate End Resource Group structured field.");
		}
		resourceGroup.setEnd(ergField);
	}
	
	public void storeResource(final String name, final byte[] resource) {
		resourceGroup.addResource(resFactory.createResource(name, resource));
	}
	
	public abstract void storeBDT(StructuredField bdtField);

	public abstract void storeEDT(StructuredField edtField);
	
	public abstract void storePageGroup(byte[] pageGroup);
	
	public void writeBeginEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		if (printFileEnvelope != null) {
			outputStream.writeStructuredField(printFileEnvelope.getBegin());
		}
	}

	public void writeEndEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		if (printFileEnvelope != null) {
			outputStream.writeStructuredField(printFileEnvelope.getEnd());
		}
	}
}
