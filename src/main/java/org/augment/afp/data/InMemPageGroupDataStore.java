package org.augment.afp.data;

import java.util.HashMap;
import java.util.Map;

/**
 * In-memory implementation of the PageGroupDataStore.
 *
 */
public class InMemPageGroupDataStore implements PageGroupDataStore {
	
	private Map<String, byte[]> pageGroups;
	
	/**
	 * Creates a new instance of the data store.
	 */
	public InMemPageGroupDataStore() {
		this.pageGroups = new HashMap<>();
	}
	
	@Override
	public void store(final String id, final byte[] bytes) {
		this.pageGroups.put(id, bytes);
	}
	
	@Override
	public byte[] get(final String id) {
		return pageGroups.get(id);
	}

}
