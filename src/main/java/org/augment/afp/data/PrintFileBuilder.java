package org.augment.afp.data;

import java.util.ArrayList;
import java.util.List;

import org.augment.afp.util.StructuredField;

public class PrintFileBuilder extends BasicPrintFileBuilder {
	
	// building the pagegroup (documents)
	private List<PrintFileDocumentGroup> documents;
	private PrintFileDocumentGroup currentDocGroup;
	
	private PageGroupDataStore pageGroupDataStore;
	
	public PrintFileBuilder(final ResourceDataStore resourceDataStore, final PageGroupDataStore pageGroupDataStore) {
		super(resourceDataStore);
		this.documents = new ArrayList<>();
		this.pageGroupDataStore = pageGroupDataStore;
	}
	
	@Override
	public void storeBDT(final StructuredField bdtField) {
		this.currentDocGroup = new PrintFileDocumentGroup(resourceGroup);
		this.currentDocGroup.setStart(bdtField);
	}
	
	@Override
	public void storeEDT(final StructuredField edtField) {
		this.currentDocGroup.setEnd(edtField);
		documents.add(currentDocGroup);
		this.currentDocGroup = null;
	}
	
	@Override
	public void storePageGroup(final byte[] pageGroup) {
		this.currentDocGroup.addPageGroup(pageGroup, pageGroupDataStore);
	}
	
	public PrintFile build() {
		return new PrintFile(printFileEnvelope, resourceGroup, documents);
	}
}
