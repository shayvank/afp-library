package org.augment.afp.data;

import java.util.HashMap;
import java.util.Map;

/**
 * In-memory implementation of the ResourceDataStore.
 *
 */
public class InMemResourceDataStore implements ResourceDataStore {

	private Map<String, byte[]> inMemMap;
	
	/**
	 * Creates a new instance of the data store.
	 */
	public InMemResourceDataStore() {
		this.inMemMap = new HashMap<>();
	}
	
	@Override
	public void store(final String name, final byte[] bytes) {
		inMemMap.put(name, bytes);
	}

	@Override
	public byte[] get(final String name) {
		return inMemMap.get(name);
	}

}
