package org.augment.afp.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.augment.afp.data.formmap.FormMap;
import org.augment.afp.data.formmap.FormMapResource;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldOutputStream;

public class PrintFileResourceGroup {

	private StateEnvelope envelope = null;
	
	private Map<String, NamedResource> resources;
	
	private FormMap formMap;
	
	public PrintFileResourceGroup() {
		this.resources = new LinkedHashMap<>();
	}
	
	void setStart(final StructuredField start) {
		envelope = new StateEnvelope(start);
	}
	
	void setEnd(final StructuredField end) {
		envelope.setEnd(end);
	}
	
	public void addResource(final NamedResource resource) {
		if (resources.containsKey(resource.getName())) {
			throw new IllegalArgumentException("Resource with name " + resource.getName() + " already exists!");
		}
		if (resource instanceof FormMapResource) {
			formMap = ((FormMapResource) resource).getFormMap();
		}
		
		this.resources.put(resource.getName(), resource);
	}
	
	public void removeResource(final String resourceName) {
		resources.remove(resourceName);
	}
	
	public StateEnvelope getEnvelope() {
		return envelope;
	}

	public List<NamedResource> getResources() {
		return new ArrayList<>(resources.values());
	}

	public NamedResource getResourceByName(final String name) {
		return resources.get(name);
	}
	
	public FormMap getFormMap() {
		return this.formMap;
	}

	public void writeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		// write out the envelope start
		outputStream.writeStructuredField(envelope.getBegin());
		
		for (NamedResource resource: getResources()) {
			resource.writeTo(outputStream);
		}
		
		outputStream.writeStructuredField(envelope.getEnd());
	}

}
