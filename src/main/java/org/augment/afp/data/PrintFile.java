package org.augment.afp.data;

import java.io.IOException;
import java.util.List;

import org.augment.afp.util.StructuredFieldOutputStream;

public class PrintFile {

	private StateEnvelope printFileEnvelope;
	private PrintFileResourceGroup resourceGroup;
	private List<PrintFileDocumentGroup> documents;
	
	public PrintFile(final StateEnvelope printFileEnvelope, final PrintFileResourceGroup resourceGroup, final List<PrintFileDocumentGroup> documents) {
		this.printFileEnvelope = printFileEnvelope;
		this.resourceGroup = resourceGroup;
		this.documents = documents;
	}
	
	public PrintFileResourceGroup getResourceGroup() {
		return resourceGroup;
	}

	public List<PrintFileDocumentGroup> getDocuments() {
		return documents;
	}

	public void writeBeginEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		if (printFileEnvelope != null) {
			outputStream.writeStructuredField(printFileEnvelope.getBegin());
		}
	}

	public void writeEndEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		if (printFileEnvelope != null) {
			outputStream.writeStructuredField(printFileEnvelope.getEnd());
		}
	}

}
