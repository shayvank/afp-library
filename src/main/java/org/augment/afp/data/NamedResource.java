package org.augment.afp.data;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldOutputStream;

/**
 * A named resource is initiated by a begin resource structured field and terminated by an end resource structured field.
 * Structured fields that define resources may be encountered in the named resource state.
 *
 */
public class NamedResource {

	protected String name;
	
	protected ResourceDataStore dataStore;
	
	/**
	 * Create a new instance given the name and resource data store (where the byte data will be stored).
	 * 
	 * @param name resource name
	 * @param dataStore resource data store
	 */
	public NamedResource(final String name, final ResourceDataStore dataStore) {
		this.name = name;
		this.dataStore = dataStore;
	}
	
	/**
	 * Returns the name of the named resource.
	 * 
	 * @return name of the resource
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the data of the named resource.
	 * 
	 * @return data of the resource
	 */
	public byte[] getData() {
		return dataStore.get(name);	
	}
	
	/**
	 * Writes the named resource to the provided structured field output stream.
	 * 
	 * @param outputStream structured field output stream
	 * @throws IOException if an io exception occurs
	 */
	public void writeTo(StructuredFieldOutputStream outputStream) throws IOException {
		byte[] resourceData = dataStore.get(name);
		if (resourceData != null) {
			DataInputStream dis = new DataInputStream(new ByteArrayInputStream(resourceData));
			StructuredField sf = null;
			try {
				while ((sf = StructuredField.getNext(dis)) != null) {
					outputStream.writeStructuredField(sf);
				}
			} catch (EOFException eof) {
				// end of stream
			}
			
		}
	}
}
