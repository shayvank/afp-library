package org.augment.afp.data;

/**
 * Resource data store stores named resources from the print file level resoucce group.
 *
 */
public interface ResourceDataStore {

	/**
	 * Stores (saves) the resource name and data.
	 * 
	 * @param name resource name
	 * @param bytes data
	 */
	void store(String name, byte[] bytes);
	
	/**
	 * Gets the resource data given the resource name.
	 * 
	 * @param name resource name
	 * @return data
	 */
	byte[] get(String name);
}
