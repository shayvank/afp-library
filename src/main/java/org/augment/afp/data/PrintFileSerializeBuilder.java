package org.augment.afp.data;

import java.io.IOException;

import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldOutputStream;

public class PrintFileSerializeBuilder extends BasicPrintFileBuilder {

	private StateEnvelope envelope = null;
	private InMemPageGroupDataStore pageGroupDataStore;
	private PageGroup pgGrp;
	
	public PrintFileSerializeBuilder() {
		super(new InMemResourceDataStore());
		this.pageGroupDataStore = new InMemPageGroupDataStore();
	}

	@Override
	public void storeBDT(final StructuredField bdtField) {
		envelope = new StateEnvelope(bdtField);
	}

	@Override
	public void storeEDT(final StructuredField edtField) {
		envelope.setEnd(edtField);
	}
	
	public void writeBeginDocumentEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		if (envelope != null) {
			outputStream.writeStructuredField(envelope.getBegin());
		}
	}

	public void writeEndDocumentEnvelopeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		if (envelope != null) {
			outputStream.writeStructuredField(envelope.getEnd());
		}
	}
	
	@Override
	public void storePageGroup(final byte[] pageGroup) {
		String pageGroupId = "pageGroupId";
		pageGroupDataStore.store(pageGroupId, pageGroup);
		pgGrp = new PageGroup(pageGroupId, pageGroupDataStore, resourceGroup);
	}
	
	public PageGroup getPageGroup() {
		return pgGrp;
	}
	
	@Override
	public PrintFileResourceGroup getResourceGroup() {
		return super.getResourceGroup();
	}

}
