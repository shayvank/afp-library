package org.augment.afp.data;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

import org.augment.afp.data.formmap.MediumMap;
import org.augment.afp.data.formmap.PageDescriptor.SheetGroup;
import org.augment.afp.data.formmap.SizeDescriptor;
import org.augment.afp.request.ActionRequest;
import org.augment.afp.request.RequestHandler;
import org.augment.afp.request.RequestHandlerLoader;
import org.augment.afp.util.AugmentAfpException;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldOutputStream;
import org.augment.afp.util.TypeCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Page {

	private static final Logger LOGGER = LoggerFactory.getLogger(Page.class);
	
	private byte[] data;
	
	private MediumMap mediumMap;
	
	private SheetGroup sheetSide;
	
	private SizeDescriptor pgd;
	
	public Page(final byte[] data, final MediumMap mediumMap, final SheetGroup sheetSide) {
		this.data = data;
		this.mediumMap = mediumMap;
		this.sheetSide = sheetSide;
		
		StructuredField sf = null;
		try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data))) {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (SizeDescriptor.isPageDescriptor(sf)) {
					pgd = SizeDescriptor.parse(sf);
					break;
				}
			}
		} catch (Exception e) {
			throw new AugmentAfpException(e.getMessage(), e);
		}
	}
	
	public MediumMap getMediumMap() {
		return mediumMap;
	}
	
	public SheetGroup getSheetSide() {
		return sheetSide;
	}
	
	public boolean isDuplex() {
		return mediumMap.getMmc().isDuplex();
	}
	
	public boolean isFrontSide() {
		return !sheetSide.isBackSide();
	}
	
	public SizeDescriptor getPgd() {
		return pgd;
	}
	
	public void execute(final ActionRequest request) {
		RequestHandler handler = RequestHandlerLoader.findHandler(request);
		try {
			if (handler != null) {
				byte[] returnValue = handler.handle(request, data);
				if (request.isUpdate()) {
					data = returnValue;
				}
			}
		} catch (IOException e) {
			LOGGER.error("Error during Page ActionRequest execution", e);
			throw new AugmentAfpException(e.getMessage(), e);
		}				
	}

	public void writeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		StructuredField sf = null;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				outputStream.writeStructuredField(sf);
			}
		} catch (EOFException eof) {
			// end of stream
		}
	}
	
	public static boolean isBPG(final StructuredField sf) {
		return sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.PAGE;
	}
	
	public static boolean isEPG(final StructuredField sf) {
		return sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.PAGE;
	}
	
}
