package org.augment.afp.data.formmap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

import org.augment.afp.data.PrintFile;
import org.augment.afp.data.PrintFileResourceFactory;
import org.augment.afp.data.ResourceDataStore;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class FormMapResourceHelper {

	private FormMapResourceHelper() {
		// static class
	}
	
	public static void appendMediumMapToFormMap(final PrintFile afpFile, final PrintFileResourceFactory resFactory, final ResourceDataStore resourceDataStore, final byte[] mediumMapBytes) throws IOException {
		// in order to add to the form map, we will need to get the form map
		FormMap formMap = afpFile.getResourceGroup().getFormMap();
		String formMapName = formMap.getName();
		afpFile.getResourceGroup().removeResource(formMapName);
		
		byte[] formMapResourceBytes = resourceDataStore.get(formMapName);
		byte[] newFormMap = appendMediumMap(formMapResourceBytes, mediumMapBytes);
		afpFile.getResourceGroup().addResource(resFactory.createResource(formMapName, newFormMap));
	}
	
	private static byte[] appendMediumMap(byte[] formMapResourceBytes, byte[] mediumMap) throws IOException {
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(formMapResourceBytes));
		
		StructuredField sf = null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.FORM_MAP) {
					out.write(mediumMap);
				}
				out.write(sf.bytes());
			}
		} catch (EOFException eof) {
			// end of stream
		}
		return out.toByteArray();
	}
}
