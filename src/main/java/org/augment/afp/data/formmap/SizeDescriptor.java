package org.augment.afp.data.formmap;

import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.Dpi;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;
import org.augment.afp.util.UnitBase;

public class SizeDescriptor {

	private Dpi xDpi;
	private Dpi yDpi;
	private int xSize;
	private int ySize;
	
	public SizeDescriptor(final Dpi xDpi, final Dpi yDpi, final int xSize, final int ySize) {
		this.xDpi = xDpi;
		this.yDpi = yDpi;
		this.xSize = xSize;
		this.ySize = ySize;
	}
	
	public Dpi getXDpi() {
		return xDpi;
	}
	
	public Dpi getYDpi() {
		return yDpi;
	}
	
	public int getXSize() {
		return xSize;
	}
	
	public int getYSize() {
		return ySize;
	}
	
	public double getXSizeInUnits() {
		return getXSize() * 1.0 / getXDpi().getValue();
	}
	
	public double getYSizeInUnits() {
		return getYSize() * 1.0 / getYDpi().getValue();
	}
	
	public static boolean isMediumDescriptor(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.MEDIUM && sf.getTypeCode() == TypeCode.DESCRIPTOR;
	}
	
	public static boolean isPageDescriptor(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.PAGE && sf.getTypeCode() == TypeCode.DESCRIPTOR;
	}
	
	public static SizeDescriptor parse(final StructuredField mddField) {
		// we are going to assume that we have mdd field of pgd
		byte[] data = mddField.getData();
		Dpi xDpi = new Dpi(UnitBase.valueOf(data[0]), ByteUtils.toShort(ByteUtils.arraycopy(data, 2,2)));
		Dpi yDpi = new Dpi(UnitBase.valueOf(data[1]), ByteUtils.toShort(ByteUtils.arraycopy(data, 4,2)));
		int xSize = ByteUtils.toInt(ByteUtils.arraycopy(data, 6, 3));
		int ySize = ByteUtils.toInt(ByteUtils.arraycopy(data, 9, 3));
				
		return new SizeDescriptor(xDpi, yDpi, xSize, ySize);
	}
}
