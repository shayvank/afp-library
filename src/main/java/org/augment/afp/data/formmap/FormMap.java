package org.augment.afp.data.formmap;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.augment.afp.util.AugmentAfpException;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class FormMap {
	
	private String name;
	
	private Map<String, MediumMap> mediumMaps;
	
	public FormMap(final String name, Map<String, MediumMap> mediumMaps) {
		this.name = name;
		this.mediumMaps = mediumMaps;
	}
	
	public String getName() {
		return name;
	}
	
	public MediumMap getMediumMap(final String name) {
		return mediumMaps.get(name);
	}
	
	public Map<String, MediumMap> getMediumMaps() {
		return mediumMaps;
	}
	
	public static FormMap parse(final String formMapName, final DataInputStream dis) {
		PageDescriptor pgp = null;
		SizeDescriptor mdd = null;
		Map<String, MediumMap> maps = new HashMap<>();
		
		// we will assume we are starting with a BFM sf
		StructuredField sf = null;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (PageDescriptor.is(sf)) {
					pgp = PageDescriptor.parse(sf);
				} else if (SizeDescriptor.isMediumDescriptor(sf)) {
					mdd = SizeDescriptor.parse(sf); 
				} else if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.BEGIN) {
					// do the medium map stuff now
					String mmName = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
					MediumMap mediumMap = parse(mmName, pgp, mdd, dis);
					maps.put(mediumMap.getName(), mediumMap);
				}
			}
		} catch (EOFException e) {
			// end of stream
		} catch (IOException e) {
			throw new AugmentAfpException(e.getMessage(), e);
		}
		
		return new FormMap(formMapName, maps);
	}
	
	public static MediumMap parse(final String name, final PageDescriptor pgp, final SizeDescriptor mdd, final DataInputStream dis) throws IOException {
		PageDescriptor mPgp = pgp;
		SizeDescriptor mMdd = mdd;
		MediumMap map = new MediumMap(name);
		MediumModificationControl mmc = null;
		// this will run until end medium map
		StructuredField sf = null;
		while ((sf = StructuredField.getNext(dis)) != null) {
			if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.END) {
				map = new MediumMap(name, mPgp, mMdd, mmc);
				break;
			}
			
			if (PageDescriptor.is(sf)) {
				mPgp = PageDescriptor.parse(sf);
			} else if (SizeDescriptor.isMediumDescriptor(sf)) {
				mMdd = SizeDescriptor.parse(sf); 
			} else if (MediumModificationControl.is(sf)) {
				mmc = MediumModificationControl.parse(sf);
			}
		}
		return map;
	}
}
