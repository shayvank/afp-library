package org.augment.afp.data.formmap;

public class MediumMap {

	private String name;
	
	private PageDescriptor pgp;
	private SizeDescriptor mdd;
	private MediumModificationControl mmc;
	
	public MediumMap(final String name) {
		this.name = name;
	}
	
	public MediumMap(final String name, final PageDescriptor pgp, final SizeDescriptor mdd, final MediumModificationControl mmc) {
		this.name = name;
		this.pgp = pgp;
		this.mdd = mdd;
		this.mmc = mmc;
	}
	
	public String getName() {
		return name;
	}
	
	public PageDescriptor getPgp() {
		return pgp;
	}
	
	public SizeDescriptor getMdd() {
		return mdd;
	}
	
	public MediumModificationControl getMmc() {
		return mmc;
	}
}
