package org.augment.afp.data.formmap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;

import org.augment.afp.data.NamedResource;
import org.augment.afp.data.ResourceDataStore;
import org.augment.afp.data.StateEnvelope;
import org.augment.afp.util.AugmentAfpException;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class FormMapResource extends NamedResource {
	
	private FormMap formMap;
	
	public FormMapResource(final String name, final ResourceDataStore dataStore, final FormMap formMap) {
		super(name, dataStore);
		this.formMap = formMap;
	}
	
	public FormMap getFormMap() {
		return formMap;
	}
	
	public void addMediumMap(final byte[] mediumMapData) {
		
		// we need to add the medium map right after the D3 A9 C4
		byte[] fmData = dataStore.get(name);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(fmData))) {
			StructuredField sf = null;
			
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.DOCUMENT_ENVIRONMENT_GROUP) {
					baos.write(sf.bytes());
					baos.write(mediumMapData);
				} else {
					baos.write(sf.bytes());
				}
			}
		} catch (EOFException e) {
			// do nothing
		} catch (Exception e) {
			throw new AugmentAfpException(e.getMessage(), e);
		}
		dataStore.store(name, baos.toByteArray());
		
		formMap = FormMap.parse(name, new DataInputStream(new ByteArrayInputStream(dataStore.get(name))));
	}
	
	public void removeMediumMap(final String mapName) {
		byte[] fmData = dataStore.get(name);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(fmData))) {
			StructuredField sf = null;
			ByteArrayOutputStream map = new ByteArrayOutputStream();
			boolean collectMap = false;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.BEGIN) {
					map = new ByteArrayOutputStream();
					collectMap = true;
					map.write(sf.bytes());
				} else if (collectMap) {
					map.write(sf.bytes());
					if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.END) {
						collectMap = false;
						byte[] data = sf.getData();
						byte[] nameBytes = ByteUtils.arraycopy(data, 0, 8);
						String theName = (ByteUtils.getDataEncoding().stringValue(nameBytes));
						if (theName.equals(mapName)) {
							// do nothing, which effectively removes
						} else {
							// this is not what we are looking for, write it out
							baos.write(map.toByteArray());
						}
					}
				} else {
					baos.write(sf.bytes());
				}
			}
		} catch (EOFException e) {
			// do nothing
		} catch (Exception e) {
			throw new AugmentAfpException(e.getMessage(), e);
		}
		dataStore.store(name, baos.toByteArray());
		
		formMap = FormMap.parse(name, new DataInputStream(new ByteArrayInputStream(dataStore.get(name))));
	}
	
	public byte[] getMediumMapData(final String mapName) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		byte[] fmData = dataStore.get(name);
		boolean mmFound = false;
		try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(fmData))) {
			StructuredField sf = null;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.BEGIN) {
					// we need to now get the name
					String mmName = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
					if (mapName.equals(mmName)) {
						// this is the start of a medium map for the found name
						baos.write(sf.bytes());
						mmFound = true;
					}
				} else if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.END) {
					if (mmFound) {
						baos.write(sf.bytes());
						mmFound = false;
					}
				} else if (mmFound) {
					baos.write(sf.bytes());
				}
			}
		} catch (EOFException e) {
			// do nothing
		} catch (Exception e) {
			throw new AugmentAfpException(e.getMessage(), e);
		}
		
		return baos.toByteArray();
	}
	
	public byte[] getDocumentEnvironmentGroupData() {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		byte[] fmData = dataStore.get(name);
		boolean record = false;
		try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(fmData))) {
			StructuredField sf = null;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getCategoryCode() == CategoryCode.DOCUMENT_ENVIRONMENT_GROUP && sf.getTypeCode() == TypeCode.BEGIN) {
					record = true;
				}
				
				if (record) {
					baos.write(sf.bytes());
				}
				
				if (sf.getCategoryCode() == CategoryCode.DOCUMENT_ENVIRONMENT_GROUP && sf.getTypeCode() == TypeCode.END) {
					break;
				}
			}
		} catch (EOFException e) {
			// do nothing
		} catch (Exception e) {
			throw new AugmentAfpException(e.getMessage(), e);
		}
		
		return baos.toByteArray();
	}
	
	public StateEnvelope getFormMapState() {
		return getEnvelope(CategoryCode.FORM_MAP);
	}
	
	public StateEnvelope getResourceStateEnvelope() {
		return getEnvelope(CategoryCode.NAMED_RESOURCE);
	}
	
	private StateEnvelope getEnvelope(final CategoryCode categoryCode) {
		StateEnvelope env = null;
		byte[] fmData = dataStore.get(name);
		try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(fmData))) {
			StructuredField sf = null;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getCategoryCode() == categoryCode && sf.getTypeCode() == TypeCode.BEGIN) {
					env = new StateEnvelope(sf);
				} else if (sf.getCategoryCode() == categoryCode && sf.getTypeCode() == TypeCode.END) {
					env.setEnd(sf);
				}
			}
		} catch (EOFException e) {
			// do nothing
		} catch (Exception e) {
			throw new AugmentAfpException(e.getMessage(), e);
		}
		
		return env;
	}

}
