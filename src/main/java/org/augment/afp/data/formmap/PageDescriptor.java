package org.augment.afp.data.formmap;

import java.util.HashMap;
import java.util.Map;

import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.Orientation;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class PageDescriptor {
	
	private Map<Boolean, SheetGroup> sheetGroups;
	
	public PageDescriptor() {
		this.sheetGroups = new HashMap<>();
	}
	
	private void addSheetGroup(final SheetGroup sheetGroup) {
		sheetGroups.put(sheetGroup.isBackSide(), sheetGroup);
	}
		
	public SheetGroup getFrontSide() {
		return sheetGroups.get(Boolean.FALSE);
	}
	
	public SheetGroup getBackSide() {
		return sheetGroups.get(Boolean.TRUE);
	}
	
	public SheetGroup getSide(final boolean backside) {
		return sheetGroups.get(backside);
	}
	
	public static boolean is(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.PAGE && sf.getTypeCode() == TypeCode.MIGRATION;
	}
	
	public static PageDescriptor parse(final StructuredField pgpField) {
		
		// we are going to assume that we have the pgp field here
		byte[] data = pgpField.getData();
		
		int indexNow = 1;				
		
		PageDescriptor pgp = new PageDescriptor();
		
		while (indexNow < data.length) {
			int repeatGrpLen = ByteUtils.toUnsignedByte(data[indexNow]);
			byte[] repeatGrpData = ByteUtils.arraycopy(data, indexNow, repeatGrpLen);
			
			// origin needs to be figured out here (3 signed bytes to int)
			int originX = ByteUtils.toInt(ByteUtils.arraycopy(repeatGrpData, 1, 3));
			int originY = ByteUtils.toInt(ByteUtils.arraycopy(repeatGrpData, 4, 3));
						
			int orient = ByteUtils.toShort(ByteUtils.arraycopy(repeatGrpData, 7, 2));
			Orientation orientation = Orientation.findByValue(orient);
			
			byte sd = repeatGrpData[9];
			boolean backSide = false;
			if (sd == 0x00) {
				backSide = false;
			} else if (sd == 0x01) {
				backSide = true;
			} else {
				throw new IllegalArgumentException("N-Up Explicit Page Placement is not supported.");
			}
			
			pgp.addSheetGroup(new SheetGroup(originX, originY, orientation, backSide));
			
			indexNow += repeatGrpLen;
		}
		
		
		return pgp;
	}
	
	public static class SheetGroup {
		private int xOrigin;
		private int yOrigin;
		private Orientation orientation;
		private boolean backSide;
		
		SheetGroup(final int xOrigin, final int yOrigin, final Orientation orientation, final boolean backSide) {
			this.xOrigin = xOrigin;
			this.yOrigin = yOrigin;
			this.orientation = orientation;
			this.backSide = backSide;
		}
		
		public int getXOrigin() {
			return xOrigin;
		}
		
		public int getYOrigin() {
			return yOrigin;
		}
		
		public Orientation getOrientation() {
			return orientation;
		}
		
		public boolean isBackSide() {
			return backSide;
		}
	}
	
}

