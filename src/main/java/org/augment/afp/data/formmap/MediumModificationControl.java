package org.augment.afp.data.formmap;

import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.TypeCode;

public class MediumModificationControl {

	private int ctrlId;
	
	private boolean duplex;
	
	private int nup;
	
	public MediumModificationControl(final int ctrlId, final boolean duplex, final int nup) {
		this.ctrlId = ctrlId;
		this.duplex = duplex;
		this.nup = nup;
	}
	
	public int getCtrlId() {
		return ctrlId;
	}

	public boolean isDuplex() {
		return duplex;
	}
	
	public int getNup() {
		return nup;
	}
	
	public static boolean is(final StructuredField sf) {
		return sf.getCategoryCode() == CategoryCode.MEDIUM && sf.getTypeCode() == TypeCode.CONTROL;
	}
	
	public static MediumModificationControl parse(final StructuredField mmcField) {
		byte[] data = mmcField.getData();
		
		int ctrlId = data[0];
		boolean duplex = false;
		int nup = 1;
		
		for (int index = 2; index < data.length; ) {
			int keyword = ByteUtils.toUnsignedByte(data[index]);
			int parameter = ByteUtils.toUnsignedByte(data[index+1]);
			
			if (keyword == 0xF4) { // duplex control
				duplex = parameter != 0x01;
			} else if (keyword == 0xFC) {
				nup = keyword;
			}
			
			index += 2;
		}
		
		return new MediumModificationControl(ctrlId, duplex, nup);
	}
}
