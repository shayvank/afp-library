package org.augment.afp.data;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;

import org.augment.afp.data.formmap.FormMap;
import org.augment.afp.data.formmap.FormMapResource;
import org.augment.afp.util.AugmentAfpException;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.Triplet;
import org.augment.afp.util.TypeCode;

public class PrintFileResourceFactory {
	
	private ResourceDataStore resourceDataStore;
	
	public PrintFileResourceFactory(final ResourceDataStore resourceDataStore) {
		this.resourceDataStore = resourceDataStore;
	}

	public NamedResource createResource(final String name, final byte[] resource) {
		resourceDataStore.store(name, resource);
		
		NamedResource res = null;
		// font information (we need that?)
		try {
			DataInputStream dis = new DataInputStream(new ByteArrayInputStream(resource));
			StructuredField brs = StructuredField.getNext(dis);
			
			if (brs.getCategoryCode() == CategoryCode.NAMED_RESOURCE && brs.getTypeCode() == TypeCode.BEGIN) {
				String rsName = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(brs.getData(), 0, 8));
				List<Triplet> triplets = Triplet.parse(brs.getData(), 10, brs.getData().length - 10);
				for (Triplet trip: triplets) {
					if (trip.getCode() == 0x21) { // resource object type					
						int rot = ByteUtils.toUnsignedByte(trip.getContents()[0]);
						if (rot == 0xFE) { // form map
							res = createFormMap(rsName, brs, resource);
						} else {
							res = createGenericResource(rsName);
						}
					}
				}
			} else {
				throw new IllegalArgumentException("BRS expected but not found.");
			}
		} catch (Exception e) {
			throw new AugmentAfpException(e.getMessage(), e);
		}
		
		return res;
	}
	
	private NamedResource createFormMap(final String rsName, final StructuredField brs, final byte[] resource) throws IOException {		
		FormMap formMap = FormMap.parse(rsName, new DataInputStream(new ByteArrayInputStream(resource)));		
		return new FormMapResource(rsName, resourceDataStore, formMap);
	}
	
	private NamedResource createGenericResource(final String rsName) {		
		return new NamedResource(rsName, resourceDataStore);
	}
}
