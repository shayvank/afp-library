package org.augment.afp.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.augment.afp.data.formmap.MediumMap;
import org.augment.afp.request.ActionRequest;
import org.augment.afp.request.RequestHandler;
import org.augment.afp.request.RequestHandlerLoader;
import org.augment.afp.util.AugmentAfpException;
import org.augment.afp.util.ByteUtils;
import org.augment.afp.util.CategoryCode;
import org.augment.afp.util.StructuredField;
import org.augment.afp.util.StructuredFieldOutputStream;
import org.augment.afp.util.Templates;
import org.augment.afp.util.TypeCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageGroup {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PageGroup.class);
	
	private String id;
	
	private PageGroupDataStore dataStore;
	
	private PrintFileResourceGroup resourceGroup;
	
	private byte[] header;
	private List<Page> pages;
	private byte[] footer;
	
	private int sheetCount;
	
	private boolean loaded = false;
	
	public PageGroup(final String id, final PageGroupDataStore dataStore, final PrintFileResourceGroup resourceGroup) {
		this.id = id;
		this.dataStore = dataStore;
		this.resourceGroup = resourceGroup;
	}
	
	public String getId() {
		return id;
	}
	
	public int getSheetCount() {
		return sheetCount;
	}
	
	public PrintFileResourceGroup getPrintFileResourceGroup() {
		return resourceGroup;
	}
	
	public void load() throws IOException {
		header = null;
		pages = new ArrayList<>();
		footer = null;
		sheetCount = 0;
		// first, we need to separate the pagegroup into "header", pages, and "footer"
		// "header" will be the top section of the page group
		// pages will be the pages arraylist
		// "footer" will be whatever is after the last page
		
		ByteArrayOutputStream headerStream = new ByteArrayOutputStream();
		ByteArrayOutputStream pageStream = null;
		ByteArrayOutputStream footerStream = new ByteArrayOutputStream();
		
		// store the IMM value
		MediumMap activeIMM = null;
		
		try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(dataStore.get(id)))) {
			
			StructuredField sf = StructuredField.getNext(dis);
			
			if (!PageGroup.isBNG(sf)) {
				throw new IllegalArgumentException("A Begin Page Group (BNG) is expected.");
			}
			
			while (!(Page.isBPG(sf))) {
				if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.MAP) {
					String imm = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
					activeIMM = resourceGroup.getFormMap().getMediumMap(imm);
				}				
				headerStream.write(sf.bytes());				
				sf = StructuredField.getNext(dis);
			}
			
			if (!Page.isBPG(sf)) {
				throw new IllegalArgumentException("A Begin Page (BPG) is expected.");
			}
			
			pageStream = new ByteArrayOutputStream();
			boolean pageOpened = true;
			boolean backside = false;
			while (pageOpened) {
				if (sf != null) {
					pageStream.write(sf.bytes());
				}
				if (Page.isEPG(sf)) {
					// we need to store off that page
					pages.add(new Page(pageStream.toByteArray(), activeIMM, activeIMM.getPgp().getSide(backside)));
					pageStream = new ByteArrayOutputStream();
					pageOpened = false;
					sheetCount += backside ? 0 : 1;
				}
				
				sf = StructuredField.getNext(dis);
				
				if (Page.isBPG(sf)) {
					pageOpened = true;
					if (activeIMM.getMmc().isDuplex()) {
						backside = !backside;
					}
				} else if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.MAP) {
					String imm = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
					activeIMM = resourceGroup.getFormMap().getMediumMap(imm);
					backside = false; // this is to force a medium eject
					while (sf != null) {						
						// read until next BPG, but if we have a new medium map again, we need to replace again
						sf = StructuredField.getNext(dis);
						if (Page.isBPG(sf)) {
							pageOpened = true;
							break;
						} else if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.MAP) {
							imm = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
							activeIMM = resourceGroup.getFormMap().getMediumMap(imm);
						}
					}
				}
			}
			
			// the rest should just be footer
			if (sf != null) {
				footerStream.write(sf.bytes()); // this "could" be null from imm logic above
			}
			while ((sf = StructuredField.getNext(dis)) != null) {
				footerStream.write(sf.bytes());
			}
									
		} catch (EOFException ex) {
			// end of the stream
		}
		
		header = headerStream.toByteArray();
		footer = footerStream.toByteArray();
		loaded = true;
	}
	
	public void unload() {
		header = null;
		pages = null;
		footer = null;
		loaded = false;
	}
	
	public boolean isLoaded() {
		return loaded;
	}
	
	public byte[] getRaw() {
		return dataStore.get(id);
	}
	
	public void execute(final ActionRequest request) {
		RequestHandler handler = RequestHandlerLoader.findHandler(request);
		try {
			if (handler != null) {
				byte[] returnValue = handler.handle(request, header);
				if (request.isUpdate()) {
					header = returnValue;
				}
			}
		} catch (IOException e) {
			LOGGER.error("Error during PageGroup ActionRequest execution", e);
			throw new AugmentAfpException(e.getMessage(), e);
		}	
	}
	
	public int getPageCount() {
		return pages.size();
	}
	
	public Page getPage(final int index) {
		return pages.get(index);
	}
	
	public void writeTo(final StructuredFieldOutputStream outputStream) throws IOException {
		
		/*
		 * Here, we need to determine what the header IMM is
		 */
		String activeIMM = "";
		try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(header))) {
			StructuredField sf;
			while ((sf = StructuredField.getNext(dis)) != null) {
				if (sf.getCategoryCode() == CategoryCode.MEDIUM_MAP && sf.getTypeCode() == TypeCode.MAP) {
					activeIMM = ByteUtils.getDataEncoding().stringValue(ByteUtils.arraycopy(sf.getData(), 0, 8));
				}
			}
		} catch (IOException e) {
			// end of stream
		}

		writeByteToStructuredOutput(header, outputStream);

		if (!pages.isEmpty()) {
			for (Page page: pages) {
				String imm = page.getMediumMap().getName();
				if (!activeIMM.equals(imm)) {
					// we need to output an IMM structured field
					activeIMM = imm;
					outputStream.writeStructuredField(new StructuredField(Templates.createInvokeMediumMap(activeIMM)));
				}
				page.writeTo(outputStream);
			}
		}
		
		writeByteToStructuredOutput(footer, outputStream);
	}
	
	private void writeByteToStructuredOutput(final byte[] data, final StructuredFieldOutputStream outputStream) throws IOException {
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		StructuredField sf = null;
		try {
			while ((sf = StructuredField.getNext(dis)) != null) {
				outputStream.writeStructuredField(sf);
			}
		} catch (EOFException eof) {
			// end of stream
		}
	}
	
	public static boolean isBNG(final StructuredField sf) {
		return sf.getTypeCode() == TypeCode.BEGIN && sf.getCategoryCode() == CategoryCode.PAGE_GROUP;
	}
	
	public static boolean isENG(final StructuredField sf) {
		return sf.getTypeCode() == TypeCode.END && sf.getCategoryCode() == CategoryCode.PAGE_GROUP;
	}
	
	public static StructuredField createBNG(final String name) throws IOException {
		return createNG(TypeCode.BEGIN, name);
	}
	
	public static StructuredField createENG(final String name) throws IOException {
		return createNG(TypeCode.END, name);
	}
	
	private static StructuredField createNG(final TypeCode typeCode, final String name) throws IOException {
		ByteArrayOutputStream sfBytes = new ByteArrayOutputStream();
		
		String pageGroupName;
		
		if (name == null || name.length() == 0) {
			throw new IllegalArgumentException("Begin Named Group names cannot be blank.");
		} else {
			String namePad = name + "        ";
			pageGroupName = namePad.substring(0,8);
		}
		
		int bngLength = 16; 
		sfBytes.write(ByteUtils.fromShort((short) bngLength));
		sfBytes.write(0xd3);
		sfBytes.write(typeCode.toByte());
		sfBytes.write(CategoryCode.PAGE_GROUP.toByte());
		sfBytes.write(0x00); // flags
		sfBytes.write(0x00); // reserved
		sfBytes.write(0x00); // reserved
		sfBytes.write(ByteUtils.getDataEncoding().byteValue(pageGroupName));
		
		return new StructuredField(sfBytes.toByteArray());
	}
	

}
