package org.augment.afp.data;

/**
 * Page group data store stores page groups for later retrieval.
 *
 */
public interface PageGroupDataStore {

	/**
	 * Stores (saves) the page group id and data.
	 * 
	 * @param id page group id
	 * @param bytes data
	 */
	void store(String id, byte[] bytes);
	
	/**
	 * Gets the page group data given the page group id.
	 * 
	 * @param id page group id
	 * @return data
	 */
	byte[] get(String id);
}
