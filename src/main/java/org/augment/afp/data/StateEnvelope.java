package org.augment.afp.data;

import org.augment.afp.util.StructuredField;

/**
 * MO:DCA architecture defines a state to be a domain within the data stream, bounded by a begin-end structured field pair.
 * This envelope object is used to represent that state domain.
 *
 */
public class StateEnvelope {

	private StructuredField begin;
	
	private StructuredField end;
	
	/**
	 * Creates an instance with just a begin structured field.
	 * 
	 * @param begin structured field
	 */
	public StateEnvelope(final StructuredField begin) {
		this.begin = begin;
	}
	
	/**
	 * Creates an instance with a begin-end structured field pair.
	 * 
	 * @param begin structured field
	 * @param end structured field
	 */
	public StateEnvelope(final StructuredField begin, final StructuredField end) {
		this(begin);
		this.end = end;
	}
	
	/**
	 * Sets an end structured field.
	 * 
	 * @param end structured field
	 */
	public void setEnd(final StructuredField end) {
		this.end = end;
	}
	
	/**
	 * Returns the begin structured field.
	 * 
	 * @return begin structured field
	 */
	public StructuredField getBegin() {
		return begin;
	}
	
	/**
	 * Returns the end structured field.
	 * 
	 * @return end structured field
	 */
	public StructuredField getEnd() {
		return end;
	}
}
